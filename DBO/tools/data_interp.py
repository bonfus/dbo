import re, sys
import numpy as np
import pickle

from scipy.interpolate import Rbf, griddata

ang2bohr = 1.8897261
ev2ha    = 27.21138505

def parse_data(prefix, atom_label, lp=-1):
    #
    # prefix     : prefix of file name (same as prefix input in dbo if not changed)
    # atom_label : name of the atom moved during the calculation
    # lp         : last oint to be parsed. A dummy large number is the default, i.e. parse all point 
    #
    dat = open(prefix+'.dat','r')
    pos = open(prefix+'.axsf','r')
    
    npoints = int(pos.readline().split()[1])
    
    if lp>0:
        npoints=lp
    
    x = np.zeros(npoints)
    y = np.zeros(npoints)
    z = np.zeros(npoints)
    v = np.zeros(npoints)
    
    i = -1
    
    for line in pos.readlines():
        d = line.split()
        if 'PRIMCOORD' in line:
            i += 1
            if i == npoints:
                break
            v[i] = float(dat.readline())
            continue
        if len(d) < 3: continue
        
        if atom_label == d[0]:
            x[i] = float(d[1])
            y[i] = float(d[2])
            z[i] = float(d[3])
           

    v-=np.min(v)
    dat.close()
    pos.close()
    
    return [x,y,z,v]
    
def save_data(x,y,z,v):
    f = open('data.pickle','wb')
    pickle.dump((x,y,z,v ),f)
    f.close()

def load_data():
    f = open('data.pickle','r')
    x,y,z,v = pickle.load(f)
    f.close()
    return (x,y,z,v)

def interpolate(x,y,z,v, method='griddata', thr=None):
    if method=='rbf':
        t = Rbf(x,y,z,v,function='linear')
    elif method=='griddata':
        t = lambda xx, yy, zz: griddata(np.column_stack([x,y,z]),v , (xx, yy, zz), method='linear',fill_value=np.max(v))
    elif method=='griddata_t':
        if thr is None:
            raise ValueError
        def ithr(xx, yy, zz):
            vo = griddata(np.column_stack([x,y,z]),v , (xx, yy, zz), method='linear',fill_value=np.max(v))
            vo[np.where(vo>=thr)] = thr
            return vo
        t = ithr
    return t


def meshgrid2(*arrs):
    arrs = tuple(reversed(arrs))  #edit
    lens = map(len, arrs)
    dim = len(arrs)

    sz = 1
    for s in lens:
        sz*=s

    ans = []    
    for i, arr in enumerate(arrs):
        slc = [1]*dim
        slc[i] = lens[i]
        arr2 = np.asarray(arr).reshape(slc)
        for j, sz in enumerate(lens):
            if j!=i:
                arr2 = arr2.repeat(sz, axis=j) 
        ans.append(arr2)

    return tuple(ans[::-1])
    
def get_grid_pos(x,y,z,v):
    box = []
    box.append([np.min(x)*1.1,np.max(x)*1.1])
    box.append([np.min(y)*1.1,np.max(y)*1.1])
    box.append([np.min(z)*1.1,np.max(z)*1.1])
    return box

def output_points(x,y,z,v,box):
    sx,sy,sz = box[0][0],box[1][0],box[2][0]
    ax,ay,az =(box[0][1] - box[0][0]), (box[1][1] - box[1][0]), (box[2][1] - box[2][0])
    
    print ('Box size: ', np.round(np.max([ax,ay,az])*ang2bohr,decimals=6))
    
    
    nx = (x - sx - (ax/2.0)) * ang2bohr
    ny = (y - sy - (ay/2.0)) * ang2bohr
    nz = (z - sz - (az/2.0)) * ang2bohr

    print ('Min x, Max x:' ,  np.min(nx), np.max(nx) )
    print ('Min y, Max y:' ,  np.min(ny), np.max(ny) )
    print ('Min z, Max z:' ,  np.min(nz), np.max(nz) )
    
    nv = v / ev2ha
    
    with file('pot.dat', 'w') as datfile:
        datfile.write(str(len(x))+'\n')
        for i in range(len(x)):
            np.savetxt(datfile,[nx[i],ny[i],nz[i],nv[i]],delimiter=" ",fmt='%-9.7f', newline=' ')
            datfile.write('\n')

def write_xsf_header(prefix, atom_label):
    in_xsf_file = open(prefix+'.axsf','r')
    out_xsf_file = open('test.xsf','w')
    
    while True:
        line = in_xsf_file.readline()
        if 'CRYSTAL' in line:
            out_xsf_file.write(line)
            out_xsf_file.write(in_xsf_file.readline())
            out_xsf_file.write(in_xsf_file.readline())
            out_xsf_file.write(in_xsf_file.readline())
            out_xsf_file.write(in_xsf_file.readline())
            break
    while True:
        line = in_xsf_file.readline()
        if 'PRIMCOORD' in line:
            out_xsf_file.write(line)
            n_atoms = int(in_xsf_file.readline().split()[0])
            n_atoms -= 1
            
            out_xsf_file.write("     " + str(n_atoms) + "  1\n")
            break            
            
    for i in range( n_atoms + 1 ):
        line = in_xsf_file.readline()
        d = line.split()
        if len(d) < 3: continue
        if atom_label != d[0]:
            out_xsf_file.write(line)
            continue
        elif i <= n_atoms:
            out_xsf_file.write(line)
            
    out_xsf_file.write('\n')
    

def generate_grid(box,nx,ny,nz,interp_function):


    sx,sy,sz = box[0][0],box[1][0],box[2][0]
    cubic_box = np.max([box[0][1] - box[0][0], box[1][1] - box[1][0], box[2][1] - box[2][0]])
    ax,ay,az = cubic_box,cubic_box,cubic_box
    
    header =    """ 
BEGIN_BLOCK_DATAGRID_3D
 whatever
 BEGIN_DATAGRID_3D
          {na}          {nb}          {nc}
      {am}      {bm}      {cm}
      {a}      0.0000000000      0.0000000000
      0.0000000000      {b}      0.0000000000
      0.0000000000      0.0000000000      {c}
""".format(am=str(sx),bm=str(sy),cm=str(sz),a=str(ax),b=str(ay),c=str(az),na=str(nx),nb=str(ny),nc=str(nz))

    # the potential is in atomic units
    #ax = ang2au*ax
    #ay = ang2au*ay
    #az = ang2au*az

    gx = np.linspace(sx,sx+cubic_box,nx, endpoint=True)
    gy = np.linspace(sy,sy+cubic_box,ny, endpoint=True)
    gz = np.linspace(sz,sz+cubic_box,nz, endpoint=True)
    
    xx, yy, zz = meshgrid2(gx,gy,gz)
    R = interp_function(xx,yy,zz)
    
    # Write XSF file
    with file('test.xsf', 'a') as xsffile:

        xsffile.write(header)
        
        xsffile.write('# Array shape: {0}\n'.format(R.shape))
    
        for data_slice in R:
    
            # The formatting string indicates that I'm writing out
            # the values in left-justified columns 7 characters in width
            # with 2 decimal places.  
            np.savetxt(xsffile, data_slice, fmt='%-7.5f')
    
            # Writing out a break to indicate different slices...
            xsffile.write('# New slice\n')
        xsffile.write(' END_DATAGRID_3D\n  END_BLOCK_DATAGRID_3D')
        
    xx = (xx - sx - (cubic_box/2.0)) * ang2bohr
    yy = (yy - sy - (cubic_box/2.0)) * ang2bohr
    zz = (zz - sz - (cubic_box/2.0)) * ang2bohr
    R /= ev2ha
    # Write pot.dat file
    
    
    print('Separation: %f' % ((cubic_box* ang2bohr)/float(nx-1)))
    
    with file('pot.dat', 'w') as datfile:
        datfile.write(str((nx-1)*(ny-1)*(nz-1))+'\n')
        for i in range((nz-1)):
            for j in range((ny-1)):
                for k in range((nx-1)):
                    np.savetxt(datfile,[xx[i,j,k],yy[i,j,k],zz[i,j,k],R[i,j,k]],delimiter=" ",fmt='%-9.7f', newline=' ')
                    datfile.write('\n')
    

def generate_grid_split(box,nx,ny,nz,interp_function,split=2):


    sx,sy,sz = box[0][0],box[1][0],box[2][0]
    cubic_box = np.max([box[0][1] - box[0][0], box[1][1] - box[1][0], box[2][1] - box[2][0]])
    ax,ay,az = cubic_box,cubic_box,cubic_box
    
    header =    """ 
BEGIN_BLOCK_DATAGRID_3D
 whatever
 BEGIN_DATAGRID_3D
          {na}          {nb}          {nc}
      {am}      {bm}      {cm}
      {a}      0.0000000000      0.0000000000
      0.0000000000      {b}      0.0000000000
      0.0000000000      0.0000000000      {c}
""".format(am=str(sx),bm=str(sy),cm=str(sz),a=str(ax),b=str(ay),c=str(az),na=str(nx),nb=str(ny),nc=str(nz))

    # the potential is in atomic units
    #ax = ang2au*ax
    #ay = ang2au*ay
    #az = ang2au*az
    
    
    R = np.zeros([nx,ny,nz])
    
    nx_block = nx/split
    ny_block = ny/split
    nz_block = nz/split
    
    gx = np.linspace(sx,sx+cubic_box,nx, endpoint=True)
    gy = np.linspace(sy,sy+cubic_box,ny, endpoint=True)
    gz = np.linspace(sz,sz+cubic_box,nz, endpoint=True)
    
    for ix in range(split):
        for iy in range(split):
            for iz in range(split):
                endpointx = (ix == (split-1))
                endpointy = (iy == (split-1))
                endpointz = (iz == (split-1))
                
                npx  = nx_block if not endpointx else np.ceil(nx-ix*nx_block)
                
                npy  = ny_block if not endpointy else np.ceil(ny-iy*ny_block)
                
                npz  = nz_block if not endpointz else np.ceil(nz-iz*nz_block)
                
                ggx = gx[ix*nx_block:ix*nx_block+npx]
                ggy = gy[iy*ny_block:iy*ny_block+npy]
                ggz = gz[iz*nz_block:iz*nz_block+npz]
    
                xx, yy, zz = meshgrid2(ggx,ggy,ggz)
                
                R[ix*nx_block:ix*nx_block+npx, iy*ny_block:iy*ny_block+npy, iz*nz_block:iz*nz_block+npz] = (interp_function(xx,yy,zz)).T
    
    # Write XSF file
    with file('test.xsf', 'a') as xsffile:

        xsffile.write(header)
        
        xsffile.write('# Array shape: {0}\n'.format(R.shape))
    
        for data_slice in R:
    
            # The formatting string indicates that I'm writing out
            # the values in left-justified columns 7 characters in width
            # with 2 decimal places.  
            np.savetxt(xsffile, data_slice, fmt='%-7.5f')
    
            # Writing out a break to indicate different slices...
            xsffile.write('# New slice\n')
        xsffile.write(' END_DATAGRID_3D\n  END_BLOCK_DATAGRID_3D')
    
    
    xx, yy, zz = meshgrid2(gx,gy,gz)
    xx = (xx - sx - (cubic_box/2.0)) * ang2bohr
    yy = (yy - sy - (cubic_box/2.0)) * ang2bohr
    zz = (zz - sz - (cubic_box/2.0)) * ang2bohr
    R /= ev2ha
    # Write pot.dat file
    
    
    print('Separation: %f' % ((cubic_box* ang2bohr)/float(nx-1)))
    
    with file('pot.dat', 'w') as datfile:
        datfile.write(str((nx-1)*(ny-1)*(nz-1))+'\n')
        for i in range((nz-1)):
            for j in range((ny-1)):
                for k in range((nx-1)):
                    np.savetxt(datfile,[xx[i,j,k],yy[i,j,k],zz[i,j,k],R[i,j,k]],delimiter=" ",fmt='%-9.7f', newline=' ')
                    datfile.write('\n')
    


if __name__ == '__main__':
    
    import argparse
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    
    group.add_argument("-parse", action="store_true")
    group.add_argument("-load", action="store_true")
    
    parser.add_argument("-s", "--save", help="Save parsed data to pickle.",
                    action="store_true")

    parser.add_argument("-tp", "--totpoint",type=int, default=-1,
                        help="Number of points to be parsed/read.")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-r","--rbf", action="store_true")
    group.add_argument("-l","--linear", action="store_true")
    group.add_argument("-n","--nointerpolation", action="store_true")
    
    parser.add_argument("-thr", "--threshold",type=float, default=1000.0,
                        help="Threshold for cutting the potential profile.")    
    
    parser.add_argument("prefix", type=str,
                    help="DBO prefix")  

    parser.add_argument("atmname", type=str,
                    help="Name of the atom used to explore the potential.") 
                    
                                         
    parser.add_argument("-oxsf","--output-xsf", action="store_true")
    
    args = parser.parse_args()

    if args.parse:
        if args.totpoint:
            #prse only a subset of all points
            data = parse_data(args.prefix,args.atmname,args.totpoint)
        else:
            data = parse_data(args.prefix,args.atmname)
    elif args.load:
        data = load_data()
        
    if args.save and (not args.load):
        save_data(*data)
    
    if args.nointerpolation:
        box = get_grid_pos(*data)
        output_points(*data,box=box)
        #generate_3dsh_input()   
        sys.exit(0)
    
    if args.rbf:
        interp_function = interpolate(*data, method='rbf')
    elif args.linear:
        if args.threshold < 1000.0:
            interp_function = interpolate(*data, method='griddata_t', thr=args.threshold)
        else:
            interp_function = interpolate(*data, method='griddata')
    
    box = get_grid_pos(*data)
    write_xsf_header(args.prefix,args.atmname)
    generate_grid_split(box,49,49,49, interp_function)
    sys.exit(0)

