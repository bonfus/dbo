#!/usr/bin/env python
#-*- coding:utf-8 -*-

# Use a minimal FGH implementation to compute eigenfumctions for bound 1d Hamiltonians

import numpy as np
import matplotlib.pyplot as plt
from Hamiltonian import h as H, hcont as HCont  # TODO: fix lowercase name
from scipy.linalg import eigh
from scipy.integrate import simps
from scipy import interpolate

L = 1.5  # length of x-interval to use
N = 2**10  # number of samples
NplotMin = 0  # quantum number of first eigenfunction to plot
Nplot = 3  # number of eigenfunctions to plot
# consider a H atom
# put here m_atom/m_electron with m_electron = 1 
mass_ratio = 1836.1527 # for hydrogen
# put here m_atom/m_electron with m_electron = 1 
mass_ratio = 206.7683 # for muon

# define a potential:
#pot = lambda x: +0.5 * 3.0**2 * (x-L/2.)**2  # harmonic oscillator potential, centered
#pot = lambda x: -0.5 * 10.0**2 * (x-L/2.)**2 + (x-L/2.)**4  # double well potential
#pot = lambda x: mass_ratio * 0.0948 * (x-L/2.)**2

# interpolate potential from data file
x,v = np.loadtxt('data').T
# center potential
x -= np.min(x)
# shift to positive values
v += (0.0-np.min(v))
# go to atomic units
v /= 27.2107

#
# The idea is to solve:
#   [p^2 /2 m_atom + V(r) ] psi = E psi
#   [p^2 /2 m_ratio*m_e + V(r) ] psi = E psi
#   [p^2 /2 m_e + m_ratio*V(r) ] psi = m_ratio*E psi
v *= mass_ratio

pot = interpolate.interp1d(x, v)

#----
# define a grid:
x = np.linspace(0, L, N)

# sample potential:
Vsampled = pot(x)
print ('==> Using periodic boundary conditions')
Hsampled = H(L, Vsampled)

# diagonalize the Hamiltonian matrix:
E, psi = eigh(Hsampled)

WFscaleFactor = (np.max(Vsampled) - np.min(Vsampled))/Nplot
plt.plot(x, Vsampled, ls="-", c="k", lw=2)
for i in range(NplotMin, NplotMin+Nplot):
    # physically normalize WF
    WFnorm = np.sqrt(simps(np.abs(psi[:,i])**2, x=x))
    psi[:,i] /= WFnorm
    plt.plot(x, WFscaleFactor*np.abs(psi[:,i])**2 + E[i], ls="-", lw=1)
    print("E[%s] = %s Ha"%(i, E[i]))
    print("Rescaled E[%s] = %s Ha"%(i, E[i]/mass_ratio))
    print("Transition E[%s] = %s cm^-1"%(i, 219474.6 * (E[i]-E[NplotMin])/mass_ratio)) # 1 Ha = 219474.6 cm^-1

plt.show()

print ('==> Not using periodic boundary conditions')
Hsampled = HCont(L, Vsampled)

# diagonalize the Hamiltonian matrix:
E, psi = eigh(Hsampled)

WFscaleFactor = (np.max(Vsampled) - np.min(Vsampled))/Nplot
plt.plot(x, Vsampled, ls="-", c="k", lw=2)
for i in range(NplotMin, NplotMin+Nplot):
    # physically normalize WF
    WFnorm = np.sqrt(simps(np.abs(psi[:,i])**2, x=x))
    psi[:,i] /= WFnorm
    plt.plot(x, WFscaleFactor*np.abs(psi[:,i])**2 + E[i], ls="-", lw=1)
    print("E[%s] = %s Ha"%(i, E[i]))
    print("Rescaled E[%s] = %s Ha"%(i, E[i]/mass_ratio))
    print("Transition E[%s] = %s cm^-1"%(i, 219474.6 * (E[i]-E[NplotMin])/mass_ratio)) # 1 Ha = 219474.6 cm^-1

plt.show()

# TODO: plot potential *and* WF
# TODO: perform physical normalization of the WFs
