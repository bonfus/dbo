!----------------------------------------------------------------------------
!
! ... Module for potential functions
!
MODULE poten
    use kinds,        ONLY : DP    

    public Harmonic, Gauss
    
    
    CONTAINS
    
    real(DP) function Harmonic(x,y,z)
        implicit none
        real(DP) x,y,z
        Harmonic = 0.5 * ( x**2 + y**2 + z**2 )
        return
    end function Harmonic

    real(DP) function Gauss(x,y,z,a,d)
        implicit none
        real(DP) x,y,z,a,d
        real(DP) r2
		r2 = x*x + y*y + z*z
		Gauss =  d * dexp(a * r2)
        return
    end function Gauss
    
    real(DP) function SoftCul(x,y,z,a,d)
        implicit none
        real(DP) x,y,z,a,d
        real(DP) r2    
        r2 = x*x + y*y + z*z
		SoftCul = d / sqrt(r2 + a)
		return 
    end function SoftCul


END MODULE poten



