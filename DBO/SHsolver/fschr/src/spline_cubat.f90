!----------------------------------------------------------------------------
!
! ... Cubatures for numerical integration
!
module cubat
    use kinds,      only : DP
    use spline_data,only :Sdeg, Cord
    public :: Rvals,Svals,Tvals,Weights
    
    real, allocatable :: Rvals(:),Svals(:),Tvals(:),Weights(:)
    
    public :: init_cubat, deallocate_cubat
    
contains
    subroutine init_cubat()
        implicit none
        integer :: csize
        integer :: k, s, t, u
        
        real(DP) :: w1D(Cord), x1D(Cord)
        

        
        csize = Cord * Cord * Cord
        
        
        allocate(Rvals(csize))
        allocate(Svals(csize))
        allocate(Tvals(csize))
        allocate(Weights(csize))       
        
        CALL gaulegf(-1.d0, 1.d0, x1D, w1D, Cord);
        
        k = 1
	    do s=1, Cord 
            do t = 1, Cord
                do u=1, Cord
                    Rvals(k)   = x1D(s)
                    Svals(k)   = x1D(t)
                    Tvals(k)   = x1D(u)
                    Weights(k) = w1D(s) * w1D(t) * w1D(u)
                    k = k + 1
                end do
            end do
        end do

    end subroutine init_cubat


    ! gauleg.f90     P145 Numerical Recipes in Fortran
    ! compute x(i) and w(i)  i=1,n  Legendre ordinates and weights
    ! on interval -1.0 to 1.0 (length is 2.0)
    ! use ordinates and weights for Gauss Legendre integration
    !
    subroutine gaulegf(x1, x2, x, w, n)
    implicit none
    integer, intent(in) :: n
    double precision, intent(in) :: x1, x2
    double precision, dimension(n), intent(out) :: x, w
    integer :: i, j, m
    double precision :: p1, p2, p3, pp, xl, xm, z, z1
    double precision, parameter :: eps=3.d-14
        
    m = (n+1)/2
    xm = 0.5d0*(x2+x1)
    xl = 0.5d0*(x2-x1)
    do i=1,m
        z = cos(3.141592654d0*(i-0.25d0)/(n+0.5d0))
        z1 = 0.0
        do while(abs(z-z1) .gt. eps)
        p1 = 1.0d0
        p2 = 0.0d0
        do j=1,n
            p3 = p2
            p2 = p1
            p1 = ((2.0d0*j-1.0d0)*z*p2-(j-1.0d0)*p3)/j
        end do
        pp = n*(z*p1-p2)/(z*z-1.0d0)
        z1 = z
        z = z1 - p1/pp
        end do
        x(i) = xm - xl*z
        x(n+1-i) = xm + xl*z
        w(i) = (2.0d0*xl)/((1.0d0-z*z)*pp*pp)
        w(n+1-i) = w(i)
    end do
    end subroutine gaulegf
    
    subroutine deallocate_cubat
        deallocate(Rvals,Svals,Tvals,Weights)
    end subroutine deallocate_cubat
    
end module cubat
