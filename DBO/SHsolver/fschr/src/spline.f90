!-----------------------------------------------------------------------------------!
!                                                                                   !
!                        U-Spline Schroedinger solver                               !
!                                                                                   !
!   Description: one particle 3D Shroedinger equation solver                        !
!                on a real space spline basis                                       !
!                                                                                   !
!   Author:      Pietro Bonfa - pietro (dot) bonfa (at) fis (dot) unipt (dot) it    !
!                                                                                   !
!   This file is distributed under the terms of the                                 !
!   GNU General Public License. See the file `License'                              !
!   in the root directory of the present distribution,                              !
!   or http://www.gnu.org/copyleft/gpl.txt .                                        !
!                                                                                   !
!-----------------------------------------------------------------------------------!
!
module spline

  use kinds,      only : DP

    ! vectors for allocating sparse Hamiltonian and Overlap matrix 
    ! and element indices
  real(DP), allocatable :: Hmat(:)
  real(DP), allocatable :: Smat(:)
  integer, allocatable :: isH(:), jsH(:)  
    !
    ! vector and matrices used to save eigenvalues and eigenvectors
  real(DP), allocatable :: Evals(:)
  real(DP), allocatable :: Evecs(:,:)
  

  public init_spline, solve, finalize, assemble, get_wfc, normalize

contains
    !-------------------------------------------------------------------
    subroutine init_spline
    !-------------------------------------------------------------------
    !    
    ! This subroutine initialize the spline solver. Defines the box size
    ! and calls various initialization routines of submodules 
    !
        USE spline_data, ONLY : x0,y0,z0, h,nx,ny,nz,xmax,ymax,zmax,&
                                nsplx,nsply,nsplz, Sdeg
                                
        USE overkin, ONLY : overkin_precalc
        USE potelt, ONLY  : potelt_precalc
        USE cubat,   ONLY : init_cubat
        !
        implicit none
        !
        ! boundary of the simulation box
        xmax = x0 + h * nx;
        ymax = y0 + h * ny;
        zmax = z0 + h * nz;
        !
        ! number of spline function used
        nsplx = nx - Sdeg
        nsply = ny - Sdeg
        nsplz = nz - Sdeg
        !
        write(*,'(5X,A)') 'Precalc OverKin'
        CALL overkin_precalc
        !
        write(*,'(5X,A)') 'Initailize Cubatures'
        CALL init_cubat
        !
        write(*,'(5X,A)') 'Precalc Poten'
        CALL potelt_precalc  
        !
        write(*,'(5X,A)') 'Initialized'
    end subroutine init_spline
    !
    !
    !-------------------------------------------------------------------
    subroutine assemble
    !-------------------------------------------------------------------
    ! This subroutine calculates matrix elements
        USE overkin, ONLY : OvrKin
        USE potelt, ONLY : potential
        USE spline_data, ONLY : x0,y0,z0, h,nx,ny,nz,xmax,ymax,zmax, Cord, Sdeg, nsplx,nsply,nsplz
        !
        implicit none
        ! 
        integer :: idx(3), jdx(3) ! spline coordinate indices
        !
        ! variables used to compute the number of non-zero elements
        integer :: sw, sc, W, N, vertex, border, face, center 
        !
        ! counters
        integer :: i,k,l,ix,iy,iz,jx,jy,jz
        integer :: jxbeg, jxend, jybeg, jyend, jzbeg, jzend
        !
        ! douoble precision overlap, kinetic, potential energy (and auxiliary for sum)
        real(DP) :: ovr,kin, p, aux
        !
        !
        ! Calculates the number of non-zero elements. This is done
        ! by counting the number of neighbouring splines in a cube
        ! 
        ! maximum number of neigbours (including itself) for each
        ! B-Spline in each direction
        !
        sw = 2*Sdeg+1
        !
        ! calculates a usefull quantity
        sc = 0
        do i=Sdeg+1,sw
            sc = sc + i
        end do
        !
        ! WARNING: this is only for a cube! easy to generalize to parallelepiped
        !
        ! compute various elements
        ! vertex (Sdeg x Sdeg x Sdeg cube at the vertexes)
        vertex = (sc**3)
        !
        !
        ! edge
        border = (sw)*(sc**2)*(nsplx-2*(Sdeg+1))
        !
        ! faces
        face  = (sw**2)*(sc)*((nsplx-2*(Sdeg+1))**2)
        !
        !center
        center = (sw**3) * ((nsplx-2*(Sdeg+1))**3)
        ! sum all contributions
        W = 8 * vertex + 6*face + 12*border + center
        !
        write(*,'(5X,A, I)') 'Non-zero elements: ', W
        !
        ! H and S matrix dimension
        N = nsplx*nsply*nsplz
        !
        write(*,'(5X,A)') 'Allocating memory'
        allocate(Hmat(W))
        allocate(Smat(W))
        allocate(isH(N+1))
        allocate(jsH(W))        
        !
        write(*,'(5X,A$)') 'Computing matrix elements'
        !
        ! number of elements per row        
        isH = 0
        isH(1) = 1
        !
        k = 1 
        l = 1
        !
        do ix=1, nsplx
            write(*,'(A1$)') '.'
            do iy=1, nsply
                do iz=1,nsplz
                    !
                    idx(1) = ix
                    idx(2) = iy
                    idx(3) = iz
                    !
                    ! ix,iy,iz spline interacts with the 3 splines
                    ! on each direction if present.
                    !
                    jxbeg = min(ix, max(1,ix-Sdeg))
                    jybeg = min(iy, max(1,iy-Sdeg))
                    jzbeg = min(iz, max(1,iz-Sdeg))
                    !
                    jxend = min(ix+Sdeg,nsplx)
                    jyend = min(iy+Sdeg,nsply)
                    jzend = min(iz+Sdeg,nsplz)
                    !
                    do jx=jxbeg, jxend
                        do jy=jybeg,jyend
                            do jz=jzbeg,jzend
                                !
                                jdx(1) = jx
                                jdx(2) = jy
                                jdx(3) = jz                               
                                !
                                ! get kinetic energy and overlap
                                CALL OvrKin(idx,jdx,ovr,kin)
                                !
                                ! get potential energy
                                CALL Potential(idx,jdx, p)
                                !
                                ! now you have k, o and p
                                aux = kin + p ! + alpha * ovr;
                                
                                ! fill H and S matrices
                                Hmat(k) = aux
                                Smat(k) = ovr
                                !
                                ! index of spline jx,jy,jz in H and S matrix 
                                jsH(k) = nsplz*nsply*(jx-1) + nsplz*(jy-1) + (jz-1) + 1
                                !
                                k = k + 1
                            end do
                        end do
                    end do
                    isH(l+1)=k
                    l = l + 1 
                end do
            end do
        end do    
        write(*,'(A)') 'done!'
    end subroutine assemble
    
    subroutine solve
        USE kinds,   ONLY : DP
        USE overkin, ONLY : OvrKin
        USE potelt, ONLY : potential
        
        USE spline_data, ONLY : x0,y0,z0, h,nx,ny,nz,xmax,ymax,zmax, &
                                Cord, Sdeg, nsplx,nsply,nsplz, &
                                search_interval, mass, evtoha
        implicit none

        real(DP), allocatable :: dE(:), dres(:), dX(:,:)

      !!!!!!!!!!!!!!!!! Feast declaration variable
        integer,dimension(64) :: feastparam 
        double precision :: depsout
        real :: sepsout
        integer :: loop       
        
      !!!!!!!!!!!!!!!!! Others
        integer :: t1,t2,tim
        integer :: i,j, k, l
        integer :: N,M0,M,info
        double precision :: dEmin,dEmax
        
        N = nsplx*nsply*nsplz
        
        
        M0 = 100

        allocate(Evals(1:M0))     ! Eigenvalue
        allocate(dres(1:M0))      ! Residual  
        allocate(Evecs(1:N,1:M0)) ! Eigenvectors
        
        call feastinit(feastparam)
        
        feastparam(1) = 1 !com
        feastparam(2) = 8 !nbe

        feastparam(3) = 2

        feastparam(4) = 6 !maxloop
        feastparam(6) = 1 !residi
                
        call dfeast_scsrgv('F',N,Hmat,isH,jsH,Smat,isH,jsH,feastparam, &
                            depsout,loop,search_interval(1),search_interval(2), &
                            M0,Evals,Evecs,M,dres,info)
        
        deallocate(Hmat, Smat, isH, jsH)
        
        print *,'TRACE',sum(Evals(1:M))
        print *,'Relative error on the Trace',depsout
        print *,'Eigenvalues/Residuals'
        do i=1,M
            print *,i,Evals(i),dres(i)
        enddo
        !
        print *,'Eigenvalues/mass [Ha]'
        do i=1,M
            print *,i,Evals(i)/mass,dres(i)/mass
        enddo        

        print *,'Eigenvalues/mass [eV]'
        do i=1,M
            print *,i,Evals(i)/mass/evtoha,dres(i)/mass/evtoha
        enddo     
                
        deallocate(dres)
        
    end subroutine solve
    
    subroutine normalize(n)
        
        use spline_data, only: nsplx,nsply,nsplz, Sdeg
        use spline_functions, only: BSpline_S
        use overkin, only : GetOvr
        
        implicit none
        integer, intent(in) :: n ! which wavefunction
        real(DP) :: squaresum, o
        
        integer :: l,m, i, j, k, ix,iy,iz, totn, tempcounter
        integer :: ibeg(3), iend(3)
        
        integer :: idx(3), jdx(3)
        
        totn = nsplx*nsply*nsplz
        
        squaresum = 0.d0
        tempcounter = 0
        do l=1,totn
            !squaresum = squaresum + Evecs(i,n)*Evecs(i,n)*Ovr()
            
            ix = (l-1)/(nsplz*nsply)
            iy = ((l-1)-ix*nsplz*nsply)/nsplz
            iz = (l-1)-ix*nsplz*nsply-iy*nsply
            
            !write(*,*) 'ix,iy,iz', ix,iy,iz
            
            !
            ibeg(1) = ix+1
            ibeg(2) = iy+1
            ibeg(3) = iz+1
            !
            iend(1) = min(ix+Sdeg,nsplx)
            iend(2) = min(iy+Sdeg,nsply)
            iend(3) = min(iz+Sdeg,nsplz)
            
            idx(1)=ix
            idx(2)=iy
            idx(3)=iz
            !write(*,*) 'l: ', l
            do i=ibeg(1),iend(1)
                do j=ibeg(2),iend(2)
                    do k=ibeg(3),iend(3)
                    
                        jdx(1)=i-1
                        jdx(2)=j-1
                        jdx(3)=k-1
                        
                        m = nsplz*nsply*(i-1) + nsplz*(j-1) + (k-1) + 1
                        !write(*,*) 'm: ', m
                        call GetOvr(idx,jdx,o)
                        !write(*,*) 'ovr: ', o
                        squaresum = squaresum + Evecs(l,n)*Evecs(m,n)*o
                        tempcounter = tempcounter + 1
                    end do
                end do
            
            end do
        end do
        write(*,*) 'I mede', tempcounter , 'integrals'
        write(*,*) 'squaresum', squaresum
        
    end subroutine normalize
    
    subroutine get_wfc(x,y,z,n,res)
        use spline_data, only: Sdeg, h,nsplx,nsply,nsplz,x0,y0,z0
        use spline_functions, only: BSpline_Value
        !
        implicit none
        real(DP), intent(in) :: x, y, z
        integer, intent(in) :: n ! which wavefunction
        real(DP), intent(out) :: res
        
        integer :: l,i,j,k,xbeg,ybeg,zbeg,xend,yend,zend
        real(DP) :: r,s,t,rx,ry,rz
        integer :: bx,by,bz
        
        bx = int((x-x0)/h)+1
        by = int((y-y0)/h)+1
        bz = int((z-z0)/h)+1
        !        
        xbeg = min(bx, max(1,bx-Sdeg-1))
        ybeg = min(by, max(1,by-Sdeg-1))
        zbeg = min(bz, max(1,bz-Sdeg-1))
        !
        xend = min(bx,nsplx)
        yend = min(by,nsply)
        zend = min(bz,nsplz)
        
        res = 0.d0
        l=0
        do i=xbeg,xend
            do j=ybeg,yend
                do k=zbeg,zend
                
                    !rx = ( r + dfloat(bx-i))
                    !ry = ( s + dfloat(by-j))
                    !rz = ( t + dfloat(bz-k))  
                    
                    r = ((x-x0) - h*(i - 1))/h
                    s = ((y-y0) - h*(j - 1))/h
                    t = ((z-z0) - h*(k - 1))/h
                    !write(*,*) 'r s t: ', r,s,t
                    
                    !write(*,*) 'rx ry rz: ', rx,ry,rz
                    
                    l = nsplx*nsply*(i-1) + nsplx*(j-1) + (k-1) + 1
                    
                    res = res + BSpline_Value(r,Sdeg)*&
                                BSpline_Value(s,Sdeg)*&
                                BSpline_Value(t,Sdeg)*Evecs(l,n)
                end do
            end do
        end do
    
    end subroutine
    
    subroutine save_wfc(fname, xsfnx, xsfny, xsfnz, n)
        use spline_data, only : x0,y0,z0,h,nx,ny,nz 
        implicit none
        character(len=64), intent(in) :: fname
        
        integer, intent(in) :: xsfnx, xsfny, xsfnz
        integer, intent(in) :: n
        
        
        integer :: i,j,k
        real(DP) :: x,y,z,res
        
        
        open(10,file='./wfc.xsf',action='write')
        write(10,'(A)') 'BEGIN_BLOCK_DATAGRID_3D'
        write(10,'(A)') '3D_WAVEFUNCTION'
        write(10,'(A)') 'DATAGRID_3D_UNKNOWN'
        write(10,'(3I)') xsfnx, xsfnx, xsfnx
        write(10,'(3F12.9)') x0*0.529177211, y0*0.529177211, z0*0.529177211
        write(10,'(3F12.9)') (h*nx)*0.529177211, 0.d0, 0.d0
        write(10,'(3F12.9)') 0.d0, (h*ny)*0.529177211, 0.d0
        write(10,'(3F12.9)') 0.d0, 0.d0, (h*nz)*0.529177211
        
        
        do k=1,xsfnz
            do j=1,xsfny
                do i=1,xsfnx
                    x = x0 + (i-1)*h
                    y = y0 + (j-1)*h
                    z = z0 + (k-1)*h
                    CALL get_wfc(x,y,z,n,res)
                    write(10,'(F16.12)') res
                end do
            end do
        end do
        
        write(10,'(A)') 'END_DATAGRID_3D'
        write(10,'(A)') 'END_BLOCK_DATAGRID_3D'
        close(10,status='keep')
    
    end subroutine save_wfc
    
    subroutine finalize
        use overkin, only : deallocate_overkin
        use cubat,   only : deallocate_cubat
        use potelt,  only : deallocate_potelt
        
        call deallocate_overkin
        call deallocate_cubat
        call deallocate_potelt
        
        deallocate(Evals,Evecs)
    end subroutine finalize
  
end module spline
