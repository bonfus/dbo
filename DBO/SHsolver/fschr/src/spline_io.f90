MODULE spline_io
    USE kinds, ONLY : DP
    !
    PUBLIC :: save_xsf
    !
    CONTAINS
    !
    SUBROUTINE save_xsf(np, vout)

        USE spline_data, ONLY: nx,ny,nz,Cord,x0,y0,z0,h
        !
        IMPLICIT NONE
        !
        integer, intent(in) :: np
        real(DP), intent(in) :: vout(np)
        !
        integer :: px,py,pz,Q, l
        
        Q = Cord * Cord * Cord
    
        open(10,file='./interppot.xsf',action='write')
        !write(10,'(A)') 'BEGIN_BLOCK_DATAGRID_3D'
        !write(10,'(A)') 'BEGIN_BLOCK_DATAGRID_3D'
        !write(10,'(A)') 'BEGIN_BLOCK_DATAGRID_3D'
        !write(10,'(A)') 'BEGIN_BLOCK_DATAGRID_3D'
        write(10,'(A)') 'BEGIN_BLOCK_DATAGRID_3D'
        write(10,'(A)') '3D_CULOMBPOT'
        write(10,'(A)') 'DATAGRID_3D_UNKNOWN'
        write(10,'(3I)') nx, ny, nz
        write(10,'(3F12.9)') x0*0.529177211, y0*0.529177211, z0*0.529177211
        write(10,'(3F12.9)') (h*nx)*0.529177211, 0.d0, 0.d0
        write(10,'(3F12.9)') 0.d0, (h*ny)*0.529177211, 0.d0
        write(10,'(3F12.9)') 0.d0, 0.d0, (h*nz)*0.529177211
        
        
        !do pz= 0,nz-1
        !    do iqz = 0, Cord-1            
        !        do py= 0, ny-1
        !            do iqy = 0, Cord-1                        
        !                do px = 0, nx-1
        !                    do iqx = 0, Cord -1
        !                        iq = 1 + iqx*Cord*Cord + iqy*Cord + iqz
        !                        l = pz + nz*py + nz*ny*px
        !                        l = l*Q + iq
        !                        write (10, '(4F24.12)') positions(:,l), vout(l)
        !                        !write (10, '(F24.12)')  vout(l)
        !                    end do
        !                end do
        !            end do
        !        end do
        !    end do
        !end do
        do pz= 0,nz-1
                do py= 0, ny-1
                        do px = 0, nx-1
                                l = pz + nz*py + nz*ny*px
                                l = l*Q + 1
                                write (10, '(4F24.12)') vout(l)
                                !write (10, '(F24.12)')  vout(l)
                        end do
                end do
        end do    
        
        write(10,'(A)') 'END_DATAGRID_3D'
        write(10,'(A)') 'END_BLOCK_DATAGRID_3D'
        close(10,status='keep')
    END SUBROUTINE
END MODULE spline_io
