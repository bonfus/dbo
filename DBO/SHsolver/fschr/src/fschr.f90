!-----------------------------------------------------------------------------------!
!                                                                                   !
!                        U-Spline Schroedinger solver                               !
!                                                                                   !
!   Description: one particle 3D Shroedinger equation solver                        !
!                on a real space spline basis                                       !
!                                                                                   !
!   Author:      Pietro Bonfa - pietro (dot) bonfa (at) fis (dot) unipt (dot) it    !
!                                                                                   !
!   This file is distributed under the terms of the                                 !
!   GNU General Public License. See the file `License'                              !
!   in the root directory of the present distribution,                              !
!   or http://www.gnu.org/copyleft/gpl.txt .                                        !
!                                                                                   !
!-----------------------------------------------------------------------------------!
!

program FSCHR
    USE spline,  ONLY: init_spline, solve, finalize, assemble, get_wfc, normalize, save_wfc
    USE spline_data, ONLY : x0, y0, z0, h, nx,ny,nz, Sdeg, Cord, mass, &
                            potfile, intrpfname, interptype, search_interval, fpp, &
                            set_defaults
    USE kinds, ONLY : DP
    !
    implicit none
    !
    real(DP) testwfc, r, res
    real(DP) wfc(100)
    !
    namelist /inputspl/ x0, y0, z0, nx, ny, nz, h, Sdeg, Cord, mass
    namelist /interpolator/ potfile, interptype 
    namelist /solver/ search_interval, fpp
    !
    !
    call set_defaults()
    !
    open(7, file='input.in')
    read(7, nml = inputspl)
    rewind(7)
    read(7, nml = interpolator)
    rewind(7)
    read(7, nml = solver)
    close(7)
    
    search_interval = search_interval * mass
    
    ! spline initialization    
    CALL init_spline
    !
    !
    write(*,*) 'Assemble H and S'
    CALL assemble
    !
    !
    write(*,*) 'Now I solve'
    CALL solve
    
    !intrpfname = "./wfc.xsf"
    !CALL save_wfc(intrpfname, 30, 30, 30, 1)
    !
    CALL finalize
    !
end program
