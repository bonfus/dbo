!----------------------------------------------------------------------------
!
! ... These are the definitions of 1D spline functions
!
module spline_functions    

public BSpline_Value, BSpline_ValueD, BSpline_S

contains

!
! Returns value of one dimensional B-spline of degree "n" at point "x"
!
recursive FUNCTION  BSpline_Value(x, n) RESULT(res)
    use kinds,      only : DP
    
    implicit none
    REAL(DP), INTENT(IN) :: x
    INTEGER, INTENT(IN) :: n
    
    REAL(DP) res
    
    REAL(DP) a 
    REAL(DP) b 

    a = 0
    b = 0

    if ( n < 0 )  write (*,*) 'Panico Paura!'
        
    if (n == 0) then
        if ( 0. <= x .AND. x < 1. ) then
            res = 1.d0
            return
        else
            res = 0.d0
            return
        end if
    end if

    a = x / n;
    b = (n + 1 - x) / n;

    res = a * BSpline_Value(x, n - 1) + b * BSpline_Value(x - 1, n - 1);
    return
END FUNCTION BSpline_Value

!
! Returns value of first derivative of one dimensional B-spline of degree "n" at point "x"
!
real(DP) FUNCTION BSpline_ValueD(x, n)
    use kinds,      only : DP
  
    implicit none
    REAL(DP), INTENT(IN) :: x
    INTEGER, INTENT(IN) :: n       
    BSpline_ValueD = BSpline_Value(x, n - 1) - BSpline_Value(x - 1, n - 1)
    return
END FUNCTION BSpline_ValueD

!
! Helper function used for evaluation of overlap and
! kinetic integrals analytically
!
real(DP) FUNCTION BSpline_S(k, n)
    use kinds,      only : DP
    
    implicit none
    INTEGER, INTENT(IN) :: k, n 
    BSpline_S = BSpline_Value(DFLOAT (n + k + 1), 2 * n + 1) 
    return
END FUNCTION BSpline_S

end module spline_functions   
