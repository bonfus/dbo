!----------------------------------------------------------------------------
!
! ... Common variables for the spline program
!
module spline_data

  use kinds,      only : DP

  REAL(dp) :: x0, y0, z0, h ! cube border and spacing
  REAL(dp) :: xmax, ymax, zmax
  
  integer :: nsplx,nsply,nsplz
  
  INTEGER  :: nx, ny, nz  ! number of segments
  INTEGER  :: Sdeg, Cord  ! Bspline degree, Cubature Order  

  ! 
  real(DP) :: mass = 1.0d0   ! muon mass / electron mass
  !real(DP) :: mass = 206.7683   ! muon mass / electron mass
  real(DP), parameter ::  evtoha = 0.03674932 ! 1 eV in Ha
  !
  ! Data for potential interpolation
  character(len=64) :: potfile
  character(len=64) :: intrpfname
  integer :: interptype
  REAL(DP) :: rbf_r0 = 2.0D0
  INTEGER :: phitype=6
  INTEGER :: qs3_nq = -1
  INTEGER :: qs3_nr = -1
  INTEGER :: qs3_nw = -1
  !
  REAL(DP), allocatable :: potpoints(:,:), potvals(:)
  INTEGER :: potnp = 0
  !
  ! data for sparse marix solver
  real(DP) :: search_interval(2)
  integer,dimension(15) :: fpp 

  public x0,y0,z0,h,xmax,ymax,zmax,Sdeg,Cord, nsplx,nsply,nsplz, &
          potfile, intrpfname, interptype, search_interval, fpp, mass,&
          potnp, potpoints, potvals
          
  public set_defaults, load_potential, deallocate_potential
  
  CONTAINS
  SUBROUTINE set_defaults
    USE interpolator, ONLY : set_default_params, set_interp_params
    
    mass = 1.0d0
    
    search_interval(1) = 0.0 * mass
    search_interval(2) = 5.0 * mass
    
    
    fpp(:) = -1
    
    !interpolator
    irbf_r0 = 2.0D0
    iphitype=6
    CALL set_default_params()
    !
    ! SET RBF
    SELECT CASE (interptype)
    CASE (0)
        CALL set_interp_params(0,irbf_r0 = 2.0D0,iphitype=6)      
    CASE (2)
        IF ( qs3_nq > 0 ) CALL set_interp_params(2, iqs3_nq=qs3_nq)
        IF ( qs3_nr > 0 ) CALL set_interp_params(2, iqs3_nr= qs3_nr)
        IF ( qs3_nw > 0 ) CALL set_interp_params(2, iqs3_nw =qs3_nw)
    CASE DEFAULT
        WRITE(*,*) 'Interpolation type not implemented!'
        STOP
    END SELECT
    !
    
  
  END SUBROUTINE set_defaults
  
  SUBROUTINE load_potential
  
    OPEN	(37,FILE=potfile,ACTION='READ',ERR=98)
    READ	(37,*,ERR=98) potnp
    
    potnp=INT(potnp)
    IF (potnp < 0 )	THEN
        RETURN
    END IF

    allocate (potpoints(3,potnp))
    allocate (potvals(potnp))
    
    l=1
    DO		I	= 1, potnp
    READ	(37,*,ERR=98) X, Y, Z, V
            potpoints(1,l) = X
            potpoints(2,l) = Y
            potpoints(3,l) = Z
            potvals(l) = V
            l = l+1
    END DO
        
    CLOSE	(37)   
    RETURN
98  WRITE(*,*) 'Error opening potential file!'
    STOP
  END SUBROUTINE
  
  SUBROUTINE deallocate_potential
    deallocate (potpoints, potvals)  
  END SUBROUTINE deallocate_potential
  
end module spline_data
