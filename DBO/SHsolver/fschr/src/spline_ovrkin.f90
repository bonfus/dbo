!
! This module calculates kinetic energy and overlap between splines
!
module overkin
    use kinds, only : DP
    
    real(dp), allocatable  ::  ovr(:)
    real(dp), allocatable  ::  der(:)
    
    public overkin_precalc, OvrKin, GetOvr, deallocate_overkin
    
    CONTAINS
    !
    ! Calculates one-dimensional overlap and kinetic orbitals ANALITYCALY
    !
    subroutine overkin_precalc
        use spline_data, only : h, Sdeg
        use spline_functions, only: BSpline_S
        implicit none
        real(DP) :: a, b, c
        integer :: k
        
        allocate(ovr(Sdeg+1))
        allocate(der(Sdeg+1))
    
        do k=1,Sdeg+1
            ovr(k) = h * BSpline_S(k-1, Sdeg)
            !write (*,*) 'Ovr k', ovr(k)
        end do
        
        do k=1,Sdeg+1
            a = BSpline_S(k-1, Sdeg - 1)
            b = BSpline_S(k - 2, Sdeg - 1)
            c = BSpline_S(k , Sdeg - 1)
            der(k) = (2 * a - b - c) / h   
            !write (*,*) 'der k', der(k)        
        end do
    
    end subroutine overkin_precalc  

    !
    ! Returns element of overlap (ovr) and kinetic (kin) matrix between two B-splines .
    !
    subroutine OvrKin(idx, jdx, rovr, rkin)
    
        implicit none
        integer, intent(in) :: idx(3), jdx(3)
        real(DP), intent(out) :: rovr
        real(DP), intent(out) :: rkin
        
        integer :: jx,jy,jz
        real(DP) :: dx,dy,dz
        
        jx = abs( idx(1) - jdx(1) ) + 1  ! index starts from 1
        jy = abs( idx(2) - jdx(2) ) + 1  ! index starts from 1
        jz = abs( idx(3) - jdx(3) ) + 1  ! index starts from 1
        
        rovr = ovr(jx) * ovr(jy) * ovr(jz)
    
        dx = der(jx) * ovr(jy) * ovr(jz)
        dy = ovr(jx) * der(jy) * ovr(jz)
        dz = ovr(jx) * ovr(jy) * der(jz)
    
        rkin = 0.5 * (dx + dy + dz)
    end subroutine OvrKin

    subroutine GetOvr(idx, jdx, rovr)
    
        implicit none
        integer, intent(in) :: idx(3), jdx(3)
        real(DP), intent(out) :: rovr
        
        
        integer :: jx,jy,jz
        real(DP) :: dx,dy,dz
        
        jx = abs( idx(1) - jdx(1) ) + 1  ! index starts from 1
        jy = abs( idx(2) - jdx(2) ) + 1  ! index starts from 1
        jz = abs( idx(3) - jdx(3) ) + 1  ! index starts from 1
        
        rovr = ovr(jx) * ovr(jy) * ovr(jz)
    
    end subroutine GetOvr


    subroutine deallocate_overkin
        deallocate(ovr,der)
    end subroutine deallocate_overkin
end module overkin
