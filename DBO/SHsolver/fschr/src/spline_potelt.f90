!----------------------------------------------------------------------------
!
! ... Matrix element for potential energy
!
module potelt
    use kinds,      only : DP
    real(DP), allocatable :: bspl_vals(:,:) ! this contains preculaculated bsplpine values
    real(DP), allocatable :: pot_vals(:,:) ! this contains potential values at cubatures points
    
    logical :: save_intrp_pot = .true.
    
    public potelt_precalc, potential, deallocate_potelt

    contains
    
    
    integer function IdxBsplQ(px,py,pz,w)
    
        integer :: px,py,pz,w
        
        IdxBsplQ = w * (w * px + py) + pz + 1
        
    end function IdxBsplQ

    integer function IdxPotQ(px,py,pz,wx,wy,wz)
    
        integer :: px,py,pz,wx,wy,wz
        
        IdxPotQ = wy * (wx * px + py) + pz + 1
        
    end function IdxPotQ
     
    subroutine potential(idx,jdx, res)
    
        use kinds, only : DP
        
        use poten, only : Harmonic
        use spline_data, only : x0,y0,z0,h,Sdeg, Cord, nx, ny, nz
        use cubat,  only : Rvals,Svals,Tvals,Weights
        
        implicit none
        integer, intent(in) :: idx(3)
        integer, intent(in) :: jdx(3)
        real(DP), intent(out) :: res

        integer :: ibeg(3), iend(3)
        integer :: Q, P
        real(DP) :: w,r,s,t,x,y,z,pot, h2, cellPot, potTot

        integer :: i, px,py,pz,dx,dy,dz,iq, ki,kj, kp
        
        P = Sdeg + 1
        Q = Cord * Cord * Cord

        do i = 1, 3
            ibeg(i) = max(idx(i),jdx(i))
            iend(i) = min(idx(i),jdx(i)) + Sdeg
        end do
        
        potTot = 0.d0

        do px= ibeg(1),iend(1)
            do py= ibeg(2), iend(2)
                do pz = ibeg(3), iend(3)
                    cellPot = 0.d0
                    
                    dx = px - idx(1)
                    dy = py - idx(2)
                    dz = pz - idx(3)
                    
                    ki = IdxBsplQ(dx, dy, dz, P) ! Index of B-Spline B_i
                    
                    dx = px - jdx(1)
                    dy = py - jdx(2)
                    dz = pz - jdx(3)                        
                    kj = IdxBsplQ(dx, dy, dz, P) ! Index of B-Spline B_j
                    !write(*,*) 'ki, kj: ', ki, kj
                    
                    do iq = 1, Q
                        r = Rvals(iq)
                        s = Svals(iq)
                        t = Tvals(iq)
                        w = Weights(iq)
                        
                      
						x = x0 + h * (0.5 * (r + 1) + (px-1))
						y = y0 + h * (0.5 * (s + 1) + (py-1))
						z = z0 + h * (0.5 * (t + 1) + (pz-1))                        
                        
                        
                        kp = IdxPotQ(px-1, py-1, pz-1, nx, ny, nz)
                        
                        pot = w * pot_vals(iq,kp)
                        
                        cellPot = cellPot + bspl_vals(iq, ki) * bspl_vals(iq, kj) * pot
                        
                    end do
                    
                    potTot = potTot + cellPot;
                    
                end do
            end do
        end do
    	
        h2 = 0.5 * h
        res = potTot * h2 * h2 * h2
        
    end subroutine potential
    
    subroutine potelt_precalc
        use kinds, only : DP
        use spline_data, only : Sdeg, Cord, nx, ny, nz, x0, y0, z0, h, &
                                 potpoints, potvals, load_potential, mass, &
                                 deallocate_potential, potnp, interptype
        use spline_functions, only: BSpline_Value
        use cubat,  only : Rvals,Svals,Tvals
        use interpolator, only : interpolate
        use poten, only : Harmonic, Gauss
        use spline_io, only : save_xsf
        
        implicit none
        integer :: P, Q
        
        ! loopers
        integer :: px, py, pz, iq, k, l
        integer :: iqx, iqy, iqz
        
        !cubat points
        real(DP) :: x,y,z, r,s,t, ax, ay ,az, valore
        
        
        real(DP), allocatable :: positions(:,:)
        real(DP), allocatable :: vout(:)
        
        P = Sdeg + 1
        Q = Cord * Cord * Cord
                
        allocate(bspl_vals(Q,P*P*P))        
        
        do px = 1, P
            
            do py = 1, P
                
                do pz=1, P 
                    
                    do iq=1, Q
                        
                        r = Rvals(iq)
                        s = Svals(iq)
                        t = Tvals(iq)
                        !write(*,'(A, 3F20.10)') 'r,s,t,',r,s,t
                        x = 0.5 * (r + 1.0) + dfloat(px-1)
                        y = 0.5 * (s + 1.0) + dfloat(py-1)
                        z = 0.5 * (t + 1.0) + dfloat(pz-1)
                        !write(*,'(A, 3F20.10)') 'x,y,z,',x,y,z
                        ax = BSpline_Value(x, Sdeg)
                        ay = BSpline_Value(y, Sdeg)
                        az = BSpline_Value(z, Sdeg)
                        !write(*,'(A, 3F20.10)') 'ax,ay,az,',ax,ay,az
                        k = IdxBsplQ(px-1, py-1, pz-1, P)

                        bspl_vals(iq, k) = ax * ay * az
                        !write(*,'(A, I4, I4, F20.10)') 'precalc, iq, k: ', iq, k, bspl_vals(iq, k)
                        !write(*,*)
                    end do
                    
                end do
            end do
        end do
        
        allocate(pot_vals(Q,(nx)*(ny)*(nz)))
        allocate(positions(3,Q*(nx)*(ny)*(nz)))
        allocate(vout(Q*(nx)*(ny)*(nz)))
        
        CALL load_potential()
        
        l=1
        
        do px= 1,nx
            do py= 1, ny
                do pz = 1, nz               
                    
                    do iq = 1, Q
                        r = Rvals(iq)
                        s = Svals(iq)
                        t = Tvals(iq)
                        
                        
						x = x0 + h * (0.5 * (r + 1) + dfloat(px-1))
						y = y0 + h * (0.5 * (s + 1) + dfloat(py-1))
						z = z0 + h * (0.5 * (t + 1) + dfloat(pz-1))                        
                        

                        positions(1, l) = x
                        positions(2, l) = y
                        positions(3, l) = z
                        l = l + 1
                       
                    end do
                    !

                    !
                end do
            end do
        end do

        CALL interpolate( 3, potnp , potpoints, potvals,  Q*(nx)*(ny)*(nz), &
                            positions, interptype, vout )
        l=1
        do px= 1,nx
            do py= 1, ny
                do pz = 1, nz               
                    
                    k = IdxPotQ(px-1, py-1, pz-1, nx, ny, nz) ! Index of Potential value 
                                        
                    do iq = 1, Q        
                        ! JUST FOR TESTING
                        !valore = Harmonic(x, y, z)
                        !valore = Gauss(x,y,z,-1.0d0,-1.0d0)
                        !pot_vals(iq, k) = valore
                        ! END TESTING
                        
          
                        
                        pot_vals(iq, k) = vout(l) * mass !  * mass
                        l = l + 1
                    end do
                end do
            end do
        end do
        
        
        if (save_intrp_pot) then
            CALL save_xsf(Q*nx*ny*nz,vout)
        end if              
        
        
        deallocate(positions,vout)
        CALL deallocate_potential
        
        
    end subroutine
    
    subroutine deallocate_potelt
        deallocate(bspl_vals,pot_vals)
    end subroutine deallocate_potelt
    
end module potelt    





