objects	= $(OBS)globals.o $(OBS)3dsch.o $(OBS)preset.o $(OBS)readd.o \
	$(OBS)inivect.o $(OBS)vext.o $(OBS)imstep.o $(OBS)ortho.o \
	$(OBS)vare.o
#
3dsch	: prepare $(objects) $(pseudo)
	$(FORT) $(FLAL) $(objects) $(pseudo) $(LDIR) $(LIBS) -o 3dsch

# create directory for objects
prepare :
	- ( if ! test -d $(OBS) ; then ( mkdir $(OBS) || exit 1 ) ; fi )

globals.mod	: globals.f90
	$(FORT) $(FL90) $(IDIR) globals.f90 -o $(OBS)globals.o
$(OBS)globals.o	: globals.f90
	$(FORT) $(FL90) $(IDIR) globals.f90 -o $(OBS)globals.o

$(OBS)3dsch.o : 3dsch.f90 3dsch.inc $(OBS)globals.o
	$(FORT) $(FL90) $(IDIR) 3dsch.f90   -o $(OBS)3dsch.o
$(OBS)preset.o : preset.f90  3dsch.inc $(OBS)globals.o
	$(FORT) $(FL90) $(IDIR) preset.f90  -o $(OBS)preset.o
$(OBS)readd.o : readd.f90  3dsch.inc $(OBS)globals.o
	$(FORT) $(FL90) $(IDIR) readd.f90   -o $(OBS)readd.o
$(OBS)inivect.o : inivect.f90  3dsch.inc $(OBS)globals.o
	$(FORT) $(FL90) $(IDIR) inivect.f90 -o $(OBS)inivect.o
$(OBS)vext.o : vext.f90  3dsch.inc $(OBS)globals.o
	$(FORT) $(FL90) $(IDIR) vext.f90    -o $(OBS)vext.o
$(OBS)imstep.o : imstep.f90  3dsch.inc $(OBS)globals.o
	$(FORT) $(FL90) $(IDIR) imstep.f90  -o $(OBS)imstep.o
$(OBS)ortho.o : ortho.f90  3dsch.inc $(OBS)globals.o
	$(FORT) $(FL90) $(IDIR) ortho.f90   -o $(OBS)ortho.o
$(OBS)vare.o : vare.f90  3dsch.inc $(OBS)globals.o
	$(FORT) $(FL90) $(IDIR) vare.f90    -o $(OBS)vare.o

#ifeq ($(USEC),1)
#$(OBS)vusr.o : vusr.c
#	$(CC) $(CFLAGS) $(IDIR) vusr.c    -o $(OBS)vusr.o
#
#else
#
#$(OBS)vusr.o : vusr.f90
#	$(FORT) $(FL90) $(IDIR) vusr.f90    -o $(OBS)vusr.o
#
#endif
