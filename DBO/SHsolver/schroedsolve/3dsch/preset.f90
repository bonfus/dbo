!============================================================================
!
SUBROUTINE PRESET
#include "3dsch.inc"
! 
INTEGER, PARAMETER ::	ICNF  = 7
INTEGER, PARAMETER ::	IOLD  = 8
DOUBLE PRECISION	HR2, SPECT
LOGICAL			EX
CHARACTER (LEN=64) ::	CNFILE
!
!	This is still a weird aspect of fftw3 -- we need to
!	allocate space that we never need.
!
COMPLEX (KIND(0.D0)),	ALLOCATABLE, DIMENSION (:) ::	TMPR
COMPLEX (KIND(0.D0)),	ALLOCATABLE, DIMENSION (:) ::	TMPC
INTEGER			K, NSTA, ORDER

NAMELIST /MESH /	MX, MY, MZ, HR, MAXIM, MORB, &
			IMSG, ORDER, RMUL, &
			ESTP, ESTE, EPSI, EPSR

NAMELIST /MODEL/	H2M, NORB, RPAR, IPAR, &
			INFILE, OUTFIL, POTFIL, INTYPN, &
            RBFPHI, RBFR0, EPSPOS, QS3NQ,QS3NW,QS3NR            
!
!----------------------------------------------------------------------------
!
!	MX:	Mesh in x-direction is -MX:MX-1
!	MY:	Mesh in y-direction is -MY:MY-1
!	MZ:	Mesh in z-direction is -MZ:MZ-1
!		The mesh sizes must be multiples of 4, but for
!		best performance they should be a power of 2.
!
!	HR:	Discretization in coordinate space.
!		There is no good reason for using different
!		step sizes in different directions.
!
!	MAXIM	is the maxiumum number of imaginary timesteps.
!	MORB	Number of states that are propagated.
!		This number may be larger than the number
!		of states calculated to improve convergence.
!
!	ORDER	order of derivatives in Laplacian.
!	RMUL	multiplicator of timestep ramp.
!		0.5 is a good value.
! 
!	ESTP	Convergence factor of the imaginary timestep iterations.
!		This is the value at which the iterations start.
!
!	ESTE	Convergence factor of the imaginary timestep iterations.
!		This is the smallest value that is used during the iterations.
!
!	IMSG	A bit field, defining the level at which output messages
!		on the iterations are generated, and controlling some
!		technical aspects of the calculation.
!
!	IMSG	BIT	Function
!	 0	-	No informaton on iterations
!	 1	0	Show errors and timings after for each converged
!			time step.
!	 2	1	Show errors and timing after each imaginary timestep
!			iterations.
!	 4	2	Show timigs.
!	 8	3	Dump eigenvalues and wave functions into a
!                       binary data file. This generates a huge file.
!			Meant for testing, documentation, and further
!			processing.
!	16	4	Do the second order algorithm.
!			(Set this flag only to waste CPU time).
!
!	EPSI:		Desired accuracy for the imaginary time step
!			iterations.
!	EPSR:		Ratio between expectation energy and normalization
!			energy error. EPSR=0 will turn this off.
!
!----------------------------------------------------------------------------
!
IINP	=  5					! stdin file
IOUT	=  6					! stdout file
IRES	= 60					! sequence of test files
!
!	Default values for mesh and flags
!
EPSI	= 1.D-8
EPSR	= 0.D0
ORDER	= 9					! Laplacian order
RMUL	= 0.5D0					! Ramp multiplicator
IMSG	= 1					! Message level
!
!   Default values for added input variables
!
INTYPN  = 2
RBFPHI  = 6
RBFR0   = 0.0D0
EPSPOS  = 1.0D-5
QS3NQ   = 17
QS3NW   = 32
QS3NR   = 3
!
!----------------------------------------------------------------------------
!
!	Read mesh parameters:
!
CNFILE	= TRIM(PREFIX)//".mesh"
WRITE	(IOUT,99996) CNFILE
!
!	Namelist-like input for user readability
!
INQUIRE	(FILE=CNFILE,EXIST=EX)
IF (.NOT.EX) THEN
  PRINT *,'Mesh-input file does not exist'
  STOP
END IF
OPEN	(ICNF,FILE=CNFILE,STATUS='OLD',ERR=97)
READ	(ICNF,MESH)
CLOSE	(ICNF)
!
IF (ORDER.EQ. 0)	ORDER	= 9
IF (RMUL .EQ. 0.D0)	RMUL	= 0.5D0
!
IF (BTEST(IMSG,4))	THEN
  PRINT *,'***** Running 2nd order algorithm:'
  PRINT *,'***** I hope you know what you are doing !'
END IF
MAXIM	= MAX0(1, MAXIM)			! Max. imstep iterations
IF (MOD(MX,4) .NE. 0)	THEN
  MX	= 4*(MX/4+1)
  PRINT '(A,I3)',' ***** Warning: MX changed to',MX
END IF
IF (MOD(MY,4) .NE. 0)	THEN
  MY	= 4*(MY/4+1)
  PRINT '(A,I3)',' ***** Warning: MY changed to',MY
END IF
IF (MOD(MZ,4) .NE. 0)	THEN
  MZ	= 4*(MZ/4+1)
  PRINT '(A,I3)',' ***** Warning: MZ changed to',MZ
END IF
MX2	= MX+1
MY2	= 2*MY
MZ2	= 2*MZ
!
!	More discretization constants.
!	HR is the stepsizes in all directions. There is no
!	good reason for using different mesh sizes in
!	x, y, and z--direction.
!
!	FFT requires FRQ*FQR = 1/(8*MX*MY*MZ) and HR*HQi = 2*PI/(2*Mi)
!		where i = (x,y,z)
!
HQX	= PI/(HR*MX)
HQY	= PI/(HR*MY)
HQZ	= PI/(HR*MZ)
MXYZ	= 8*MX*MY*MZ				! Total number of grid points.
FRQ	= HR**3
FQR	= HQX*HQY*HQZ/(2.D0*PI)**3
!
!----------------------------------------------------------------------------
!
!	Physics parameters come from an extra configuration file.
!	This allows us to have a clean separation between the
!	inputs defining the numerical discretization,
!	the physical model, and the graphics output when applicable.
!
!	Read model parameters:
!	We read in the value of hbar^2/2m such that
!	the user can basically use any system of units.
!
!	Trivial initialization of model.
!
H2M		= 1.D0					! hbar^2/2m
RPAR(1:10)	= 0.D0
IPAR(1:10)	= 0
NORB		= 0
CNFILE	= TRIM(PREFIX)//".model"
INQUIRE (FILE=CNFILE,EXIST=EX)
IF (.NOT.EX)	THEN
  PRINT *,'Model file does not exist'
  STOP
END IF
!
!	Namelist-like input for user readability
!
OPEN	(ICNF,FILE=CNFILE,STATUS='OLD',ERR=96)
READ	(ICNF,MODEL)
CLOSE	(ICNF)
!
!	Check for stupid parametrizations.
!
IF (NORB.LE.0)	STOP
MORB	= MAX0(MORB,NORB)
WRITE	(IOUT,99994) OUTFIL
WRITE	(IOUT,99998) POTFIL
WRITE	(IOUT,99993) MX, MY, MZ, HR
WRITE	(IOUT,99992) MAXIM, NORB
!
!	Allocate "global" arrays.
!	First physics arrays:
!
ALLOCATE (VONE(-MX:MX-1,-MY:MY-1,-MZ:MZ-1))
ALLOCATE (DVON(-MX:MX-1,-MY:MY-1,-MZ:MZ-1))
!
!	Allocate eigenvalue and eigenvector fields
!
ALLOCATE (EVAL(1:MORB))                        ! Overlap eigenvalues
ALLOCATE (EVAR(1:MORB))                        ! Local variational eigenvalues
ALLOCATE (EVEC(-MX:MX-1,-MY:MY-1,-MZ:MZ-1,MORB))
EVAL (1:MORB)	= 0.D0
EVAR (1:MORB)	= 0.D0
!
!	Allocate and preset kinetic energy tables.
!	First derivative needed for the magnetic
!	field code only.
!
ALLOCATE (T2X(0:2*MX-1), T2Y(0:2*MY-1), T2Z(0:2*MZ-1))
!
HR2	= HR/2.D0
DO	K	= 0, 2*MX-1
  T2X(K)	= SPECT(K,MX,ORDER)/HR2**2
END DO
DO	K	= 0, 2*MY-1
  T2Y(K)	= SPECT(K,MY,ORDER)/HR2**2
END DO
DO	K	= 0, 2*MZ-1
  T2Z(K)	= SPECT(K,MZ,ORDER)/HR2**2
END DO
!
!	Initialize FFT routines.
!
!	This always initializes real forward and backward
!	transformation because most of the transformations
!	will be used both ways anyway.
!
ALLOCATE(TMPR(MXYZ))
ALLOCATE(TMPC(4*(MX+1)*MY*MZ))

CALL DFFTW_PLAN_DFT_R2C_3D(PLAXK, 2*MX,2*MY,2*MZ, &
	TMPR, TMPC, FFTW_ESTIMATE)
CALL DFFTW_PLAN_DFT_C2R_3D(PLAKX, 2*MX,2*MY,2*MZ, &
	TMPR, TMPC, FFTW_ESTIMATE)
DEALLOCATE (TMPR, TMPC)
!
!	Now see if we have perhaps a file with wave functions.
!	Accept that file if the mesh has the same size,
!	and read as many states we can find.
!
!	Initialize wave functions with plane waves.
!
CALL	INIVECT
!
NSTA	= MORB
CALL	READD(EVAL, EVEC, MX, MY, MZ, NSTA, INFILE)
IF (NSTA .GE. 0)	RETURN
!
!---------------------------------------------------------------------------
!
!	An error was found in the initial data file.
!	To be safe, re-initialize.
!
CALL	INIVECT
!
RETURN
!
97	PRINT *,'Bad mesh input file'
STOP
96	PRINT *,'Bad model input file'
STOP
99996	FORMAT(/,' Mesh      file = ',A64)
99994	FORMAT(  ' Output    file = ',A64)
99998	FORMAT(  ' Potential file = ',A64)
99993	FORMAT(' MX = ',I3,' MY = ',I3,' MZ = ',I3,' HR = ',F6.2)
99992	FORMAT(' ITIM = ',I3,I5,' STATES')
END SUBROUTINE PRESET
!
!============================================================================
!
DOUBLE PRECISION FUNCTION SPECT(K, M, ORDER)
!
!	Calculates the spectrum of the kinetic energy operator
!	for eigenvalue K in an M-point formula if a
!	ORDER-point derivative is used. Presently, second
!	derivative formulas up to 19 points are implemented.
!	If an even value of ORDER or a value larger than 19
!	is passed, the quadratic spectrum is used.
!	The return value must be scaled appropriately.
!
DOUBLE PRECISION, PARAMETER	:: PI	= 3.14159265358979323846D0
INTEGER	K,M, ORDER
DOUBLE PRECISION	XI
IF (ORDER .GT. 19) THEN
  SPECT	= (PI*MIN0(K,2*M-K)/(2*M))**2
  RETURN
END IF
XI	= DSIN(K*PI/(2*M))**2
IF	(ORDER .EQ. 3) THEN
  SPECT	= XI
ELSE IF	(ORDER .EQ. 5) THEN
  SPECT	= XI*(1.D0 + XI/3.D0)
ELSE IF	(ORDER .EQ. 7) THEN
  SPECT	= XI*(1.D0 + XI/3.D0 + 8.D0*XI**2/45.D0)
ELSE IF	(ORDER .EQ. 9) THEN
  SPECT	= XI*(1.D0 + XI/3.D0 + 8.D0*XI**2/45.D0 + 4.D0*XI**3/35.D0)
ELSE IF	(ORDER .EQ.11) THEN
  SPECT	= XI*(1.D0 + XI/3.D0 + 8.D0*XI**2/45.D0 + 4.D0*XI**3/35.D0 &
	+ 128.D0*XI**4/1575.D0)
ELSE IF	(ORDER .EQ.13) THEN
  SPECT	= XI*(1.D0 + XI/3.D0 + 8.D0*XI**2/45.D0 + 4.D0*XI**3/35.D0 &
	+ 128.D0*XI**4/1575.D0 + 128.D0*XI**5/2079.D0)
ELSE IF	(ORDER .EQ.15) THEN
  SPECT	= XI*(1.D0 + XI/3.D0 + 8.D0*XI**2/45.D0 + 4.D0*XI**3/35.D0 &
	+ 128.D0*XI**4/1575.D0 + 128.D0*XI**5/2079.D0 &
	+ 1024.D0*XI**6/21021.D0)
ELSE IF	(ORDER .EQ.17) THEN
  SPECT	= XI*(1.D0 + XI/3.D0 + 8.D0*XI**2/45.D0 + 4.D0*XI**3/35.D0 &
	+ 128.D0*XI**4/1575.D0 + 128.D0*XI**5/2079.D0 &
	+ 1024.D0*XI**6/21021.D0 + 256.D0*XI**7/6435.D0)
ELSE IF	(ORDER .EQ.19) THEN
  SPECT	= XI*(1.D0 + XI/3.D0 + 8.D0*XI**2/45.D0 + 4.D0*XI**3/35.D0 &
	+ 128.D0*XI**4/1575.D0 + 128.D0*XI**5/2079.D0 &
	+ 1024.D0*XI**6/21021.D0 + 256.D0*XI**7/6435.D0 &
	+ 32768.D0*XI**8/984555.D0)
ELSE
  PRINT *,'Warning: Invalid discretization order used.'
  PRINT *,'Taking quadratic spectrum. This may be bad.'
  ORDER	= 99
  SPECT	= (PI*MIN0(K,2*M-K)/(2*M))**2
END IF
RETURN
END FUNCTION SPECT
