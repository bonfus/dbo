USE	GLOBALS
IMPLICIT NONE
INCLUDE 'fftw3.f'
!
!	This is the global constants file of the 3D Schroedinger
!	equation code.
!
!	Useful parameters. Meaning is self-explanatory.
!
DOUBLE PRECISION, PARAMETER	:: THIRD= 1.D0/3.D0
DOUBLE PRECISION, PARAMETER	:: PI	= 3.14159265358979323846D0
DOUBLE PRECISION, PARAMETER	:: PI4	= 4.D0*PI
DOUBLE PRECISION, PARAMETER	:: PI43	= 4.D0*PI*THIRD
!
!	A few parameters that are needed all over:
!	Proper data alignment suggests that double precision
!	data in COMMON should appear first.
!
COMMON	/CFILE/	IINP, IOUT, IRES, &			! File numbers
		INFILE, POTFIL, OUTFIL, PREFIX  	! and names
COMMON	/CFLAG/	ESTP, ESTE, DELE, DELV, &		! Timesteps, errors
		EPSI, EPSR, RMUL, &	 		! error bounds
		TPRO, TGRA, &				! timings,
		MAXIM, &				! iteration limit
		IMSG					! message level
COMMON	/CMESH/	HR, HQX, HQY, HQZ, FRQ, FQR, &		! Discretizations
		MX, MY, MZ, MX2, MY2, MZ2, MXYZ		! mesh sizes
COMMON	/CCNS1/	H2M, MORB, NORB	 			! Physics constants
COMMON	/CPLAN/	PLAXK, PLAKX				! FFTW plans
COMMON	/CUSER/	RPAR(10), IPAR(10)			! User definable parameters
COMMON  /CINTR/ INTYPN, RBFPHI, RBFR0, &    ! interpolation parameters
                EPSPOS, QS3NQ,QS3NW,QS3NR
!
INTEGER			IINP, IOUT, IRES, MAXIM, NORB, MORB, IMSG
INTEGER			MX, MY, MZ, MX2, MY2, MZ2, MXYZ
INTEGER ::		IPAR
INTEGER*8		PLAXK, PLAKX
REAL (KIND(0.D0))  ::	ESTP, ESTE, DELE, DELV, &
			EPSI, EPSR, &
			HR, HQX, HQY, HQZ, FRQ, FQR, &
			H2M
REAL (KIND(0.D0))  ::	RMUL
REAL (KIND(0.D0))  ::	RPAR
CHARACTER (LEN=64) ::	INFILE, OUTFIL, POTFIL, PREFIX
REAL (KIND(0.0))   ::	TGRA, TPRO			! Timers must be real
! Added variables for interpolation
INTEGER :: INTYPN ! Interpolation type: 0 for RBF, 2 for Smooth trivariate, 
                 ! 9 do not interpolate but parse points directly.
INTEGER :: RBFPHI  ! Function  used as radial basis function.
REAL (KIND(0.D0)) :: RBFR0 !   $R_0$ used for radial basis function.
REAL (KIND(0.D0)) :: EPSPOS
INTEGER :: QS3NQ,QS3NW,QS3NR


!============================================================================
!
!	Definition of time-step ramp variables:
!
!	Typically, the imaginary time step iterations start
!	with an initial time step, run to convergence, then
!	reduce the time step, and so on until convergence
!	has been reached.
!
!	The code proceeds as follows:
!
!	(a)	Initialize time step ESTN=ESTP
!	(b)	Start at a given time step ESTP.
!	(b.1)	Run to convergence.
!	(b.2)	Reduce the time step by a factor RMUL
!	(b.3)	if RMUL*ESTP is larger than ESTE,
!		go back to (b.1).
!
!============================================================================
