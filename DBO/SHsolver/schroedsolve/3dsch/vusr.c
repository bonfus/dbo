#include <math.h>
#include <stdio.h>

/*------------------------------------------------------------------
 * user defined function defining the potential.
 * the arrays rpar and ipar are double/int arrays of length 10
 * that can be used to pass additional parameters from the Fortran
 * code.
 */
double myv(double x, double y, double z, double *rpar,
	   int *ipar, int ierr) {

  double v;

  v= rpar[0]*pow(x,ipar[0]) + rpar[1]*pow(y,ipar[1]) 
	 + rpar[2]*pow(z,ipar[2]);
  return(v);
}


/*------------------------------------------------------------------
 * C wrapper function that is called from fortran. In principle,
 * the user could define his potential here, but he would have
 * to mind to use (*x), (*y), (*z) instead of x,y,z throughout,
 * so a small wrapper seems to be the safer choice. 
 */
void vusr_(double *v, double *x, double *y, double *z, double *rpar, 
	    int *ipar, int *ierr) {

  *v = myv(*x,*y,*z,rpar,ipar,*ierr);
}

