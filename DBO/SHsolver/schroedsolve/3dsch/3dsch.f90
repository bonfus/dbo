!============================================================================
!
!	Master program for a simple 3D Schroedinger equation
!	solver for local potentials.
!
!============================================================================
!
PROGRAM SCH3D
#include "3dsch.inc"
!
INTEGER VALUE(8)
CHARACTER (LEN=16) ::	DATE, TIME, ZONE
CHARACTER (LEN=64) ::	EVAFILE
CHARACTER (LEN=64) ::	FMTSTR
CHARACTER (LEN= 1) ::	SEP(0:1)
!
REAL(KIND(1.0))	::	TTO0, TTOT, TPRT, TGRT
REAL(KIND(1D0))	::	ESTN
INTEGER			ITER, ITEP, IE
LOGICAL			FOURTH, FOPEN
DATA SEP                /"|"," "/
!
!----------------------------------------------------------------------------
!
!	First executable statement
!
CALL	GETARG(1,PREFIX)
!
!	Read and initialize discretization
!	parameters and physics constants.
!
CALL	PRESET
!
!	Select runtime parameters:
!	FOURTH:		Algorithm type
!
FOURTH	= .NOT.BTEST(IMSG,4)
!
!	Prepare graphics and results output.
!
!	Open a few files to monitor progress.
!	prefix.conv keeps track of errors and total energies
!	prefix.eval gives the normalization and expectation eigenvalues
!		after each iteration
!
WRITE (FMTSTR,99995) 2*NORB
EVAFILE	= TRIM(PREFIX)//".eval"
INQUIRE (FILE=EVAFILE,OPENED=FOPEN)
IF  (FOPEN)  CLOSE(IRES)
OPEN(IRES,FILE=EVAFILE,STATUS='REPLACE')
!
ITER	= 0
CALL	CPU_TIME(TTO0)					! Init global timer
CALL	DATE_AND_TIME(DATE,TIME,ZONE,VALUE)
WRITE (IOUT,99999) VALUE(3:1:-1), VALUE(5:7)
!
!----------------------------------------------------------------------------
!
!	Computation starts here:
!
ESTN	= ESTP/RMUL
!
!	The core of the calculation:
!	Calculate local external potential
!
CALL	VEXT	(FOURTH)
!
!	Propagate imaginary timestep iterations.
!	Pass iteration number to IMSTEP to indicate if we must initialize.
!	IMSTEP returns in ITEP the number of iterations that were needed
!	to reach the relative accuracy EPSI.
!
ITEP	= ITER
TGRT	= 0.					! Ortho timer
TPRT	= 0.					! Propagation timer
DO	WHILE	(ESTN .GT. ESTE+0.000001)
  ESTN	= DMAX1(RMUL*ESTN,ESTE)
  CALL	IMSTEP	(ESTN, ITEP, EPSI, FOURTH)
  TGRT	= TGRT + TGRA
  TPRT	= TPRT + TPRO
  WRITE   (IRES,FMT=FMTSTR) ITEP, ESTN, DELE, DELV, &
		(EVAL(IE), EVAR(IE), IE=1, NORB)
  CALL FLUSH (IRES)
  IF (ESTN .LE. ESTE + 0.000001)	EXIT
END DO
!
IF (BTEST(IMSG,3))	CALL	SAVED	(EVAL, EVEC, MX, MY, MZ, MORB, OUTFIL)
!
!=============================================================================
!
!	The serious stuff has been completed here.
!	The rest of this is just output.
!
!----------------------------------------------------------------------------
!
!	Timing and convergence:
!
CALL	CPU_TIME(TTOT)
TTOT	= TTOT - TTO0
WRITE (IOUT,*)
IF (BTEST(IMSG,2)) WRITE (IOUT,99998)	TTOT, TPRT, TGRT
!
!----------------------------------------------------------------------------
!
!	Resulting eigenvalues
!
WRITE (IOUT, 99997)	ESTN
WRITE (IOUT, 99996) NORB, MORB, &
	(EVAL(IE), SEP(MIN0(IABS(IE-NORB),1)), IE = 1, MORB)
WRITE (IOUT, '(A)') 'Energy in eV'
WRITE (IOUT, 99996) NORB, MORB, &
	(EVAL(IE)*27.21139, SEP(MIN0(IABS(IE-NORB),1)), IE = 1, MORB)
STOP
!
!----------------------------------------------------------------------------
!
99999	FORMAT (/" ======= 3D Schroedinger equation solver ", &
		" ==== Date: ",I2.2,"-",I2.2,"-",I4," ==== ", &
		"Time: ",I2.2,":",I2.2,":",I2.2," =======",/)
99998	FORMAT (' Timing:      T_tot =',F9.2,2X,'T_pro =',F9.2,2X, &
		'T_ort =',F9.2)
99997	FORMAT (/,' Converged | propagated states: ESTP = ',F7.4)
99996	FORMAT (' N = ',I3,1X,'E_n','(1:',I3,'):'&
 	,6(1X,F8.4,1X,A1):/(20X,6(1X,F8.4,1X,A1):))
99995	FORMAT ('(I5,F12.4,2ES12.3,',I3,'ES24.15)')
!
END PROGRAM SCH3D
