!===========================================================================
!
SUBROUTINE INIVECT
#include "3dsch.inc"
!
INTEGER, PARAMETER	:: IMAX=152
!
!	Initialize eigenvectors with "particle in a box"
!	wave functions. Let's just hope
!       that we never need more than 152 states.
!
DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:,:) :: FTX, FTY, FTZ
!
!	Local variables
!
INTEGER			IBOX(3, IMAX)		! Increase if we need more !
INTEGER			IE, IX, IY, IZ
REAL (KIND(0.D0))	DSQRT, DSIN

DATA	IBOX	/ &				! Ordered states in a box
  1, 1, 1,  2, 1, 1,  1, 2, 1,  1, 1, 2,  1, 2, 2, &
  2, 1, 2,  2, 2, 1,  1, 1, 3,  1, 3, 1,  3, 1, 1, &
  2, 2, 2,  1, 2, 3,  1, 3, 2,  2, 1, 3,  2, 3, 1, &
  3, 1, 2,  3, 2, 1,  2, 2, 3,  2, 3, 2,  3, 2, 2, &
  1, 1, 4,  1, 4, 1,  4, 1, 1,  1, 3, 3,  3, 1, 3, &
  3, 3, 1,  1, 2, 4,  1, 4, 2,  2, 1, 4,  2, 4, 1, &
  4, 1, 2,  4, 2, 1,  2, 3, 3,  3, 2, 3,  3, 3, 2, &
  2, 2, 4,  2, 4, 2,  4, 2, 2,  1, 3, 4,  1, 4, 3, &
  3, 1, 4,  3, 4, 1,  4, 1, 3,  4, 3, 1,  1, 1, 5, &
  1, 5, 1,  3, 3, 3,  5, 1, 1,  2, 3, 4,  2, 4, 3, &
  3, 2, 4,  3, 4, 2,  4, 2, 3,  4, 3, 2,  1, 2, 5, &
  1, 5, 2,  2, 1, 5,  2, 5, 1,  5, 1, 2,  5, 2, 1, &
  1, 4, 4,  2, 2, 5,  2, 5, 2,  4, 1, 4,  4, 4, 1, &
  5, 2, 2,  3, 3, 4,  3, 4, 3,  4, 3, 3,  1, 3, 5, &
  1, 5, 3,  3, 1, 5,  3, 5, 1,  5, 1, 3,  5, 3, 1, &
  2, 4, 4,  4, 2, 4,  4, 4, 2,  1, 1, 6,  1, 6, 1, &
  2, 3, 5,  2, 5, 3,  3, 2, 5,  3, 5, 2,  5, 2, 3, &
  5, 3, 2,  6, 1, 1,  1, 2, 6,  1, 6, 2,  2, 1, 6, &
  2, 6, 1,  3, 4, 4,  4, 3, 4,  4, 4, 3,  6, 1, 2, &
  6, 2, 1,  1, 4, 5,  1, 5, 4,  4, 1, 5,  4, 5, 1, &
  5, 1, 4,  5, 4, 1,  3, 3, 5,  3, 5, 3,  5, 3, 3, &
  2, 2, 6,  2, 6, 2,  6, 2, 2,  2, 4, 5,  2, 5, 4, &
  4, 2, 5,  4, 5, 2,  5, 2, 4,  5, 4, 2,  1, 3, 6, &
  1, 6, 3,  3, 1, 6,  3, 6, 1,  6, 1, 3,  6, 3, 1, &
  4, 4, 4,  2, 3, 6,  2, 6, 3,  3, 2, 6,  3, 6, 2, &
  6, 3, 2,  6, 2, 3,  6, 3, 3,  3, 4, 5,  3, 5, 4, &
  4, 3, 5,  4, 5, 3,  5, 3, 4,  5, 4, 3,  1, 1, 7, &
  1, 7, 1,  7, 1, 1,  1, 5, 5,  5, 1, 5,  5, 5, 1, &
  1, 4, 6,  1, 6, 4,  4, 1, 6,  4, 6, 1,  6, 1, 4, &
  6, 4, 1,  1, 2, 7,  1, 7, 2,  2, 1, 7,  2, 7, 1, &
  7, 2, 1,  7, 1, 2 /
!
IF (MORB .GT. IMAX) THEN
  WRITE (IOUT,99999) IMAX
  STOP
END IF
ALLOCATE	(FTX(-MX:MX,MORB), FTY(-MY:MY,MORB), FTZ(-MZ:MZ,MORB))
!
!	Initialize 1D-particle in a box states
!
DO	IE	= 1, MORB
  DO	IX	=-MX, MX
    FTX(IX,IE)	= DSIN(PI*IE*(IX+MX)/(2*MX))/DSQRT(1.D0*MX)
  END DO
  DO	IY	=-MY, MY
    FTY(IY,IE)	= DSIN(PI*IE*(IY+MY)/(2*MY))/DSQRT(1.D0*MY)
  END DO
  DO	IZ	=-MZ, MZ
    FTZ(IZ,IE)	= DSIN(PI*IE*(IZ+MZ)/(2*MZ))/DSQRT(1.D0*MZ)
  END DO
END DO
!
!	Preset wave functions with 3D particle-in-a-box states.
!
DO	IE	= 1, MORB
  DO	IZ	=-MZ, MZ-1
    DO	IY	=-MY, MY-1
      EVEC(-MX:MX-1,IY,IZ,IE)	= &
	FTX(-MX:MX-1,IBOX(1,IE))*FTY(IY,IBOX(2,IE))*FTZ(IZ,IBOX(3,IE))
    END DO
  END DO
END DO
DEALLOCATE (FTX, FTY, FTZ)
!
RETURN
99999   FORMAT ("This code has been built for no more than ",I3," states.")
END SUBROUTINE INIVECT
