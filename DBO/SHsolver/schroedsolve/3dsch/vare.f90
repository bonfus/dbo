!==============================================================================
!
SUBROUTINE	VARE
!
#include "3dsch.inc"
!
!	Local variables
!
DOUBLE PRECISION,   ALLOCATABLE, DIMENSION (:,:,:)	:: TMPR
COMPLEX(KIND(0d0)), ALLOCATABLE, DIMENSION (:,:,:)	:: TMPC
DOUBLE PRECISION	EVP, EKP
INTEGER			KX, KY, KZ, IEV
!
!------------------------------------------------------------------------------
!
!	Allocate workspace for FFTs
!
ALLOCATE	(TMPC(0:MX,0:2*MY-1,0:2*MZ-1))
ALLOCATE	(TMPR(-MX:MX-1,-MY:MY-1,-MZ:MZ-1))
!
!==============================================================================
!
!	Variational calculation of the eigenvalues by evaluating
!	<psi_i | H | psi_i>.
!
!	The convergence is as (dt)^8 but it gives only the
!	eigenvalues and not the eigenvectors to that accuracy.
!	It costs a little extra time, so it's for testing and
!	demonstration purposes only and may be skipped.
!
DO	IEV	= 1, MORB
  TMPR		= EVEC(:,:,:,IEV)
  EVP		= SUM(VONE(:,:,:)*TMPR**2)
  CALL DFFTW_EXECUTE_DFT_R2C(PLAXK,TMPR,TMPC)
  EKP		= 0.D0
  DO	KZ	= 0, 2*MZ-1
    DO	KY	= 0, 2*MY-1
      KX	= 0
      EKP	= EKP +      (T2X(KX)+T2Y(KY)+T2Z(KZ))*ABS(TMPC(KX,KY,KZ))**2
      DO	KX	= 1, MX-1
        EKP	= EKP + 2.D0*(T2X(KX)+T2Y(KY)+T2Z(KZ))*ABS(TMPC(KX,KY,KZ))**2
      END DO
      KX	= MX
      EKP	= EKP +      (T2X(KX)+T2Y(KY)+T2Z(KZ))*ABS(TMPC(KX,KY,KZ))**2
    END DO
  END DO
  EKP		= H2M*EKP/MXYZ
  EVAR(IEV)	= EKP + EVP
END DO
DEALLOCATE	(TMPC, TMPR)
!
RETURN
END SUBROUTINE VARE
