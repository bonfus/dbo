!==============================================================================
!
SUBROUTINE IMSTEP	(ESTN, ITEP, ERRP, FOURTH)
!
#include "3dsch.inc"
DOUBLE PRECISION	ESTN, ERRP
INTEGER			ITEP
LOGICAL			FOURTH
!
!	Local variables:
!	exp(-epsilon*V), exp(-epsilon*V'), exp(-epsilon*T)
!
REAL(KIND(0.D0)), ALLOCATABLE, DIMENSION (:,:,:) :: EXPV, EXDV, EXPT
!
!	Workspace for FFTs and [V,[T,V]]
!
REAL(KIND(0.D0)), ALLOCATABLE, DIMENSION (:,:,:) :: TMPR
COMPLEX (KIND(0.D0)), ALLOCATABLE, DIMENSION (:,:,:) :: TMPK
!
REAL(KIND(0.D0))	TVAL(MORB), EVA0(MORB), ERRE(MORB)
!
!			Progress output files:
!
CHARACTER (LEN=64) ::	EITFILE
CHARACTER (LEN=64) ::	FMTSTRNG
LOGICAL			FOPEN
REAL			TPR0, TPR, TGS0, TGS			! Timers
REAL(KIND(0.D0))	FACV, FACK, PSQ, DELT
INTEGER			KX, KY, KZ, ITIM, NST, IEV, I0, IER
!
!==============================================================================
!
!	Calculate the eigenvectors by imaginary time-step iterations,
!	using second or fourth order operator factorization.
!	Input is the target accuracy ERRP, the subroutine returns ITEP,
!	which is the number of propagations it took to get that accuracy.
!
!	As input parameter, ITEP gives the iteration number and may be
!	used to indicate the first call for initialization.
!
!	The flag FOURTH selects the algorithm.
!
!	Allocate work arrays
!
ALLOCATE	(EXPV(-MX:MX-1,-MY:MY-1,-MZ:MZ-1))
ALLOCATE	(EXDV(-MX:MX-1,-MY:MY-1,-MZ:MZ-1))
ALLOCATE	(TMPR(-MX:MX-1,-MY:MY-1,-MZ:MZ-1))
ALLOCATE	(EXPT(0:MX2-1, 0:MY2-1, 0:MZ2-1))
ALLOCATE	(TMPK(0:MX2-1, 0:MY2-1, 0:MZ2-1))
!
!------------------------------------------------------------------------------
!
!	Open a files to monitor eigenvalue convergence:
!	prefix.evit keeps the track of the eigenvalues during iteration
!
IF (ITEP.EQ.0)	THEN
  EITFILE	= TRIM(PREFIX)//".evit"
  INQUIRE (FILE=EITFILE,OPENED=FOPEN)
  IF	(FOPEN) CLOSE(IRES+1)
  OPEN (IRES+1,FILE=EITFILE,STATUS="REPLACE")
END IF
WRITE(FMTSTRNG,99999) 2*MORB
!
!------------------------------------------------------------------------------
!
IF (MORB .LE. 0)	RETURN		! should never happen.
TGRA	= 0.				! Timer for orthogonalization.
TPRO	= 0.				! Timer for propagation.
IF (FOURTH)	THEN			! Algorithm specific coefficients
  FACV	= ESTN/6.D0
  FACK	= ESTN/2.D0
ELSE
  FACV	= ESTN/2.D0
  FACK	= ESTN
END IF
!
!=============================================================================
!
!	Set up the parts of the Hamiltonian.
!
!	(A)	Kinetic energy:
!
DO	KZ	= 0, MZ2-1
  DO	KY	= 0, MY2-1
    DO	KX	= 0, MX2-1
      PSQ	= FACK*H2M*(T2X(KX)+T2Y(KY)+T2Z(KZ))
      EXPT(KX,KY,KZ)	= FRQ*FQR*DEXP(-PSQ)
    END DO
  END DO
END DO
!
!	(B)	Potential energy:
!
EXPV	= DEXP(-FACV*VONE(:,:,:))
IF (FOURTH)	THEN
  EXDV	= DEXP(-4.D0*FACV*(VONE(:,:,:) + ESTN**2*DVON(:,:,:)))
END IF
!
!==============================================================================
!
!	Imaginary time-step iteration loop.
! 
ERRE(1:MORB)	= 1.D0
EVA0(1:MORB)	= EVAL(1:MORB)
DO	ITIM	= 1, MAXIM
!
!	Propagate those vectors, whose eigenvalues have errors larger than ERRP.
!
  DO	I0	= 1, NORB
    IF (ERRE(I0) .GE. ERRP)	EXIT
  END DO
!
!	Keep the next line to prevent freezing states
!
! I0		= 1
  NST		= MORB-I0+1			! Number of states we propagate
  IF (NST .LE. 0)	EXIT
  CALL CPU_TIME(TPR0)				! Propagation time
  DO	IEV	= I0, MORB
    TMPR	= EXPV*EVEC(:,:,:,IEV)
    CALL DFFTW_EXECUTE_DFT_R2C (PLAXK, TMPR, TMPK)
    TMPK	= EXPT*TMPK
    CALL DFFTW_EXECUTE_DFT_C2R (PLAKX, TMPK, TMPR)
!
!	Do this only if we use the fourth order algorithm.
!	Second order can be tried for testing purposes.
!
!------------------------------------------------------------------------------
!
    IF (FOURTH) THEN
      TMPR	= EXDV*TMPR
      CALL DFFTW_EXECUTE_DFT_R2C (PLAXK, TMPR, TMPK)
      TMPK	= EXPT*TMPK
      CALL DFFTW_EXECUTE_DFT_C2R (PLAKX, TMPK, TMPR)
    END IF
!
!------------------------------------------------------------------------------
!
    EVEC(:,:,:,IEV)	= EXPV*TMPR
  END DO
  CALL	CPU_TIME(TPR)
!
!------------------------------------------------------------------------------
!
!	Orthogonalize the states:
!	Separating the stepsize from the orthogonalization
!	makes the orthogonalization code independent
!	of the propagation method.
!
  CALL	CPU_TIME(TGS0)
  TVAL(1:I0-1)	= DEXP(-ESTN*EVAL(1:I0-1))
  CALL ORTHO(EVEC, TVAL, I0, MORB, MXYZ, IER)
!
!       Error warning
!
  IF (IER.NE.0) THEN
    PRINT *,'***** Warning *****'
    PRINT *,'***** The overlap matrix is not positive definite.'
    PRINT *,'***** This is likely due to rounding errors and will go away', &
        '      with smaller time steps.'
    PRINT *,'***** Program continuing with reduced time step, but be careful !'
  END IF
  TVAL(1:MORB)	=-DLOG(TVAL(1:MORB))/ESTN
  CALL	CPU_TIME(TGS)
!
  TGS   	= TGS - TGS0                    ! Orthogonalization time
  TPR		= TPR - TPR0			! Propagation time
  TPRO		= TPRO + TPR
!
!------------------------------------------------------------------------------
!
!	Error estimates. Only necessary for occupied states.
!
  ERRE(I0:MORB)	= DABS(EVAL(I0:MORB)-TVAL(I0:MORB))
  DELT		= DSQRT(SUM(ERRE(1:NORB)**2)/SUM(TVAL(1:NORB)**2))
!
  TGRA	= TGRA + TGS				! Time for orthogonalizing
!
  CALL VARE
  DELV	= DSQRT(SUM(EVAL(1:NORB)-EVAR(1:NORB))**2) &
        / SUM(TVAL(1:NORB)**2)
  IF (BTEST(IMSG,1))	THEN
    WRITE(IOUT,99998)	ITIM, I0, ESTN, TPR, TGS, DELT, DELV
    CALL FLUSH(IOUT)
  END IF
  EVAL(I0:MORB)	= TVAL(I0:MORB)
  WRITE (IRES+1,FMT=FMTSTRNG)	ITIM, ESTN, DELT, DELV, &
        (EVAL(IEV),EVAR(IEV),IEV=1,NORB)
  CALL FLUSH (IRES+1)
!
!       IER .NE. 0 can happen only of the overlap matrix is
!       not positive. This can only be due to rounding errors.
!       It normally goes away by reducing the time step, if it
!       does not, something is seriously wrong.
!
  IF (DELT .LT. DMAX1(EPSR*DELV,ERRP) .OR. IER.NE.0)	EXIT
END DO
IF (BTEST(IMSG,0) .AND.(.NOT.BTEST(IMSG,1)))	THEN
  WRITE(IOUT,99998)	ITIM, I0, ESTN, TPR, TGS, DELT, DELV
  CALL FLUSH(IOUT)
END IF
ITEP    = ITIM
!
!------------------------------------------------------------------------------
!
!	All done. Do the timing, test-output and bookeeping.
!
DELE	= DSQRT(SUM(EVA0(1:NORB)-EVAL(1:NORB))**2) &
        /SUM(EVAL(1:NORB)**2)
WRITE	(IRES+1,*)
CALL FLUSH (IRES+1)
!
DEALLOCATE	(EXPV, EXDV, EXPT, TMPR, TMPK)
RETURN
99999	FORMAT('(I5,F8.4,2ES12.3,',I3,'F14.9)')
99998	FORMAT(2I5,3F9.3,2ES12.3)
END SUBROUTINE IMSTEP
