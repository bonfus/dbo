!============================================================================
!
SUBROUTINE ORTHO (EVEC, TVAL, IE0, MAXST, MXYZ, IERR)
!
!----------------------------------------------------------------------------
!
!	This orthogonalizes the eigenvectors by diagonalizing
!	the square of the transition operator, whatever it is.
!	The method converges faster than Gram-Schmidting.
!	It costs in practice no more time because this type
!	of problems is apparently very well suited for the
!	Winograd-Strassen algorithm for matrix multiplication.
!
!	It returns an orthonormalized set of states and estimates
!	for the new eigenvalues of the transition operator in the
!	array TVAL. The eigenvalues must be translated into
!	the physical eigenvalues in the calling program. This
!	strategy allows us to use the same orthogonalization
!	routine for both the imaginary timestep and inverse
!	iteration algorithms.
!
!       The error flag is set if the overlap matrix is not
!       strictly positive definite. This can only be caused
!       by rounding errors. Errors that are detected by the LAPACK
!       routine DSYEV are more serious and will lead to an abort.
!
!	This routine is identical in 2D and 3D.
!
!	Parameters:
!		EVEC		: Eigenvectors			(input/output)
!		TVAL		: Eigenvalues			(input/output)
!		IE0		: Number of "frozen" state	(input)
!		MAXST		: Number of eigenvectors	(input)
!		MXYZ		: Lenght of eigenvectors	(input)
!               IERR            : Error message.
!                               
!
!----------------------------------------------------------------------------
!
IMPLICIT NONE
!
DOUBLE PRECISION, INTENT(INOUT)		:: EVEC(MXYZ, MAXST)	! Eigenvectors
DOUBLE PRECISION, INTENT(INOUT)		:: TVAL(MAXST)		! Eigenvalues
INTEGER, INTENT(IN)			:: IE0, MAXST, MXYZ
INTEGER, INTENT(OUT)                    :: IERR                 ! Error flag
!
!----------------------------------------------------------------------------
!
!	Local quantities
!
INTEGER, PARAMETER ::	LWORK=2000
DOUBLE PRECISION	TTMP
!
!	Workspace for eigenvalue solver
!
DOUBLE PRECISION	WORK(LWORK)
!
!	Workspace for temporary eigenvectors
!
DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:,:) :: TVEC
!
!	Transition matrix
!
DOUBLE PRECISION, ALLOCATABLE, DIMENSION (:,:) :: TMAT
INTEGER		IP(1)           				! Pivot vector
INTEGER		NST, IE
!
!----------------------------------------------------------------------------
!
ALLOCATE (TMAT(MAXST,MAXST))
!
!	Set up matrix of overlap integrals.
!	Negative sign to have ascending eigenvalues.
!
!	Number of states we propagate
!
NST		= MAXST - IE0 + 1
!
!	First orthogonalize the propagated states onto the frozen states.
!
IF (IE0 .GT. 1) THEN
  CALL  DGEMM('T','N', IE0-1, NST, MXYZ,-1.D0, EVEC(:,1), MXYZ, &
        EVEC(:,IE0), MXYZ, 0.D0, TMAT, MAXST)
  CALL  DGEMM('N','N', MXYZ, NST, IE0-1, 1.D0, EVEC, MXYZ, TMAT, &
	 MAXST, 1.D0, EVEC(:,IE0), MXYZ)
END IF
!
!	Now we have the propagated states orthogonal to the frozen ones.
!	Calculate the overlap integrals.
!
CALL    DSYRK('L','T', NST, MXYZ, -1.D0, EVEC(:,IE0), MXYZ, 0.D0, &
        TMAT, MAXST)
!
!----------------------------------------------------------------------------
!
!	Transform the eigenvectors to the new, orthogonal basis.
!
CALL DSYEV('V','L', NST, TMAT, MAXST, TVAL(IE0), WORK, LWORK, IERR)
IF (IERR .NE. 0)        THEN
  PRINT 99999,IERR
  STOP
END IF
IERR    = 0
!
!       This is an emergency brake. In principle, all eigenvalues
!       of the overlap matrix should be positive, but numerical
!       rounding errors might cause one to be slightly positive.
!       The routine itself catches this by taking the absolute
!       value, but the user should issue a warning and see if
!       the problem persists.
!
IF (MAXVAL(TVAL(IE0:MAXST)) .GT. 0)  IERR    = -1
DO	IE	= 1, NST
  TMAT(1:NST,IE)	= TMAT(1:NST,IE)/DSQRT(DABS(TVAL(IE+IE0-1)))
END DO
TVAL(IE0:MAXST)		= DSQRT(DABS(TVAL(IE0:MAXST)))
!
!----------------------------------------------------------------------------
!
!	This will bomb if not enough space for a duplicate
!	of the eigenvectors could be allocated. But we should
!	expect that many eigenvectors will be done only on
!	a big machine anyway.
!
ALLOCATE (TVEC(MXYZ,MAXST))
CALL DGEMM('N','N', MXYZ, NST, NST, 1.D0, EVEC(:,IE0), MXYZ, TMAT, &
	MAXST, 0.D0, TVEC(:,IE0), MXYZ)
EVEC(:,IE0:MAXST)	= TVEC(:,IE0:MAXST)
DEALLOCATE (TVEC,TMAT)
!
!----------------------------------------------------------------------------
!
!	Sorting might be necessary if not all states have been
!	propagated, and the order of eigenvalues has been switched.
!	Simple binary sort is sufficient because most eigenvalues will
!	be ordered anyway.
!
DO	IE	= 1, MAXST-1
  IP(1:1)	= MAXLOC(TVAL(IE:MAXST)) + IE - 1
  IF (IP(1) .NE. IE)    THEN
    TTMP	= TVAL(IP(1))
    TVAL(IP(1))	= TVAL(IE)
    TVAL(IE)	= TTMP
    CALL	DSWAP(MXYZ, EVEC(:,IP(1)), 1, EVEC(:,IE),1)
  END IF
END DO
!
!----------------------------------------------------------------------------
!
RETURN
99999   FORMAT ("Lapack error",I5," in DSYEV. Program halted.")
END SUBROUTINE ORTHO
