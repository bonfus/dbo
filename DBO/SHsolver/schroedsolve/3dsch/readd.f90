SUBROUTINE READD(EVAL, EVEC, MX, MY, MZ, MORB, FNAME)
!
REAL (KIND(0.D0)), INTENT(OUT)	:: EVAL(1:MORB)
REAL (KIND(0.D0)), INTENT(OUT)	:: EVEC(-MX:MX-1,-MY:MY-1,-MZ:MZ-1,1:MORB)
INTEGER, INTENT(IN) 		:: MX, MY, MZ
INTEGER, INTENT(INOUT) 		:: MORB
CHARACTER(LEN=*), INTENT(IN)	:: FNAME
!
!	Local variables
!
INTEGER, PARAMETER ::	IOUT  = 6
INTEGER, PARAMETER ::	IOLD  = 9
INTEGER			IX, IY, IZ, IE
INTEGER*4		NX, NY, NZ, NORB
LOGICAL			EX
!
!	Read as many wave function as possible from a binary
!	data file. It expects that the wave functions
!	EVEC(:,:,:,:)	have been initialized somehow.
!
!	This does the following:
!	The program first checks if the mesh is the same as
!	the one defined in the calling program. If not, it
!	returns without doing anything. If the mesh is correct
!	the program reads as many wave functions as found
!	in the file and overwrites the initialized values.
!	The variable MORB contains the number of
!	records that were successfully read.
!
!	If an error occurs durinr read, the program returns
!	with MORB=-1. It is then possible that the data
!	in the fields EVAL(:) and EVEC(:,:,:,:) have been
!	corrupted, and the user should re-initialize them.
!
!	Different compilers have different conventions in
!	short and long integers. To be definite, integers
!	in the binary data file are always long.
!
OPEN	(IOLD,FILE=FNAME,ACTION='READ',STATUS='OLD',FORM='UNFORMATTED',ERR=99)
READ	(IOLD,ERR=98) NX, NY, NZ, NORB
IF (NX.NE.MX .OR. NY.NE.MY .OR. NZ.NE.MZ)	THEN
  WRITE (IOUT,99998) TRIM(FNAME)
  INQUIRE (IOLD, OPENED=EX)
  IF (EX)	CLOSE(IOLD)
  MORB		= 0
  RETURN
END IF
MORB		= MIN0(INT(NORB),MORB)
DO		IE	= 1, MORB
  READ	(IOLD,ERR=98)	EVAL(IE)
  DO		IZ	=-MZ, MZ-1
    DO		IY	=-MY, MY-1
!
!	It seems the construct (-MX:MX-1) does not work on some of the
!	the Intel compilers for long records.
!
      READ (IOLD,ERR=98) (EVEC(IX,IY,IZ,IE),IX=-MX,MX-1)
    END DO
  END DO
END DO
CLOSE	(IOLD)
!
RETURN
!
!	Error exit for wrong mesh
!
99	WRITE (IOUT,99999) TRIM(FNAME)
  INQUIRE (IOLD, OPENED=EX)
  IF (EX)	CLOSE(IOLD)
  MORB	= 0
RETURN
!
!	Error exit for corrupted file
!
98	WRITE (IOUT,99997) TRIM(FNAME)
  INQUIRE (IOLD, OPENED=EX)
  IF (EX)	CLOSE(IOLD)
  MORB	=-1
RETURN
99999	FORMAT (' Cannot open binary input data file ',(A))
99998	FORMAT (' Inconsistent mesh in binary input data file ',(A))
99997	FORMAT (' Error reading binary output data file ',(A), &
	' Re-initialize !')
END SUBROUTINE READD
!============================================================================
!
SUBROUTINE SAVED(EVAL, EVEC, MX, MY, MZ, MORB, FNAME)
!
!	Save the results for graphics output.
!
REAL (KIND(0.D0)), INTENT(IN)	:: EVAL(1:MORB)
REAL (KIND(0.D0)), INTENT(IN)	:: EVEC(-MX:MX-1,-MY:MY-1,-MZ:MZ-1,1:MORB)
INTEGER, INTENT(IN) 		:: MX, MY, MZ, MORB
CHARACTER(LEN=*), INTENT(IN)	:: FNAME
!
!	Local variables
!
INTEGER, PARAMETER ::	IOUT  = 6
INTEGER, PARAMETER ::	INEW  = 9
INTEGER			IX, IY, IZ, IE
!
!	Dump all wave functions into the output file as well.
!	This generates a huge file and should normally be turned off.
!	Different compilers have different conventions in
!	short and long integers. To be definite, always write long
!	integers.
!
OPEN	(INEW,FILE=FNAME,FORM='UNFORMATTED',ERR=99)
WRITE	(INEW) INT4(MX), INT4(MY), INT4(MZ), INT4(MORB)
DO		IE	= 1, MORB
  WRITE (INEW)        EVAL(IE)
  DO		IZ	=-MZ, MZ-1
    DO		IY	=-MY, MY-1
!
!	It seems the construct (-MX:MX-1) does not work on some of the
!	the Intel compilers for long records.
!
      WRITE (INEW) (EVEC(IX,IY,IZ,IE),IX=-MX,MX-1)
    END DO
  END DO
END DO
CLOSE	(INEW)
!
RETURN
99	WRITE (IOUT,99999) TRIM(FNAME)
STOP
99999	FORMAT (' Cannot open binary output data file ',(A))
END SUBROUTINE SAVED
