!============================================================================
!
SUBROUTINE VEXT (FOURTH)
USE interpolator
#include "3dsch.inc"
!
LOGICAL	FOURTH
!
!	Local variables
!
INTEGER			IX, IY, IZ, KX, KY, KZ, IER, L, I, NP
REAL (KIND(0.D0)) ::	V
REAL (KIND(0.D0)) ::	FACT, X, Y, Z, PSQ
REAL (KIND(0.D0)), ALLOCATABLE, DIMENSION (:,:,:)	:: TMPR
COMPLEX (KIND(0.D0)), ALLOCATABLE, DIMENSION (:,:,:)	:: TMPC
REAL (KIND(0.D0)) , ALLOCATABLE, DIMENSION (:,:)	:: inpoints
REAL (KIND(0.D0)) , ALLOCATABLE, DIMENSION (:)	:: outvals
REAL (KIND(0.D0)) , ALLOCATABLE, DIMENSION (:,:)	:: potpoints
REAL (KIND(0.D0)) , ALLOCATABLE, DIMENSION (:)	:: potvals

!
!----------------------------------------------------------------------------
!
!	Generate the local potential.
!
!	The user must supply a double precision function
!	VUSR(X,Y,Z,RPAR,IPAR,IER)
!	that returns the value of the potential at space
!	point (X,Y,Z).
!
!	The program passes a vector RPAR of real numbers
!	and a vector IPAR of integer numbers to the
!	subroutine that may be used to characterize the
!	model. An error flag IER may be used to trap
!	any errors within this routine, but the better
!	strategy is, of course, to stop the program
!	right there.
!

IER	= 0
VONE(:,:,:)	= 0.D0
allocate (inpoints(3,2*MZ*2*MY*2*MX))
allocate (outvals(2*MZ*2*MY*2*MX))

l=1

DO	IZ	=-MZ, MZ-1
  Z		= HR*IZ
  DO	IY	=-MY, MY-1
    Y		= HR*IY
    DO	IX	=-MX, MX-1
      X		= HR*IX
        inpoints(1,l) = X
        inpoints(2,l) = Y
        inpoints(3,l) = Z
        l = l + 1
        !CALL VUSR(V,X,Y,Z,RPAR,IPAR,IER)
        !VONE(IX,IY,IZ) = H2M*V
    END DO
  END DO
END DO

!CALL VUSR(V,X,Y,Z,RPAR,IPAR,IER)



OPEN	(37,FILE=POTFIL,ACTION='READ',ERR=98)

READ	(37,*,ERR=98) NP

NP=INT(NP)
IF (NP < 0 )	THEN
  RETURN
END IF

allocate (potpoints(3,NP))
allocate (potvals(NP))

l=1
DO		I	= 1, NP
  READ	(37,*,ERR=98) X, Y, Z, V
        potpoints(1,l) = X
        potpoints(2,l) = Y
        potpoints(3,l) = Z  
        potvals(l) = V
        l = l+1
END DO

CLOSE	(37)
IF ( INTYPN < 9 ) THEN 

  write(*,'(A,I2)') 'Interpolating using method', INTYPN
  IF ( (INTYPN .ne. 0)  .and. (INTYPN .ne. 2) ) THEN
    write(*,*) 'Interpolation method not implemented, switching to 2.'
    INTYPN = 2
  END IF
  IF ( INTYPN .eq. 0 ) CALL set_interp_params(INTYPN ,irbf_r0 = RBFR0,iphitype=RBFPHI)
  IF ( INTYPN .eq. 0 ) CALL interpolate( 3, np , potpoints, potvals, 2*MZ*2*MY*2*MX, inpoints, INTYPN, outvals )
  IF ( INTYPN .eq. 2 ) CALL set_interp_params(INTYPN ,iqs3_nq = QS3NQ , iqs3_nr =QS3NR , iqs3_nw = QS3NW )
  IF ( INTYPN .eq. 2 ) CALL interpolate( 3, np , potpoints, potvals, 2*MZ*2*MY*2*MX, inpoints, INTYPN, outvals )
  write(*,*) 'Done interpolating.'
  !write(*,*) VONE(1:10,1:10,1:10)
  
  l=1
  DO	IZ	=-MZ, MZ-1
    Z		= HR*IZ
    DO	IY	=-MY, MY-1
      Y		= HR*IY
      DO	IX	=-MX, MX-1
        X		= HR*IX
        VONE(IX,IY,IZ) = outvals(l)
        l = l + 1
      END DO
    END DO
  END DO
  
ELSE
  l=1
  DO	IZ	=-MZ, MZ-1
    Z		= HR*IZ
    DO	IY	=-MY, MY-1
      Y		= HR*IY
      DO	IX	=-MX, MX-1
        X		= HR*IX
        IF ( ABS(potpoints(1,l)-X) < EPSPOS .and. &
            ABS(potpoints(2,l)-Y) < EPSPOS .and. &
            ABS(potpoints(3,l)-Z) < EPSPOS ) THEN
          VONE(IX,IY,IZ) = potvals(l)
        ELSE
          write(*,*) 'Position: ',   potpoints(:,l) , 'does not match the grid point: ', X, Y, Z
        END IF
        l = l + 1
      END DO
    END DO
  END DO  
END IF
!write(*,*) VONE(1:10,1:10,1:10)

deallocate(inpoints,outvals,potpoints,potvals)

IF (IER.NE.0)	THEN
WRITE (IOUT,99999)	IER, X, Y, Z
STOP
END IF

!
!	Check if the attractive part of the potemtial is
!	too large such that it can cause overflows.
!	This just gives a warning.
!
IF (ESTP*MINVAL(VONE).LT.-0.5*DLOG(HUGE(1.D0)))	THEN
  PRINT *,'**** Your potential is too attractive for the numerical model.'
  PRINT *,'**** This may cause floating overflows.'
  PRINT *,'**** Use an offset or different scalings.'
END IF
IF (.NOT.FOURTH)	RETURN
!
!
!------------   [V,[T,V]]/48    ---------------------------------------------
!
!	If we use the fourth--order algorithm, then generate
!	the [V,[T,V]] term.
!
!	This version does the derivatives by the specific
!	formula that is consistent with the 4th order algorithm.
!	It is assumed that the function is periodic.
!
!	We do this in momentum space
!	because then we can use any order derivative formula.
!
FACT		= H2M*FRQ*FQR/48.D0
ALLOCATE	(TMPC(  0:MX  ,0:2*MY-1,0:2*MZ-1))
ALLOCATE	(TMPR(-MX:MX-1,-MY:MY-1,-MZ:MZ-1))
TMPR		= VONE
CALL DFFTW_EXECUTE_DFT_R2C (PLAXK, TMPR, TMPC)
DO	KZ	= 0, 2*MZ-1
  DO	KY	= 0, 2*MY-1
    DO	KX	= 0, MX
      PSQ	= T2X(KX)+T2Y(KY)+T2Z(KZ)
      TMPC(KX,KY,KZ)    = PSQ*TMPC(KX,KY,KZ)
    END DO
  END DO
END DO
CALL DFFTW_EXECUTE_DFT_C2R (PLAKX, TMPC, TMPR)
DVON(:,:,:)	= 2.D0*VONE(:,:,:)*TMPR
TMPR		= VONE(:,:,:)**2
CALL DFFTW_EXECUTE_DFT_R2C (PLAXK, TMPR, TMPC)
DO	KZ	= 0, 2*MZ-1
  DO	KY	= 0, 2*MY-1
    DO	KX	= 0, MX
      PSQ	= T2X(KX)+T2Y(KY)+T2Z(KZ)
      TMPC(KX,KY,KZ)	= PSQ*TMPC(KX,KY,KZ)
    END DO
  END DO
END DO
CALL DFFTW_EXECUTE_DFT_C2R (PLAKX, TMPC, TMPR)
DVON(:,:,:)= FACT*(DVON(:,:,:)- TMPR)
!
DEALLOCATE (TMPR, TMPC)
RETURN
99999	FORMAT ("User defined potemtial returned error ",I3, &
		"at point (",F7.3,",",F7.3,",",F7.3,")")
98  PRINT *, "Error reading POT file!"        
STOP
END SUBROUTINE VEXT
