\documentclass[report]{elsart}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amssymb,amsmath}
\usepackage{epsfig}
\usepackage{bm}
\usepackage{hyperref}
\usepackage{calc}
\def\comment#1{\bigskip\hrule\smallskip#1\smallskip\hrule\bigskip}
\tolerance=10000

%% This list environment is used for the references in the
%% Program Summary
%%
\newcounter{bla}
\newenvironment{refnummer}{%
\list{[\arabic{bla}]}%
{\usecounter{bla}%
 \setlength{\itemindent}{0pt}%
 \setlength{\topsep}{0pt}%
 \setlength{\itemsep}{0pt}%
 \setlength{\labelsep}{2pt}%
 \setlength{\listparindent}{0pt}%
 \settowidth{\labelwidth}{[9]}%
 \setlength{\leftmargin}{\labelwidth}%
 \addtolength{\leftmargin}{\labelsep}%
 \setlength{\rightmargin}{0pt}}}
 {\endlist}

\clubpenalty=9999
\widowpenalty=9999


\begin{document}
\begin{frontmatter}
\vspace {2truein}
\title{
A fast and simple program for solving local Schr\"odinger
equations in two and three dimensions}

\author{ S. Janecek and E. Krotscheck}
\address{Institut f\"ur Theoretische Physik, Johannes Kepler
Universit\"at Linz, A-4040 Linz, Austria}

\date{\today}
\begin{abstract}
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%	
%%	Abstract
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

We describe a simple and rapidly converging code for solving
local Schr\"odinger equations in two and three dimensions.
Our method utilizes a fourth-order factorization of the
imaginary time evolution operator which improves the convergence
rate by one to two orders of magnitude compared with a second-order
Trotter factorization. We present the theory behind the method
and strategies for assessing convergence and accuracy.

Our code requires one user defined function which specifies the local
external potential. We describe the definition of this function as
well as input and output functionalities.

\end{abstract}
\end{frontmatter}

% Computer program descriptions should contain the following
% PROGRAM SUMMARY.

{\bf PROGRAM SUMMARY}
  %Delete as appropriate.

\begin{small}
\noindent
{\em Program Title:} 3dsch/2dsch                              \\
{\em Journal Reference:}                                      \\
  %Leave blank, supplied by Elsevier.
{\em Catalogue identifier:}                                   \\
  %Leave blank, supplied by Elsevier.
{\em Licensing provisions:}                                   \\
  %enter "none" if CPC non-profit use license is sufficient.
{\em Programming language:} Fortran 90                        \\
\\
{\em Computer:} Tested on x86, amd64, Itanium2, and MIPS
architectures.
Should run on any architecture providing a Fortran 90 compiler. \\
  %Computer(s) for which program has been designed.
\\
{\em Operating system:} So far tested under UNIX/Linux and Irix.
Again, any OS with a Fortran 90 compiler available should suffice.\\
  %Operating system(s) for which program has been designed.
\\
{\em RAM:}  2MB to 16GB, depending on system size.\\
  %RAM in bytes required to execute program with typical data.
\\
{\em Supplementary material:}                                 \\
  Sample input files for the 2D and the 3D version as well as
  a {\rm gnuplot\/} script for assessing convergence.
\\
{\em Keywords:} Schr\"odinger equation, Diffusion algorithm,
operator factorizations. \\
  % Please give some freely chosen keywords that we can use in a
  % cumulative keyword index.
\\
{\em PACS:} 02.70.-c, 31.15.-p                                \\
  % see http://www.aip.org/pacs/pacs.html 
\\
{\em Classification:}                                         \\
  %Classify using CPC Program Library Subject Index, see (
  % http://cpc.cs.qub.ac.uk/subjectIndex/SUBJECT_index.html)
  6.10 Wave Functions and Integrals, 7.3 Electronic Structure \\
\\
{\em External routines/libraries:} FFTW3, Lapack. \\
  % Fill in if necessary, otherwise leave out.
\\
{\em Nature of problem:}\\
  Numerical calculation of low-lying states of 2D and 3D local Schr\"odinger
  equations in configuration space. \\
  %Describe the nature of the problem here.
   \\
{\em Solution method:}\\
  4$^{\rm th}$ order factorization of the diffusion operator. \\
   \\
{\em Restrictions:}\\
  The code is at this time designed for up to 152 states in 3D and
  for up to 100 states in 2D. This number can easily be increased
  by generating more trial states in the initialization routine. \\
   \\
{\em Running time:} Seconds to hours, depending on system size. \\
   \\
{\em References:}
\begin{refnummer}
\item S. A. Chin, Phys. Lett A 226, (1997) 344.
\item J. Auer, S. A. Chin, and E. Krotscheck, J. Chem. Phys. 114 (2001) 7338.
\item M. Aichinger and E. Krotscheck, Comp. Mat. Sci. 34 (2005) 188
\end{refnummer}

\end{small}

\newpage

% In program descriptions the main text of the paper is listed under
% the heading LONG WRITE-UP.

\hspace{1pc}
{\bf LONG WRITE-UP}

\
\section {Introduction}

Central to many problems in quantum physics is the solution of the
Schr\"odinger equation of a particle in a local, external
potential. The problem has, for a long time, been a difficult one
simply because of the sheer number of variables needed if one wants to
represent, in three dimensions, a wave function on a regular grid.  In
3-D, the number of mesh points can easily exceed $N\sim 10^6$,
rendering direct diagonalization methods, with computational effort
proportional to $N^3$, impractical.  However, even the smaller
personal computers of the past decade have now enough memory that the
storage of real-space representations of one or several wave functions
is no longer a problem. This makes, as we shall see, the solution of
the Schr\"odinger equation on a regular grid in coordinate space
surprisingly simple.

Our method of choice is to evolve the system in imaginary time. We
have shown in previous work that the use of fourth order
factorizations of the evolution operator
\cite{Suzuki96,ChinPLA97,Chinjcp01} produces rapidly converging
algorithms for solving the Schr\"odinger equation.  The method has so
far been implemented, in a computationally much more complex
environment, in the design of a new density functional code for
metallic clusters and molecules for both local and non-local
potentials, quantum dots, and in arbitrarily strong magnetic fields
\cite{imstep,cluster,qrings,nonloc}. This communication provides
an easy-to-use and user-friendly
implementation of the core functionality of that code. Its
purpose is the calculation of low-lying bound-state or periodic
solutions of the two- and three-dimensional Schr\"odinger equation.

\section{Algorithm}
\subsection {Operator Factorizations}

We consider the problem of solving the one--body
Schr\"odinger equation
\begin{equation}
H({\bf r})\psi_j({\bf r}) = E_j\psi_j({\bf r})
\end{equation}
for a {\it local\/} potential, {\it i.e.\/} the Hamiltonian
has the form
\begin{equation}
H({\bf r})= T({\bf r}) + V({\bf r})
= -\frac{\hbar^2}{ 2m}\nabla^2 + V({\bf r})\,.
\label{eq:Hamiltonian}
\end{equation}

The solutions corresponding to the lowest $n$ energy eigenvalues can
be obtained as follows: one starts with a set of trial vectors
$\{\psi_j^{(k=0)}({\bf r}), j = 1,\ldots, n\}$.
One then applies
the {\em evolution operator\/}
\begin{equation}
\mathcal{T}(\epsilon)  \equiv e^{-\epsilon H}
\label{eq:evolutionOperator}
\end{equation}
on this set of trial states
\begin{equation}
\phi_j^{(k+1)}({\bf r}) \equiv \mathcal{ T}(\epsilon) \psi_j^{(k)}({\bf r})\,.
\label{eq:iterations}
\end{equation}
The states $\{\phi_j^{(k+1)}({\bf r})\}$ are then orthonormalized
which produces a new set of trial states
$\{\psi_j^{(k+1)}({\bf r})\}$. The procedure is repeated until the set
of eigenvalues and corresponding eigenfunctions have converged.

Since the evolution operator (\ref{eq:evolutionOperator}) cannot be
calculated exactly for a Hamiltonian (\ref{eq:Hamiltonian}), one must
approximate $\mathcal{T}(\epsilon)$. A popular approximation is the
famous ``Trotter formula''
\begin{equation}
\mathcal{T}^{(2)}(\epsilon) \equiv e^{-\frac{1}{2}\epsilon V}
e^{-\epsilon T} e^{-\frac{1}{2}\epsilon V} = 
 \mathcal{T}(\epsilon)  + \mathcal{O}(\epsilon^3)\,.
\label{eq:D2A}
\end{equation}

With this factorization one has replaced the exact Hamiltonian by an
approximate Hamiltonian; the induced error depends on the size of the
time step $\epsilon$. When $\epsilon$ is small, the induced error is
small, but many iterations of the above are needed for convergence.
When $\epsilon$ is large, the iterations converge more quickly, but
towards a poorer approximation of the correct eigenvalue/eigenfunction
pairs. Thus, the method is, while robust and stable, rather time
consuming.

The performance of the diffusion method can be greatly improved by
using fourth order factorizations of the evolution operator. Several
of such factorizations have been derived by Suzuki \cite{Suzuki96} and
Chin \cite{ChinPLA97}; we have studied these methods extensively in
previous work \cite{imstep,cluster}. We found that the overall
performance of the different versions is comparable, some fine-tuning
is possible but for the sake of simplicity we have opted here for the
implementation of just one version \cite{Suzuki96,ChinPLA97}, namely
\begin{equation}
\mathcal{T}^{(4)}(\epsilon)
\equiv 
  {\rm e}^{-\frac{1}{6}\epsilon V}
  {\rm e}^{-\frac{1}{2}\epsilon T} 
  {\rm e}^{-\frac{2}{3}\epsilon \widetilde V } 
  {\rm e}^{-\frac{1}{2}\epsilon T} 
  {\rm e}^{-\frac{1}{6}\epsilon V} = 
 \mathcal{T}(\epsilon)  + \mathcal{O}(\epsilon^5),
 \label{eq:D4A}
\end{equation}
where
\begin{equation}
\widetilde V({\bf r})=V({\bf r})+\frac{1}{48}\,\epsilon^2[V,[T,V]]({\bf r})
= V({\bf r}) + \frac{\hbar^2\epsilon^2}{48m}\left|\nabla V({\bf r})\right|^2
\label{eq:Vtilde}
\end{equation}
The individual factors in Eq. (\ref{eq:D4A}) are diagonal in coordinate or
momentum space, respectively.  The computations are performed by Fourier
transforming the wave functions back and forth in successive application
of each component of the factorized operator (\ref{eq:D4A}).

The best numerical accuracy and consistency is reached when exactly the
same discretizations are used for the double commutator term in
Eq. (\ref{eq:Vtilde}) and in the kinetic energy operator.
For this purpose, we write
\begin{equation}
[V,[T,V]]({\bf r}) = \frac{\hbar^2}{2m}
\left[\left(\nabla^2 V^2({\bf r})\right) -
2V({\bf r})\left(\nabla^2 V({\bf r})\right)\right]\,.\label{eq:double}
\end{equation}
If we use, for example, a $n$-point formula for the Laplacian in
Eq. (\ref{eq:double}), we should use exactly the same formula for the
kinetic energy in the evolution operator. This is feasible since, in
momentum space, the eigenvectors for {\it all\/} equidistant
discretizations of the second derivative operator are plane
waves, and the corresponding eigenvalues can be calculated
exactly \cite{cluster}:
\begin{equation}
t(k) = \frac{\hbar^2k^2}{2m}\left(\frac{\sin\xi}{\xi}\right)^2
\left[ 1 + \frac{1}{3}\sin^2\xi + \frac {8}{45}\sin^4\xi
+ \frac {4}{35}\sin^6\xi + \frac{128}{1575}\sin^8\xi +
 \ldots\right]\,.
\end{equation}
Here $h$ is the mesh size in coordinate space, $\xi = k h/2$, and the
terms in the square bracket consecutively refer to a 3, 5, 7, 9,
$\ldots$-- point formula for the second derivative.

\subsection{Orthogonalization}
\label{sec:ortho}

The propagation step outlined above produces, for any approximation of
the operator $\mathcal{T}(\epsilon)$, from an orthonormal set of trial
vectors $\{\psi_j^{(k)}(\mathbf{r}),\,j=1,\dots,n\}$, where $k$ is the
iteration number, a new set of vectors
$\{\phi_j^{(k)}(\mathbf{r}),\,j=1,\dots,n\}$. Before the next
propagation step, these states must be orthonormalized to generate a new
set of states $\{\psi_j^{(k+1)}(\mathbf{r}),\,j=1,\dots,n\}$. This
can, for example, be done by a simple Gram--Schmidt procedure. We
found it, however, advantageous to use the following method which fits
quite naturally into the imaginary time propagation strategy: Consider
the overlap matrix of propagated vectors
\begin{equation}
M_{i\,j} = \left\langle \psi_i^{(k)}\mathcal{T}(\epsilon)
\right|\left. \mathcal{T}(\epsilon) \psi_{j}^{(k)}\right\rangle
= \left\langle \phi_i^{(k+1)}\right|\left.
\phi_{j}^{(k+1)}\right\rangle
\label{eq:Mij}
\end{equation}
and the eigenvalue problem
\begin{equation}
\sum_{j} M_{ i\,j}c_{j}^{(n)}
= m_n c_{i}^{(n)}\,.
\end{equation}
The linear combinations
\begin{equation}
\left|\psi_{j}^{(k+1)}\right\rangle
\equiv \frac{1}{\sqrt{m_j}}
\sum_{i} c_{i}^{(j)} \left|\phi_{i}^{(k)}\right\rangle
\label{eq:propstep}
\end{equation}
form an orthonormal set of linear combinations
of the propagated wave functions.
Moreover, the eigenvalues
$m_{j}$ converge towards
\begin{equation}
m_{j}^{(k)} \equiv \exp(-2\epsilon E_{j}^{(k)}(\epsilon))
\rightarrow m_{j} = \exp(-2\epsilon E_{j}(\epsilon))\,.
\label{eq:enorm}
\end{equation}
We will refer to the $ E_{j}(\epsilon)$ as the
{\it normalization\/} energies.

With increasing number of states the orthogonalization begins to take
up a significant fraction of the computational effort.
At the first glance, the subspace orthogonalization method appears to
be more time consuming than Gram--Schmidt orthogonalization.  For $n$
states, both methods require $n(n+1)/2$ scalar products.  However,
Gram--Schmidt orthogonalization needs to calculate $n(n+1)/2$ linear
combinations of vectors, whereas subspace orthogonalization needs to
calculate $n^2$ such linear combinations, {\it cf.}
Eq. (\ref{eq:propstep}). The disadvantage is compensated by two
features: One is that subspace orthogonalization leads to faster
convergence, in particular the low--lying states and nearly degenerate
states converge more rapidly. Computing time can be saved by
``freezing'' low--lying states that have already converged and the
number of states that need to be propagated and orthogonalized
decreases more rapidly than in the Gram--Schmidt procedure. The
second, less obvious advantage is that the matrix product
(\ref{eq:propstep}) is very effectively carried out with the
Winograd--Strassen algorithm \cite{NumRec2}, as implemented for
example in the ATLAS \cite{atlas} library.  The break-even
points occurs at about 120 states on a mesh of $64^3$ points. Thus,
although na\"\i vely slower, the subspace orthogonalization provides a
significant advantage over the Gram-Schmidt orthogonalization used
in Ref. \cite{imstep}.

When the highest calculated state is almost degenerate with a state
that has been omitted, the subspace orthogonalization method converges
somewhat better if one keeps a few more states for which no
convergence test is needed.

\subsection{Expectation energy}
\label{sec:vare}
A useful estimate of the accuracy of the normalization energies as a
function of the time step $\epsilon$ can be obtained by calculating
the {\it expectation\/} energy
\begin{equation}
H_j^{(k)}(\epsilon) = \left\langle \psi_j^{(k)} \right| H \left|
 \psi_j^{(k)} \right\rangle\,.
\label{eq:eexp}
\end{equation}
The accuracy of the expectation energies is of order $\epsilon^{4}$
for the second order, and of order $\epsilon^{8}$ for the fourth order
algorithm.  One expects therefore that the expectation energies are,
as a function of the time step, more accurate than the normalization
energies defined in Eq. (\ref{eq:enorm}).  Since the computation of
the expectation energies involves relatively little overhead, we can
utilize them to determine a termination criterion for the imaginary
time step iterations. These iterations normally start with a
relatively large time step, are iterated a few times, then the time
step is reduced, and the process is repeated until the time step is
small enough such that one is practically in the asymptotic regime. If
the time step is large, then there is, of course, no need to iterate
to very high accuracy.  As a termination condition one may therefore
use that the {\em rms--}error of the normalization energies,

\begin{equation}
\Delta E^{(k)}(\epsilon) \equiv
\left[\frac 1 N\sum_j\left(E_j^{(k)}(\epsilon)-E_j^{(k-1)}
(\epsilon)\right)^2\right]^{1/2}
\label{eq:dele}
\end{equation}
is comparable to the accuracy predicted by the expectation energies
\begin{equation}
\Delta H^{(k)}(\epsilon) \equiv
\left[\frac 1 N\sum_j
\left(E_j^{(k)}(\epsilon)-H_j^{(k)}(\epsilon)\right)^2\right]^{1/2}\,,
\label{eq:delh}
\end{equation}
{\it i.e.\/}
\begin{equation}
\Delta E^{(k)}(\epsilon) \le \gamma \Delta H^{(k)}(\epsilon)
\label{eq:terminator}
\end{equation}
where $\gamma$ is an empirically determined factor of order 0.1\dots 1.

\section{Program description}

\subsection{Physical Model}

The Schr\"odinger equation is solved in a rectangular box centered around
the origin, $-L_i \le x_i < L_i$, $x_i \in \{x,y,z\}$. The physical
model is defined by the value of $\hbar^2/2m$ and a user-supplied
function that returns the local potential $V({\bf r})$ in units of
$\hbar^2/2m$.  Since we make heavy use of fast Fourier transforms, we
solve in principle the problem subject to periodic boundary
conditions.  Bound states are calculated by having a sufficiently
repulsive potential at the boundaries of the discretization box; it is
the user's responsibility to check for unwanted superlattice effects.

We have in previous work \cite{imstep,cluster,nonloc} supplied ample
documentation for the convergence and speed of the fourth order
imaginary time step method in the framework of density functional
theory, there is no need to repeat these discussions here.

\subsection{Program structure}

The program consists of a few Fortran files, which are summarized in
table \ref{tab:source}. Practically all of the time consuming
operations are performed in the FFTW-3.0.1 library routines
\cite{fftw} for fast Fourier transforms and a set of Lapack
\cite{Lapack} routines. We have tested the code using the Intel
Fortran compiler \cite{ifort} on SGI Altix systems \cite{SGI}, the
SGI f90 compiler on Octane workstations \cite{SGI}, the free GNU g95
\cite{g95} and Sun's f95 Fortran compiler \cite{sunfort} on several
Linux PCs. Sample makefiles are provided for these compilers. We have
successfully tested the code using the Lapack reference implementation
on netlib \cite{Lapack}, the ``automatically optimized'' Atlas library
\cite{atlas}, and Intel's Math Kernel library \cite{ifort}.

\begin{table}
  \centering
  \begin{tabular}{lp{0.75\linewidth}}
    \hline
    \texttt{3dsch.f90} & Main program.\\
    \texttt{3dsch.inc} & Definitions of a few scalar global variables.\\
    \texttt{globals.f90} & Definitions of the vector global variables.\\
    \texttt{preset.f90} & Routine to initialize the code at the
    beginning. Allocates memory and initializes Fourier transforms.\\
    \texttt{readd.f90} & Reads the input files and writes wave functions
    to a large file for further processing.\\
    \texttt{inivect.f90} & Before the first iteration, the wave
    functions have to be initialized with some starting values. Here
    we are using ``particle-in-a-box'' wave functions.\\
    \texttt{imstep.f90} & The core of the program, implements the
    imaginary timestep algorithm. \\
    \texttt{ortho.f90} & Orthogonalization of the wave functions
    using the subspace orthogonalization procedure described in
    section \ref{sec:ortho}.\\
    \texttt{vare.f90} & Routine to calculate the expectation
    energy (see Sec. \ref{sec:vare}).\\
    \texttt{vext.f90} & Initializes the external potential and the
    auxiliary potential $\widetilde V({\bf r})$ defined in
    Eq. (\ref{eq:Vtilde}) from the user-supplied functions
    \texttt{vusr.f90} or \texttt{vusr.c}\\
    \texttt{vusr.f90} & Contains the user-defined function for
    the local potential (Fortran version).\\
    \texttt{vusr.c}   & Contains the user-defined function for
    the local potential (C version).\\
    \hline
  \end{tabular}
  \vspace{0.2cm}
  \caption{Fortran source code files of the program.}
  \label{tab:source}
\end{table}

\subsection{Program usage}

The 3D version of the program is called by \texttt{3dsch prefix} and
looks for two input files in the current directory called
\texttt{prefix.mesh} and \texttt{prefix.model} which are described in
tables \ref{tab:mesh} and \ref{tab:model}, respectively. The 2D version
is analogously
called by \texttt{2dsch prefix}. All further discussion refers the
the 3D code, in the 2D version all references to the $z$ coordinate
are simply omitted.

During the execution, the program writes to standard output and to two
output files, \texttt{prefix.eval} end \texttt{prefix.evit}. The first
file contains the converged eigenvalues obtained for each timestep,
such that the last line shows the eigenvalues in the desired
accuracy. The second file, \texttt{prefix.evit}, contains more
detailed output written during the imaginary timestep iterations and
is mostly used for debugging purposes. The contents of both output
files are described in table \ref{tab:eval}.

On exit, the converged wave functions are, depending on the value of
\texttt{IMSG} (defined in \texttt{prefix.mesh}) written to a binary
file, (see table \ref{tab:imsg}).  The name of this file can be
configured in the input file \texttt{prefix.model}. At startup, the
program looks for a file of the same structure, also defined in
\texttt{prefix.model}. If the binary file exists, the contained wave
functions are read in as starting values, providing a mechanism to use
starting values different than the pre-configured
``particle-in-a-box'' wave functions in \texttt{inivect.f90}. This may
speed up the calculation if, for example, several runs with slightly
different potentials are carried out. The code that reads the binary
file can be found in \texttt{readd.f90}. The routine does not rely on
global definitions and can also be used in a user-written
post-processing code.

\begin{table}
  \centering
  \begin{tabular}{lp{0.75\linewidth}}
    \hline
	\texttt{MX}	& Mesh points in x-direction. The mesh points are
        counted from \texttt{-MX} to \texttt{MX-1}, so there are a
        total of \texttt{2*MX} mesh points in x-direction\\
	\texttt{MY}	& Mesh in y-direction is -MY:MY-1 \\
	\texttt{MZ}	& Mesh in z-direction is -MZ:MZ-1 \\
                & The mesh sizes must be multiples of 4, but for
		  optimum performance they should be a power of 2. \\

	\texttt{HR}	& Discretization in coordinate space (distance of mesh
        points). The same value is used in all three directions. \\

	\texttt{MAXIM}	& Maximum number of imaginary timestep iterations. \\
	\texttt{MORB}	& Number of states that are propagated.
		  This number may be larger than the number
		  of states calculated to improve convergence. \\

	\texttt{ORDER}	& Order of derivatives in Laplacian. \\
	\texttt{RMUL}	& Multiplicator of timestep ramp.
		  0.5 is a good value. \\
 
	\texttt{ESTP}	& Convergence factor of the imaginary timestep iterations.
		  This is the value at which the iterations start. \\

	\texttt{ESTE}	& Convergence factor of the imaginary timestep iterations.
		  This is the smallest value that is used during the
		  iterations. \\

	\texttt{IMSG}	& A bit field, defining the level at which output messages
		  on the iterations are generated, and controlling some
		  technical aspects of the calculation. The possible
                  values are described in table \ref{tab:imsg}\\

	\texttt{EPSI}	& Desired accuracy for the imaginary time step
		  iterations. \\
	\texttt{EPSR}	& Ratio $\gamma$ between expectation energy and normalization
		  energy error, see Eq. (\ref{eq:terminator}).
		  \texttt{EPSR$\,=\,$0} will turn this off. \\
    \hline
  \end{tabular}
  \vspace{0.2cm}
  \caption{Contents of the input file \texttt{prefix.mesh}.}
  \label{tab:mesh}
\end{table}

\begin{table}
  \centering
  \begin{tabular}{llp{0.75\linewidth}}
    \hline
    IMSG	& BIT	& Function\\
    \hline
    0	& -	& No information on iterations\\
    1	& 0	& Show errors and timings after each converged
    time step.\\
    2	& 1	& Show errors and timing after each imaginary timestep
    iteration.\\
    4	& 2	& Show timings.\\
    8	& 3	& Dump eigenvalues and wave functions into a
    binary data file. Depending on the grid, this may generate a huge file.
    Meant for testing, documentation, and further
    processing. The file name is configured in \texttt{prefix.model}\\
    16	& 4	& Use the second order algorithm.
    (Set this flag only to waste CPU time).\\
    \hline
  \end{tabular}
  \vspace{0.2cm}
  \caption{Definition of the bitfield \texttt{IMSG} contained
    in \texttt{prefix.mesh}.}
  \label{tab:imsg}
\end{table}

\begin{table}
  \centering
  \begin{tabular}{lp{0.75\linewidth}}
    \hline
    \texttt{H2M} & Value of $\frac{\hbar}{2m}$. Can be used to set the
    unit system to atomic Rydberg or Hartree units, but also to use an
    effective mass.\\
    \texttt{NORB} & Number of states that are converged to the desired
    accuracy.\\
    \texttt{RPAR(1:10)} & Array for user-defined double precision
    parameters that are passed to the user-defined potential function
    in \texttt{vusr.f90}.\\
    \texttt{IPAR(1:10)} & Array for user-defined integer
    parameters that are passed to the user-defined potential function
    in \texttt{vusr.f90}.\\
    \texttt{INFILE} & String containing the name of a binary input
    file from which the program tries to read initial wave functions.
    If the file is non-existent, or has the wrong structure,
    the wave functions are initialized by ``particle in a box''
    states.\\
    \texttt{OUTFIL} & String containing the name of the binary output
    file the wave functions are written to.\\
    \hline
  \end{tabular}
  \vspace{0.2cm}
  \caption{Contents of the input file \texttt{prefix.model}.}
  \label{tab:model}
\end{table}

\begin{table}
  \centering
  \begin{tabular}{clp{0.6\linewidth}}
    \hline
    column & format & description \\
    \hline
    1 & \texttt{I5}     & Number of iterations needed. \\
    2 & \texttt{F12.4}  & Imaginary timestep $\epsilon$. \\
    3 & \texttt{ES12.3} & {\em rms--}error $\Delta E$ of the normalization energies,
    Eq. (\ref{eq:dele}). \\
    4 & \texttt{ES12.3} & {\em rms--}error $\Delta H$ of the normalization energies
    with respect
    to the expectation energies, Eq. (\ref{eq:delh}). \\
    5 \dots 2*\texttt{MORB}+4 & \texttt{ES24.15} & Converged normalization
    energies ($E_i(\epsilon)$, Eq. (\ref{eq:enorm})) and
    expectation energies ($H_i(\epsilon)$, Eq. (\ref{eq:eexp})) for all
    states in the format\\
    &        & $E_1(\epsilon), H_1(\epsilon),E_2(\epsilon),
    H_2(\epsilon),\;\; \dots, \;\; E_\text{\tt MORB}(\epsilon),
    H_\text{\tt MORB}(\epsilon)$\\ 
    & & \texttt{MORB} is the number of propagated states defined in the file
    \texttt{prefix.mesh}, see table \ref{tab:mesh}.\\
    \hline
  \end{tabular}
  \vspace{0.2cm}
  \caption{Structure of the output files \texttt{prefix.eval} and
    \texttt{prefix.evit}. For each timestep $\epsilon$, one line is
    written to \texttt{prefix.eval} containing the
    number of iterations needed for this timestep, the {\em rms--}errors
    Eqs. (\ref{eq:dele}),(\ref{eq:delh}) and the converged
    normalization and expectation energies from Eq. (\ref{eq:enorm})
    and (\ref{eq:eexp}). The file \texttt{prefix.evit} contains the
    same information, but one line is written for each iteration, not
    just the converged result for each timestep. \texttt{prefix.evit}
    is mainly used for debugging purposes.}
  \label{tab:eval}
\end{table}

\subsection{User supplied function}

The local potential $V(\mathbf{r})$ must be supplied by the user in
the Fortran subroutine \texttt{vusr.f90}, which is called
by

\texttt{CALL VUSR(V, X, Y, Z, RPAR, IPAR, IER)}

\begin{table}
  \centering
  \begin{tabular}{clp{0.6\linewidth}}
    \hline
    variable & intent & description \\
    \hline
    \texttt{V} & out     & Value of the potential in units of $\hbar^2/2m$. \\
    \texttt{X} & in  & Value of the $x$ coordinate. \\
    \texttt{Y} & in  & Value of the $y$ coordinate. \\
    \texttt{Z} & in  & Value of the $z$ coordinate. \\
    \texttt{RPAR(1:10)} & in & User supplied vector of up to 10 real parameters.\\
    \texttt{IPAR(1:10)} & in & User supplied vector of up to 10 integer parameters.\\
    \texttt{IERR} & out & Error flag.\\
    \hline
  \end{tabular}
  \vspace{0.2cm}
  \caption{Calling parameters for the user-defined subroutine
    \texttt{vusr}. The length of the model-specification
    vectors \texttt{RPAR(1:10)} and \texttt{IPAR(1:10)}
    may be changed at compile time.}
  \label{tab:vusr}
\end{table}

It is also possible to use a C function instead of Fortran, a C
wrapper file is provided in \texttt{vusr.c}. The parameters
\texttt{RPAR(1:10)} and \texttt{IPAR(1:10)} defined in the input file
\texttt{prefix.model} (see table \ref{tab:model}) are passed to the
user defined function to allow for easy customization of the
potential.

The value of the potential is expected in units of $\hbar^2/2m$.

The subroutines \texttt{vusr.f90} and \texttt{vusr.c} contain
examples for the simple potentials
\begin{equation}
V({\bf r}) = \frac{\hbar^2}{2m}
\left[r_1 x^{n_1} + r_2 y^{n_2} + r_3 z^{n_3}\right]
\end{equation}
where the $r_i$ and $n_i$ are input, through the file \texttt{prefix.model},
in the vectors \texttt{RPAR} and \texttt{IPAR}.

\subsection{Typical use}

The program starts with an initial time step $\epsilon$, defined by
the input parameter \texttt{ESTP}. For this fixed time step, the
transport equation is iterated until convergence determined by the
parameters \texttt{EPSI} and \texttt{ESPR}, but no more than
\texttt{MAXIM} iterations. After termination, the time step is
reduced by a factor \texttt{RMUL} and the process is repeated until
the time step is less or equal to \texttt{ESTE}. A value \texttt{RMUL}
= 0.5 is a good choice for production runs. If it takes many
iterations per time step, it might be that the highest calculated
state is almost degenerate with a state that has been omitted, in that
case it is advisable that the number \texttt{MORB} be increased.

The user should, in the first trial runs, check that the expected
$\epsilon^4$ convergence for the normalization energies, and the
$\epsilon^8$ convergence of the expectation energies, is
satisfied. For that purpose, the file \texttt{prefix.eval} is
provided. Deviation from this behavior for small $\epsilon$ may
indicate a too coarse discretization; note that the exact power law is
derived under the assumption that there are no discretization
errors. To determine the correct power law, a test run with a
ramp-multiplicator \texttt{RMUL} $\approx 0.9$ may be useful.  There
is no rigorous statement at what value of the time step the asymptotic
power law should dominate, this depends on the physical model.
A good estimate for the absolute accuracy of the eigenvalues is the
{\em rms--}error $\Delta H(\epsilon)$.

\section{Summary}

We have presented in this paper a real-space program for solving the
local, two- and three-dimensional Schr\"odinger equation using
fourth--order factorizations of the evolution operator
(\ref{eq:evolutionOperator}). We have demonstrated in previous
work \cite{imstep,cluster,nonloc} that fourth--order factorizations
improve the convergence of the basic algorithm by typically one to two
orders of magnitude, depending on the accuracy wanted. We emphasize
again the simplicity of the algorithm: its core routine of imaginary
time propagation can be written in a few lines of Fortran 90 code,
using readily available Fast Fourier Transform and linear algebra
routines.

\begin{ack}
This work was supported by the Austrian Science Fund FWF under grant
No. P18134 as well as the Austrian Academic Exchange Services \"OAD
(11/2006).  We would like to thank M. Aichinger, S.~A.~Chin, and
E. H\'ernandez for useful discussions.
\end{ack}
\newpage
%\bibliography {papers}
%\bibliographystyle{elsart-num}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{10}
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{\texttt{#1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi

\bibitem{Suzuki96}
M.~Suzuki, New scheme of hybrid exponential product formulas with applications
  to quantum Monte Carlo simulations, in: D.~P. Landau, K.~K. Mon, H.-B.
  Sch{\"u}ttler (Eds.), Computer Simulation Studies in Condensed Matter
  Physics, Vol. VIII, Springer, Berlin, 1996, pp. 1--6.

\bibitem{ChinPLA97}
S.~A. Chin, Symplectic integrators from composite operator factorizations,
  Phys. Lett. A 226 (1997) 344--348.

\bibitem{Chinjcp01}
S.~A. Chin, C.~R. Chen, Fourth order gradient symplectic integrator methods for
  solving the time-dependent Schr{\"o}dinger equation, J. Chem. Phys. 114
  (2001) 7338.

\bibitem{imstep}
J.~Auer, E.~Krotscheck, S.~A. Chin, A fourth order real--space algorithm for
  solving local Schr{\"o}dinger equations, J. Chem. Phys. 115 (2001)
  6841--6846.

\bibitem{cluster}
M.~Aichinger, E.~Krotscheck, A fast configuration space method for solving
  local Kohn--Sham equations, Computational Materials Sciences 34~(2) (2005)
  188--212.

\bibitem{qrings}
M.~Aichinger, S.~A. Chin, E.~Krotscheck, E.~R{\"a}s{\"a}nen, Geometric and
  impurity effects on quantum rings in magnetic fields, Phys. Rev. B 73 (2006)
  195310.

\bibitem{nonloc}
E.~R. Hern\'{a}ndez, S.~Janecek, M.~S. Kaczmarski, E.~Krotscheck, An
  evolution-operator method for density functional theory, Phys. Rev. B 75
  (2007) 075108.

\bibitem{NumRec2}
W.~H. Press, S.~A. Teukolsky, W.~T. Vetterling, B.~P. Flannery, Numerical
  Recipes. The Art of Scientific Computation, 2nd Edition, Cambridge University
  Press, Cambridge, 1992.

\bibitem{atlas}
{\tt www.atlas.org}.

\bibitem{fftw}
{\tt www.fftw.org}.

\bibitem{Lapack}
{\tt www.netlib.org/lapack}.

\bibitem{ifort}
{\tt www.intel.com}.

\bibitem{SGI}
SCSL, Octane, and Altix are registered trademarks of Silicon Graphics Inc.

\bibitem{g95}
{\tt www.g95.org}.

\bibitem{sunfort}
{\tt www.sun.com}.

\end{thebibliography}

\newpage
\hspace{1pc}
{\bf TEST RUN OUTPUT}

The following table shows a typical test output for the included
example of a harmonic oscillator in three dimensions.  This test run
has been carried out on an AMD Opteron system, using the f95 compiler.
After a few lines describing essential parameters of the run, each
time step generates one line of output, describing
\begin{itemize}
\item The number of iterations needed for convergence at this
time step.
\item The lowest state that has been included in all iterations.
When low-lying states have converged to the desired accuracy,
the code will "freeze" these states to save time.
\item The time step for this iteration.
\item Propagation time.
\item Orthogonalization time.
\item Error $\Delta E(\epsilon)$.
\item Error $\Delta H(\epsilon)$.
\end{itemize}
After the time step has been reduced to the desired value, the code
prints the resulting eigenvalues. Convergence is tested only for the
lowest \texttt{NORB} states but, to potentially improve convergence of
the subspace orthogonalization, a larger number \texttt{MORB} can be
propagated. The sample run is for \texttt{NORB=4} and \texttt{MORB=8},
these are separated in the output by a vertical bar.

\bigskip
{\footnotesize
\begin{verbatim}
CMDLINE> ./3dsch hosc3d

 Mesh   file = hosc3d.mesh                                                     
 Output file = osc.dat                                                         
 MX =  32 MY =  32 MZ =  32 HR =   0.25
 ITIM =  99    4 STATES

=== 3D Schroedinger equation solver  === Date: 19-12-2007 === Time: 10:52:25 ===

    3    1    2.000    0.541    0.062   6.908E-05   1.290E-03
    2    1    1.000    0.486    0.061   1.861E-05   8.604E-05
    2    1    0.500    0.485    0.063   2.732E-07   5.985E-06
    2    1    0.250    0.486    0.060   1.047E-08   3.797E-07
    2    1    0.125    0.488    0.062   1.209E-09   2.324E-08
    2    1    0.062    0.487    0.065   2.838E-10   1.276E-09
    1    1    0.031    0.490    0.062   3.013E-09   1.351E-09
    1    1    0.016    0.489    0.061   1.022E-10   3.638E-11
    1    1    0.008    0.490    0.061   2.985E-11   1.755E-11
    1    1    0.004    0.490    0.064   1.786E-11   9.977E-12
    1    1    0.002    0.485    0.063   8.670E-12   4.820E-12
    1    1    0.001    0.489    0.063   4.241E-12   2.358E-12


Converged | propagated states: ESTP =  0.0010
 N =   4 E_n(1:  8):  1.5000    2.5000    2.5000    2.5000 |  3.5000    3.5000
                      3.5000    3.5000  

CMDLINE>
\end{verbatim}
}
\end{document}
