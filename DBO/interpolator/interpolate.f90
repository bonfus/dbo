!
! Copyright (C) 2001-2007 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
MODULE interpolator
  !
  IMPLICIT NONE
  !  
  INTEGER, PARAMETER :: DP = selected_real_kind(14,200)
  !
  ! Parameters for RBF interpolator
  !
  real(DP) :: rbf_r0 = 2.0
  integer :: phitype = 1
  !
  ! Parameters for smooth_trivariate
  !
  integer :: qs3_nq = 17
  integer :: qs3_nr = 3
  integer :: qs3_nw = 32  
  !
  LOGICAL :: lparamset = .false.
  !
  PUBLIC :: set_default_params, interpolate, set_interp_params
  !
  CONTAINS
  !----------------------------------------------------------------------------
  SUBROUTINE set_default_params()
    !----------------------------------------------------------------------------
    !
    ! ... This routineset default parameters
    !
    IMPLICIT NONE
    ! Parameters for smooth_trivariate
    !
    qs3_nq = 17
    qs3_nr = 3
    qs3_nw = 32   
    !
    ! Parameters for RBF
    rbf_r0 = 0.5
    phitype = 1
    !
    !
  END SUBROUTINE set_default_params
  !
  !----------------------------------------------------------------------------
  SUBROUTINE set_interp_params(itype, iqs3_nq , iqs3_nr , iqs3_nw , irbf_r0 , iphitype )  
    !
    IMPLICIT NONE
    !
    INTEGER, INTENT(IN) :: itype
    INTEGER, OPTIONAL, INTENT(IN) :: iqs3_nq , iqs3_nr , iqs3_nw
    REAL(DP), OPTIONAL, INTENT(IN) :: irbf_r0
    INTEGER, OPTIONAL, INTENT(IN) :: iphitype
    
    !
    IF ( itype .eq. 0  ) THEN
       !
       ! RBF settings
       !
       IF (PRESENT(irbf_r0)) rbf_r0=irbf_r0
       IF (rbf_r0 < 0 ) THEN
         write(*,*) 'rbf_r0 invalid values, reset to 1.0'
         rbf_r0 = 1.0
       END IF         
       IF (PRESENT(iphitype)) phitype=iphitype
       IF (phitype < 1 .or. phitype > 6) THEN
         write(*,*) 'phitype invalid values, reset to 1'
         phitype = 1
       END IF  
       
       !
    ELSE IF ( itype .eq. 1 ) THEN
       !
       ! trilinear SETTINGS
       !
    ELSE IF ( itype .eq. 2 ) THEN
       !
       IF (PRESENT(iqs3_nq)) qs3_nq = iqs3_nq
       IF (PRESENT(iqs3_nr)) qs3_nr = iqs3_nr
       IF (PRESENT(iqs3_nw)) qs3_nw = iqs3_nw
       !
    ELSE 
       !
       !CALL errore()
       stop
       !
    END IF  
    !
  END SUBROUTINE set_interp_params
  !----------------------------------------------------------------------------
  SUBROUTINE interpolate( dim1, np , pin, v, no, pout, itype, vout )
    !----------------------------------------------------------------------------
    !
    ! ... This routine handles interpolation of scattered data.
    !
    ! ... input:
    !
    ! ...    dim1  dimension of the space (1D 2D 3D)
    ! ...    np    number of points
    ! ...    pin   vector of x position y position z position
    ! ...    v     scalar field values
    ! ...    no    number of interpolated points requested
    ! ...    pout  x,y,z vector of interpolation points requested  
    ! ...    type  name of the interpolator
    !
    ! ... output:
    !
    ! ...    vout  vector with interpolated values
    !
    IMPLICIT NONE
    !
    INTEGER, INTENT(IN) :: dim1, np, no, itype
    REAL(DP), INTENT(IN) :: pin(dim1,np), v(np)
    REAL(DP), INTENT(IN) :: pout(dim1,no)
    REAL(DP), INTENT(OUT):: vout(no)
    !
    INTEGER :: ibnd
    !
    ! ... check input parameters
    !
    IF ( no == 0 ) RETURN
    !
    !CALL start_clock( 'interpolate' )  
    !
    ! ... 
    !
    IF ( itype .eq. 0  ) THEN
       !
       CALL rbf_interp()
       !
    ELSE IF ( itype .eq. 1 ) THEN
       !
       CALL trilinear()
       !
    ELSE IF ( itype .eq. 2 ) THEN
       !
       CALL smooth_trivariate()
       !
    ELSE 
       !
       !CALL errore()
       stop
       !
    END IF    
    !
    !CALL stop_clock( 'interpolate' )
    !
    RETURN
    !
    CONTAINS
       !
       !-----------------------------------------------------------------------
       SUBROUTINE rbf_interp()
         !-----------------------------------------------------------------------
         ! 
         ! ... rbf interpolator
         !
         IMPLICIT NONE

          REAL(DP),allocatable :: w(:)
          external phi6 ! cubic
          external phi5 ! linear
          external phi4 ! gaussian
          external phi3 ! thin-plate
          external phi2 ! inverse multiquadric
          external phi1 ! multiquadric
          !
          ALLOCATE (w(np))
          !
          write(*,*) 'Using r0: ', rbf_r0, 'and phi', phitype
          !
          IF ( phitype == 1 ) call rbf_weight ( dim1, np, pin, rbf_r0, phi1, v, w )
          IF ( phitype == 2 ) call rbf_weight ( dim1, np, pin, rbf_r0, phi2, v, w )
          IF ( phitype == 3 ) call rbf_weight ( dim1, np, pin, rbf_r0, phi3, v, w )
          IF ( phitype == 4 ) call rbf_weight ( dim1, np, pin, rbf_r0, phi4, v, w )
          IF ( phitype == 5 ) call rbf_weight ( dim1, np, pin, rbf_r0, phi5, v, w )
          IF ( phitype == 6 ) call rbf_weight ( dim1, np, pin, rbf_r0, phi6, v, w )
          !
          IF ( phitype == 1 ) call rbf_interp_nd ( dim1, np, pin, rbf_r0, phi1, w, no, pout, vout )  
          IF ( phitype == 2 ) call rbf_interp_nd ( dim1, np, pin, rbf_r0, phi2, w, no, pout, vout )  
          IF ( phitype == 3 ) call rbf_interp_nd ( dim1, np, pin, rbf_r0, phi3, w, no, pout, vout )  
          IF ( phitype == 4 ) call rbf_interp_nd ( dim1, np, pin, rbf_r0, phi4, w, no, pout, vout )  
          IF ( phitype == 5 ) call rbf_interp_nd ( dim1, np, pin, rbf_r0, phi5, w, no, pout, vout )  
          IF ( phitype == 6 ) call rbf_interp_nd ( dim1, np, pin, rbf_r0, phi6, w, no, pout, vout )  
          !
          deallocate(w)
         !
       END SUBROUTINE rbf_interp
       !
       !-----------------------------------------------------------------------
       SUBROUTINE trilinear()
         !-----------------------------------------------------------------------
         !
         ! ... trilinear interpolator
         !
  
         !
         RETURN
         !
       END SUBROUTINE trilinear     
       !
       !
       !-----------------------------------------------------------------------
        SUBROUTINE smooth_trivariate ( )
       !-----------------------------------------------------------------------
         !
         ! ... QSHEP3 defines a smooth trivariate interpolant of scattered 3D data.
         !
         IMPLICIT NONE
         !

         !
  
  
         real(DP), external :: qs3val
         
         integer i
         integer ier
         
         
         integer :: lcell(qs3_nr,qs3_nr,qs3_nr)
         integer ,allocatable ::  lnext(:)
         real(DP),allocatable :: a(:,:), rsq(:)
         real(DP) :: xyzdel(3)
         real(DP) :: xyzmin(3)
         real(DP) :: rmax
         !
         real(DP) vmin, vmax_shifted
         real(DP), allocatable :: v_shifted(:)
         !
         allocate(lnext(np))
         allocate(a(9,np))
         allocate(rsq(np))
         allocate(v_shifted(np))
         !
         !
         ! go to positive values, this is needed for out of boundary tracking
         vmin = MINVAL(v)
         v_shifted = v-vmin+1.0D-10
         vmax_shifted = MAXVAL(v_shifted)
         !
         write (*,*) ' Preparing interpolator...'
         !
         call qshep3 ( np, pin(1,:), pin(2,:), pin(3,:), v_shifted, qs3_nq, qs3_nw, qs3_nr, lcell, lnext, xyzmin, &
                      xyzdel, rmax, rsq, a, ier )
  
         if ( ier /= 0 ) then
           write ( *, * ) ' errore( QSHEP3D_PRB', 'Error return from QSHEP3, IER = ', ier
           stop
         end if
         !
         write (*,*) ' Interpolating...'
         do i=1, no
           
           vout(i) = qs3val ( pout(1,i), pout(2,i), pout(3,i), np, pin(1,:), pin(2,:), pin(3,:), &
                            v_shifted, qs3_nr, lcell, lnext, xyzmin, xyzdel, rmax, rsq, a )
           
           if ( vout(i) == 0.0D0 ) vout(i) = vmax_shifted
            
         end do
         ! Go beck to (possibly) negative values
         vout = vout + vmin - 1.0D-10
         !
         deallocate(lnext,a,rsq,v_shifted)
         !
         RETURN
         !
      END SUBROUTINE smooth_trivariate
  
  END SUBROUTINE interpolate
END MODULE interpolator
