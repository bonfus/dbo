!
! Copyright (C) 2003-2006 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!--------------------------------------------------------------------------
MODULE dbo_variables
  !---------------------------------------------------------------------------
  !
  ! ... This module contains all variables needed by potential 
  ! ... construction algorithm and for shroedinger eq solution.
  !
  ! ... Written by Carlo Sbraccia ( 2003-2006 ) & PB
  !
  USE kinds, ONLY : DP
  !
  IMPLICIT NONE
  !
  SAVE
  !
  ! ... "general" variables :
  !
  LOGICAL :: restart
  INTEGER :: &
       saving_interval            ! scf cycles interval between various 
                                  ! restart files update. This is a lower
                                  ! bound.
 
  !
  LOGICAL :: &
       conv_potential             ! .TRUE. when "potential" acquisition
                                  !  convergence has been achieved
                                  !   
  LOGICAL :: &
       first_pot,     &           ! if .TRUE. the code will try to load 
                                  !           electron density from file.
       tune_load_balance, &       ! if .TRUE. the load balance for image
                                  !           parallelisation is tuned at
                                  !           runtime
       use_pot, &
       use_wfc, &
       use_struct
        
  INTEGER :: &
       dim1,                     &! dimension of the configuration space
       num_of_points,            &! maximum number of points to be computed.
       pending_point,            &! last point for which scf has not been
                                  ! achieved
       max_step 
                                  
  REAL(DP) :: &
       search_step,                       &! the search step
       search_cutoff              ! the cutoff energy for search
  LOGICAL :: &
       lfixed      = .FALSE.,    &! .TRUE. if search_scheme = "fixed"
       lexplore    = .FALSE.      ! .TRUE. if search_scheme = "explore"
  !
  ! ... "general" real space arrays
  !
  REAL(DP), ALLOCATABLE :: &
       pes(:)                    ! the potential enrgy of the points
  REAL(DP), ALLOCATABLE :: &
       pos(:,:),               &! reaction path
       grad(:,:)                ! gradients acting on the path
  INTEGER, ALLOCATABLE :: &
       fix_atom_pos(:,:)                ! 0 or 1, if 0 fixed atom
  !
  !
  CONTAINS
     !
     !----------------------------------------------------------------------
     SUBROUTINE path_allocation()
       !----------------------------------------------------------------------
       !
       IMPLICIT NONE
       !
       ALLOCATE( pes( num_of_points ) )
       !
       ALLOCATE( pos(   dim1, num_of_points ) )
       ALLOCATE( grad(     dim1, num_of_points ) )
       !
       !
     END SUBROUTINE path_allocation
     !
     !
     !----------------------------------------------------------------------
     SUBROUTINE path_deallocation()
       !----------------------------------------------------------------------
       !
       IMPLICIT NONE
       !
       IF ( ALLOCATED( pos ) )          DEALLOCATE( pos )
       IF ( ALLOCATED( grad ) )         DEALLOCATE( grad )
       IF ( ALLOCATED( pes ) )          DEALLOCATE( pes )
       !
       IF ( ALLOCATED( fix_atom_pos ) )     DEALLOCATE( fix_atom_pos )     
       !
     END SUBROUTINE path_deallocation
     !
END MODULE dbo_variables
