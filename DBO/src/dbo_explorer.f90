!
! Copyright (C) 2002-2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .

MODULE dbo_explorer
  !---------------------------------------------------------------------------
  !
  ! ... This module contains the routines for potential 
  ! ... construction algorithm.
  !
  ! ... Written by Pietro Bonfa'
  !
  USE kinds, ONLY : DP
  !
  IMPLICIT NONE
  !
  !SAVE
  !
  INTEGER, ALLOCATABLE :: &
       visit_history(:,:),   &       ! positions visited
       unsampled_ngbrs(:)
       
  INTEGER :: cur_pos(3)
  INTEGER :: cur_pos_index
  INTEGER :: directions(3,6)
  !
  PRIVATE
  !
  PUBLIC :: initialize_space
  PUBLIC :: fill_pos
  PUBLIC :: plan_move
  PUBLIC :: deallocate_explorer
  !
  PUBLIC :: visit_history, unsampled_ngbrs, cur_pos, cur_pos_index
  !
  CONTAINS    
    !
    ! ... module procedures
    !
    !-----------------------------------------------------------------------  
    SUBROUTINE initialize_space()
        USE dbo_variables, ONLY: num_of_points
        !
        IMPLICIT NONE
        !
        ALLOCATE(visit_history(3,num_of_points))
        ALLOCATE(unsampled_ngbrs(num_of_points))
        
        visit_history(:,:) = -999999
        visit_history(:,1) = (/0,0,0/)
        cur_pos = (/0,0,0/)
        cur_pos_index = 1
        unsampled_ngbrs(:) = 6
        !
        !
        directions(:,1) = (/1,0,0/)
        directions(:,2) = (/-1,0,0/)
        directions(:,3) = (/0,1,0/)
        directions(:,4) = (/0,-1,0/)
        directions(:,5) = (/0,0,1/)
        directions(:,6) = (/0,0,-1/)
        
    END SUBROUTINE initialize_space
  
    !-----------------------------------------------------------------------  
    SUBROUTINE fill_pos(fii, lii)
        USE dbo_variables, ONLY: pos, search_step, num_of_points
        USE input_parameters, ONLY : nat 
        
        IMPLICIT NONE
        
        INTEGER, INTENT(INOUT) :: fii
        INTEGER, INTENT(INOUT) :: lii
        
        INTEGER :: i, p
        INTEGER :: lai ! last atom index
        LOGICAL :: visited
        INTEGER :: new_pos(3)=(/0,0,0/)
        
        fii = lii
        unsampled_ngbrs(cur_pos_index) = 0
        !
        DO i=1,6
            
            new_pos = cur_pos + directions(:,i)
            visited = .false.
            !check if new pos is already present
            DO p=1,num_of_points
                IF ( visit_history(1,p) == -999999 ) EXIT
                
                IF (visit_history(1,p) == new_pos(1) .and. &
                    visit_history(2,p) == new_pos(2) .and. &
                    visit_history(3,p) == new_pos(3)        ) THEN
                    visited = .true.
                    EXIT
                END IF
            END DO
            !
            IF (.not. visited .and. lii < num_of_points ) THEN
                !add new item
                lii = lii + 1
                !
                ! pos is in bohr
                pos(:,lii) = pos(:,1) 
                lai = nat*3 - 3
                !
                pos(lai+1,lii) = pos(lai+1,lii) + &
                                   search_step*REAL(new_pos(1))
                pos(lai+2,lii) = pos(lai+2,lii) + &
                                   search_step*REAL(new_pos(2))
                pos(lai+3,lii) = pos(lai+3,lii) + &
                                   search_step*REAL(new_pos(3))
                !
                ! add position to history
                visit_history(:,lii) =  new_pos
                !
            END IF
        ENDDO
        !
        fii = fii +1   
        
    END SUBROUTINE fill_pos
    
    SUBROUTINE plan_move(stat)
        !
        USE dbo_variables, ONLY: pes,num_of_points,search_cutoff, &
                                    max_step
        ! DEBUG
        USE mp_world,          ONLY : world_comm, root
        USE mp_global, only : mp_rank
        ! END DEBUG!
        !
        !
        IMPLICIT NONE
        !
        !
        LOGICAL, INTENT(INOUT) :: stat
        !
        LOGICAL :: visited
        INTEGER :: next_move(3)=(/0,0,0/)
        INTEGER :: new_pos(3)=(/0,0,0/)
        REAL(DP) :: next_move_rank = 9999999.0 ! dummy large value
        !
        ! counters
        !
        INTEGER :: p, pp, i
        !==== DEBUG
        integer :: myrank
        myrank =  mp_rank(world_comm)
        ! === END DEBUG
        !
        next_move_rank = 9999999.0
        next_move=(/0,0,0/)
        !
        !
        ! DEBUG
        !
        !
        if(myrank==root) write (*,*) 'PLANNING NEXT MOVE'
        
        DO p=1,num_of_points
            IF ( visit_history(1,p) == -999999 ) EXIT    
            ! debug info
            !if(myrank==root) write (*,*) 'Debug INFO for point: ', p, 'with pos', visit_history(:,p)
            ! if the nn of the point are already known, skip it
            !if(myrank==root) write (*,*)  '    unsampled_ngbrs:', unsampled_ngbrs(p),  'pes:', pes(p)  
        END DO
        ! END DEBUG
        !
        ! set status to false: we do not have a new position
        stat = .false.
        !
        ! go over all points to select a new position
        DO p=1,num_of_points
            !
            ! if the nn of the point are already known, skip it
            IF ( unsampled_ngbrs(p) <= 0 ) CYCLE
            ! we finished all points, exit loop
            IF ( visit_history(1,p) == -999999 ) EXIT
            !
            ! if we are here, this point is supposed to have unsampled nn
            ! let's check it (depends on the path)
            !
            ! Check number of unsampled nn
            unsampled_ngbrs(p) = 6
            DO i=1,6
                ! start from point position and move along the 6 directions
                new_pos = visit_history(:,p) + directions(:,i)
                !
                !
                visited = .false.
                DO pp=1,num_of_points
                    !we finished the points, exit
                    IF ( visit_history(1,pp) == -999999 ) EXIT
                    !
                    ! check if the point in direction i from point p
                    ! is in the history of visited points
                    !
                    IF (visit_history(1,pp) == new_pos(1) .and. &
                        visit_history(2,pp) == new_pos(2) .and. &
                        visit_history(3,pp) == new_pos(3)) THEN
                        visited = .true.
                        EXIT
                    END IF
                END DO
                !
                IF (visited) unsampled_ngbrs(p) = unsampled_ngbrs(p) - 1
            END DO
            !if(myrank==root) WRITE (*,*) 'I searched for nn of point:', p, 'and I think that there are', unsampled_ngbrs(p), 'unn'
            ! is this needed?
            IF ( unsampled_ngbrs(p) <= 0 ) CYCLE
            
            ! check distance

            IF ( ABS(cur_pos(1) - visit_history(1,p)) > max_step .or. &
                ABS(cur_pos(2) - visit_history(2,p)) > max_step .or. &
                ABS(cur_pos(3) - visit_history(3,p)) > max_step     ) THEN
                !if(myrank==root) write(*,*) 'Point, ', p ,' is too far'
                CYCLE
            END IF

            

            !
            ! let's see if it's below the threshold
            IF ( pes(p) > search_cutoff ) THEN
                !if(myrank==root) write(*,*) 'This point is above the threshold, I wont move here'
                CYCLE
            END IF            
            ! Now we have a good candidate, let's rank it
            ! the rank is based on the value of the potential
            ! the lower the better!
            !
            IF ( pes(p) < next_move_rank ) THEN
                cur_pos_index = p
                next_move_rank = pes(p)
                next_move = visit_history(:,p)
            END IF

            
        END DO        
        !
        ! if we didn't find a move, we are stuck!
        IF ( next_move(1) == 0 .and. next_move(2) == 0 .and. &
             next_move(3) == 0 ) RETURN
        !
        ! set the new position
        stat = .true.
        !if(myrank==root) write(*,*) ' I started from', cur_pos
        cur_pos = next_move
        !if(myrank==root) write(*,*) ' Now I go to', cur_pos
        RETURN
        !
    END SUBROUTINE
    
    
    SUBROUTINE deallocate_explorer()
        DEALLOCATE(visit_history)
        DEALLOCATE(unsampled_ngbrs)    
    END SUBROUTINE deallocate_explorer
    
END MODULE
