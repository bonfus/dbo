!
! Copyright (C) 2002-2013 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!=----------------------------------------------------------------------------=!
!
MODULE dbo_input_parameters_module
!
!=----------------------------------------------------------------------------=!
!
!  this module contains
!  1) the definition of all input parameters for DBO
!  2) the definition of namelist DBO
!  3) routines that allocate/deallocate data needed in input
!  Based upon original NEB implementation ( C.S. 17/10/2003 )
!
!=----------------------------------------------------------------------------=!
  !
  USE kinds,      ONLY : DP
  USE parameters, ONLY : nsx
  !
  IMPLICIT NONE
  !
  SAVE
  !
!=----------------------------------------------------------------------------=!
! BEGIN manual
!
!
! * DESCRIPTION OF THE INPUT FILE
!  (to be given as standard input)
!
!  The input file has the following layout:
!
!     &DBO
!       path_parameter_1,
!       path_parameter_2,
!       .......
!       path_parameter_Lastone
!     /
!     ATOMIC_SPECIES
!      slabel_1 mass_1 pseudo_file_1
!      slabel_2 mass_2 pseudo_file_2
!      .....
!     ATOMIC_POSITIONS
!      alabel_1  px_1 py_1 pz_1
!      alabel_2  px_2 py_2 pz_2
!      .....
!     POINTSET
!      mulabel  px_1 py_1 pz_1    
!     CARD_3
!     ....
!     CARD_N
!
!  -- end of input file --
!
! ... variables added for "path" calculations
!

!
! ... these are two auxiliary variables used in read_cards to
! ... distinguish among neb and smd done in the full phase-space
! ... or in the coarse-grained phase-space
!
  INTEGER :: n_inp_images
  INTEGER :: nat = 1
  REAL(DP) :: alat
  !
  CHARACTER(len=80) :: restart_mode
  ! specify how to start/restart the simulation
  CHARACTER(len=80) :: restart_mode_allowed(3)
  DATA restart_mode_allowed / 'from_scratch', 'restart', 'reset_counters' /
  !
  !
  INTEGER :: nstep_path
  INTEGER :: saving_interval
  !
  REAL(DP) :: search_cutoff
  REAL(DP) :: search_step
  INTEGER :: max_step
  
  !
  CHARACTER(len=80) :: search_method = 'fixed' 
  ! 'fixed'   all points are provided
  ! 'explore' the potential is explored point by point with a home made algorithm
  CHARACTER(len=80) :: search_method_scheme_allowed(2)
  DATA search_method_scheme_allowed / 'fixed', 'explore' /
  !
  INTEGER :: input_points = 0
  !
  INTEGER :: num_of_points = 0
  !
  ! wether the first scf calculation should use a 
  !  density already present in the tmp directory. ! NOT IMPLEMENTED !
  LOGICAL :: first_pot      = .false.
  !
  ! If the code should reuse the density, the wavefunctions and
  ! extrapolates the structure factors from previous points.
  !
  LOGICAL :: use_pot      = .false.
  LOGICAL :: use_wfc      = .false.
  LOGICAL :: use_struct   = .false.
  !
  NAMELIST / PATH / &
                    restart_mode,    &
                    saving_interval, &
                    num_of_points,   & 
                    search_method,   &
                    search_cutoff,   &
                    search_step,     &
                    max_step,        &
                    first_pot,       &
                    use_pot,         &
                    use_wfc,         &
                    use_struct
!
!    ATOMIC_POSITIONS
!
        REAL(DP), ALLOCATABLE :: pos(:,:)
        INTEGER, ALLOCATABLE :: typ(:)
        !
! ----------------------------------------------------------------------

CONTAINS

  SUBROUTINE allocate_dbo_input_ions( num_of_points )
    !
    INTEGER, INTENT(in) :: num_of_points
    !
    IF ( allocated( pos ) ) DEALLOCATE( pos )
    IF ( allocated( typ ) ) DEALLOCATE( typ )
    !
    ALLOCATE( pos( 3*nat, num_of_points ) )
    ALLOCATE( typ( nat ) )
    !
    pos(:,:) = 0.0
    !
    RETURN
    !
  END SUBROUTINE allocate_dbo_input_ions
  !
  SUBROUTINE deallocate_dbo_input_ions()
    !
    IF ( allocated( pos ) ) DEALLOCATE( pos )
    IF ( allocated( typ ) ) DEALLOCATE( typ )
    !
    RETURN
    !
  END SUBROUTINE deallocate_dbo_input_ions
  !
!=----------------------------------------------------------------------------=!
!
END MODULE dbo_input_parameters_module
!
!=----------------------------------------------------------------------------=!
