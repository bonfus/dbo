!
! Copyright (C) 2002-2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .

!---------------------------------------------------------------------------
MODULE dbo_base
  !---------------------------------------------------------------------------
  !
  ! ... This module contains most of the subroutines and functions needed by
  ! ... the implementation of "DBO" methods into Quantum ESPRESSO
  !
  ! ... Other relevant files are:
  !
  ! ... dbo_variables.f90
  ! ... dbo_io_routines.f90
  ! ... path_formats.f90
  ! ... compute_scf.f90
  !
  !
  ! ... Code written and maintained by Pietro Bonfa' and
  !       based on the original work by Carlo Sbraccia ( 2003-2007 )
  !
  USE kinds,     ONLY : DP
  USE constants, ONLY : eps32, pi, autoev, bohr_radius_angs, eV_to_kelvin
  USE dbo_io_units_module,  ONLY : iunpath
  USE io_global, ONLY : meta_ionode, meta_ionode_id
  USE mp,        ONLY : mp_bcast
  USE mp_world,  ONLY : world_comm
  !
  !
  PRIVATE
  !
  PUBLIC :: initialize_path
  PUBLIC :: build_pot
  PUBLIC :: print_data
  !
  CONTAINS    
    !
    ! ... module procedures
    !
    !-----------------------------------------------------------------------
    SUBROUTINE initialize_path()
      !-----------------------------------------------------------------------
      USE io_files,         ONLY : prefix, tmp_dir
      USE dbo_variables,   ONLY : path_allocation
      USE dbo_variables,   ONLY : pos, conv_potential, &
                                   dim1, num_of_points, pes, grad, &
                                   lexplore
      USE dbo_input_parameters_module, ONLY : restart_mode
      USE dbo_input_parameters_module, ONLY : nat, input_points
      USE dbo_input_parameters_module, ONLY : pos_      => pos
      USE dbo_io_units_module, ONLY : path_file, dat_file, crd_file, &
                                   xyz_file, axsf_file, color_file    
      USE dbo_io_routines,     ONLY : read_restart
      USE dbo_explorer,    ONLY : initialize_space
      USE dbo_io_units_module, ONLY : path_file
      !
      IMPLICIT NONE      
      !
      LOGICAL :: file_exists
      !
      ! ... output files are set
      !
      path_file = TRIM( prefix ) // ".path"
      dat_file  = TRIM( prefix ) // ".dat"
      color_file= TRIM( prefix ) // ".clr"
      crd_file  = TRIM( prefix ) // ".crd"
      xyz_file  = TRIM( prefix ) // ".xyz"
      axsf_file = TRIM( prefix ) // ".axsf"
      !
      ! ... the dimension of all "path" arrays (dim1) is set here
      ! ... ( it corresponds to the dimension of the configurational space )
      !
      !
      dim1 = 3*nat  
      CALL path_allocation()
      !
      ! This is the routine that initialize the sampled point
      ! it already takes into account the fact that the first 
      ! point is done (above)
      !
      IF ( lexplore ) CALL initialize_space()    
      !
      ! ... initialization of the allocatable arrays
      !
      pos(:,1:input_points) = pos_(1:dim1,1:input_points)
      !
      pes          = 0.0_DP
      grad         = 0.0_DP
      !
      conv_potential = .false.
      !
      !
      ! ... computed points path is read from file ( restart_mode == "restart" ) or
      ! ... generated from the input images ( restart_mode = "from_scratch" )
      !
      IF ( restart_mode == "restart" ) THEN
         !
         IF ( meta_ionode ) THEN
            !
            INQUIRE( FILE = path_file, EXIST = file_exists )
            !
            IF ( .NOT. file_exists ) THEN
               !
               WRITE( iunpath, &
                      & '(/,5X,"restart file ''",A,"'' not found: ", &
                      &   /,5X,"starting from scratch")' )  TRIM( path_file )
               !
               restart_mode = "from_scratch"
               !
            END IF
            !
         END IF
         !
         CALL mp_bcast( restart_mode, meta_ionode_id, world_comm )
         !
      END IF
      !
      IF ( restart_mode == "restart" ) THEN
         !
         CALL read_restart()
         !
      END IF      
      !
    END SUBROUTINE initialize_path
    !
    !------------------------------------------------------------------------
    SUBROUTINE build_pot( )
      USE dbo_variables, ONLY : lfixed, lexplore
      USE dbo_io_routines,  ONLY : write_dat_files, &
                                    write_restart   
      !
      LOGICAL :: stat = .false.
      !
      IF (lfixed) THEN
        CALL pot_fixed(stat)
      ELSE IF (lexplore) THEN
        CALL pot_explore(stat)
      END IF
      !
      CALL write_dat_files()
      !
      ! ... information is written on the standard output
      !
      !CALL write_output()
      !
      ! ... the restart file is written
      !
      CALL write_restart()

    !
    !------------------------------------------------------------------------      
    END SUBROUTINE build_pot
    
    SUBROUTINE pot_fixed( stat )
      !------------------------------------------------------------------------
      !
      USE dbo_variables, ONLY : num_of_points, &
                                 pending_point, &
                                 first_pot, &
                                 saving_interval
      !
      USE dbo_io_routines, ONLY : write_restart                                  
      !
      IMPLICIT NONE
      !
      LOGICAL, INTENT(OUT) :: stat
      !
      INTEGER  :: fii, lii, saved_index
      !
      !
      fii = 1
      IF ( pending_point /= 0 ) fii = pending_point
      !
      !IF ( saving_interval < 1 ) THEN
      !  lii = num_of_points
      !  ! dummy value to avoid division by 0 or strange results
      !  saving_interval = num_of_points + 1 
      !END IF  
      !
      lii = MIN( fii + saving_interval, num_of_points )      
      !
      saved_index = lii/saving_interval - 1 
      
      !
      DO WHILE (lii <= num_of_points)
        !
        !
        CALL compute_scf( fii, lii, stat )
        !
        IF ( .NOT. stat ) RETURN
        !
        IF ( lii == num_of_points ) EXIT
        !
        IF ( (lii/saving_interval) > saved_index ) THEN
          CALL write_restart()
          saved_index = saved_index + 1
        END IF
        !
        fii = lii + 1
        lii = MIN(fii + saving_interval, num_of_points)
        !
      END DO  
      !
      !
      RETURN
      !    
    
    END SUBROUTINE pot_fixed
    
    SUBROUTINE pot_explore( stat )
      !------------------------------------------------------------------------
      !
      USE dbo_variables,   ONLY : pending_point, num_of_points, &
                                   saving_interval
      USE dbo_explorer,    ONLY : fill_pos, plan_move
      USE dbo_io_routines, ONLY : write_restart, write_colors
      
      !
      IMPLICIT NONE
      !
      LOGICAL, INTENT(OUT) :: stat
      !
      INTEGER  :: fii, lii, saved_index
      !
      !
      !
      IF ( pending_point /= 0 ) THEN
        fii = pending_point
        lii = pending_point
      ELSE
        fii = 1
        lii = 1
        !
        ! this is the first point
        CALL compute_scf( fii, lii, stat )
        !
      END IF  
      !
      !
      ! The space has already been initialized in initialize_path routine
      !  the initializations takes care of the fact that the first point
      !  is computed with the previous call to compute_scf if starting
      !  from scratch.
      !
      !
      saved_index = lii/saving_interval
      !
      DO WHILE (lii<num_of_points)
        !
        CALL fill_pos ( fii, lii )
        !
        CALL compute_scf( fii, lii, stat )
        !
        IF ( .NOT. stat ) EXIT
        !
        ! ... the restart file is written
        !
        IF ( (lii/saving_interval) > saved_index ) THEN
          CALL write_restart()
          CALL write_colors()
          saved_index = saved_index + 1
        END IF
        !
        CALL plan_move(stat)
        !
        IF ( .NOT. stat ) EXIT        
        !
      END DO
      !
      ! update number of (converged) points
      num_of_points = lii
      !
      RETURN
      !    
    END SUBROUTINE pot_explore
    
    SUBROUTINE print_data()
        USE dbo_io_units_module,  ONLY : iunpath
        USE dbo_variables, ONLY : pos, pes, num_of_points, dim1
        !        
        IMPLICIT NONE
        !
        INTEGER :: i
        INTEGER :: lai ! last atom index
        !
        lai = dim1-2
        !
        write (iunpath,'(A)') 'Final results'
        DO i=1, num_of_points
            write (iunpath,'(4f14.8)') pos(lai:dim1,i), pes(i)
        END DO
        !
    END SUBROUTINE
    
    
    
END MODULE dbo_base
