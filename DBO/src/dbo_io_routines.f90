!
! Copyright (C) 2002-2006 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!----------------------------------------------------------------------------
MODULE dbo_io_routines
  !----------------------------------------------------------------------------
  !
  ! ... This module contains all subroutines used for I/O in path
  ! ... optimisations
  !
  ! ... Written by Carlo Sbraccia ( 2003-2006 )
  !
  USE kinds,      ONLY : DP
  USE constants,  ONLY : pi, autoev, bohr_radius_angs, eV_to_kelvin
  USE io_global,  ONLY : meta_ionode, meta_ionode_id
  USE mp,         ONLY : mp_bcast
  USE mp_world,   ONLY : world_comm
  !
  !USE basic_algebra_routines
  !
  IMPLICIT NONE
  !
  PRIVATE
  !
  PUBLIC :: new_image_init, get_new_image, stop_other_images
  PUBLIC :: write_dat_files, write_restart, write_colors, read_restart
  !
  CONTAINS
     !
     !
     !-----------------------------------------------------------------------
     SUBROUTINE new_image_init( nimage, fii, outdir )
       !-----------------------------------------------------------------------
       !
       ! ... this subroutine initializes the file needed for the
       ! ... parallelization among images
       !
       USE dbo_io_units_module, ONLY : iunnewimage
       USE io_files, ONLY : prefix
       USE dbo_variables, ONLY : tune_load_balance
       !
       IMPLICIT NONE
       !
       INTEGER,          INTENT(IN) :: nimage, fii
       CHARACTER(LEN=*), INTENT(IN) :: outdir
       !
       !
       IF ( nimage == 1 .OR. .NOT.tune_load_balance ) RETURN
       !
       OPEN( UNIT = iunnewimage, FILE = TRIM( outdir ) // &
           & TRIM( prefix ) // '.newimage' , STATUS = 'UNKNOWN' )
       !
       WRITE( iunnewimage, * ) fii + nimage
       !
       CLOSE( UNIT = iunnewimage, STATUS = 'KEEP' )
       !
       RETURN
       !
     END SUBROUTINE new_image_init
     !
     !-----------------------------------------------------------------------
     SUBROUTINE get_new_image( nimage, image, outdir )
       !-----------------------------------------------------------------------
       !
       ! ... this subroutine is used to get the new image to work on
       ! ... the "prefix.LOCK" file is needed to avoid (when present) that
       ! ... other jobs try to read/write on file "prefix.newimage"
       !
       USE io_files,       ONLY : iunnewimage, iunlock
       USE io_global,      ONLY : ionode
       !USE dbo_variables, ONLY : tune_load_balance
       !
       IMPLICIT NONE
       !
       INTEGER,          INTENT(IN)    :: nimage
       INTEGER,          INTENT(INOUT) :: image
       CHARACTER(LEN=*), INTENT(IN)    :: outdir
       !
       CHARACTER(LEN=256) :: filename
       LOGICAL            :: opened
       !
       !
       IF ( .NOT.ionode ) RETURN
       !
       IF ( nimage > 1 ) THEN
          !
          !IF ( tune_load_balance ) THEN
          !   !
          !   filename = TRIM( outdir ) // TRIM( prefix ) // '.LOCK'
          !   !
          !   open_loop: DO
          !      !
          !      OPEN( UNIT = iunlock, FILE = TRIM( filename ), &
          !           & IOSTAT = ioerr, STATUS = 'NEW' )
          !      !
          !      IF ( ioerr > 0 ) CYCLE open_loop
          !      !
          !      INQUIRE( UNIT = iunnewimage, OPENED = opened )
          !      !
          !      IF ( .NOT. opened ) THEN
          !         !
          !         OPEN( UNIT = iunnewimage, FILE = TRIM( outdir ) // &
          !             & TRIM( prefix ) // '.newimage' , STATUS = 'OLD' )
          !         !
          !         READ( iunnewimage, * ) image
          !         !
          !         CLOSE( UNIT = iunnewimage, STATUS = 'DELETE' )
          !         !
          !         OPEN( UNIT = iunnewimage, FILE = TRIM( outdir ) // &
          !             & TRIM( prefix ) // '.newimage' , STATUS = 'NEW' )
          !         !
          !         WRITE( iunnewimage, * ) image + 1
          !         !
          !         CLOSE( UNIT = iunnewimage, STATUS = 'KEEP' )
          !         !
          !         EXIT open_loop
          !         !
          !      END IF
          !      !
          !   END DO open_loop
          !   !
          !   CLOSE( UNIT = iunlock, STATUS = 'DELETE' )
          !   !
          !ELSE
             !
             image = image + nimage
             !
          !END IF
          !
       ELSE
          !
          image = image + 1
          !
       END IF
       !
       RETURN
       !
     END SUBROUTINE get_new_image
     !
     !-----------------------------------------------------------------------
     SUBROUTINE stop_other_images()
       !-----------------------------------------------------------------------
       !
       ! ... this subroutine is used to send a stop signal to other images
       ! ... this is done by creating the exit_file on the working directory
       !
       USE io_files,  ONLY : iunexit, exit_file
       USE io_global, ONLY : ionode
       !
       IMPLICIT NONE
       !
       !
       IF ( .NOT. ionode ) RETURN
       !
       OPEN( UNIT = iunexit, FILE = TRIM( exit_file ) )
       CLOSE( UNIT = iunexit, STATUS = 'KEEP' )
       !
       RETURN
       !
     END SUBROUTINE stop_other_images
     !
     !-----------------------------------------------------------------------
     SUBROUTINE write_dat_files()
       !-----------------------------------------------------------------------
       !
       USE cell_base,        ONLY : alat, at, bg
       USE ions_base,        ONLY : ityp, nat, atm, tau_format
       USE path_formats,     ONLY : dat_fmt, int_fmt, xyz_fmt, axsf_fmt
       USE dbo_variables,   ONLY : pos, grad, pes, num_of_points
       USE dbo_io_units_module, ONLY : iundat, iunxyz, iuncrd, iunaxsf, &
                                  dat_file, xyz_file, axsf_file, &
                                  crd_file
       !
       IMPLICIT NONE
       !
       !REAL(DP), ALLOCATABLE :: a(:), b(:), c(:), d(:), f(:), s(:), tau_out(:,:,:)
       REAL(DP), ALLOCATABLE :: tau_out(:,:,:)
       INTEGER               :: i, j, ia
       CHARACTER(LEN=256)    :: strcrd
       !
       !
       IF ( .NOT. meta_ionode ) RETURN
       !
       ! ... the *.dat and *.int files are written here
       !
       OPEN( UNIT = iundat, FILE = dat_file, STATUS = "UNKNOWN", &
             ACTION = "WRITE" )
       !
       DO i = 1, num_of_points
          !
          WRITE( UNIT = iundat, FMT = dat_fmt ) &
              ( pes(i) )*autoev
          !
       END DO
       !
       !
       CLOSE( UNIT = iundat )
       !
       ! ... the *.xyz file is written here
       !
       OPEN( UNIT = iunxyz, FILE = xyz_file, &
             STATUS = "UNKNOWN", ACTION = "WRITE" )
       !
       DO i = 1, num_of_points
          !
          WRITE( UNIT = iunxyz, FMT = '(I5,/)' ) nat
          !
          DO ia = 1, nat
             !
             WRITE( UNIT = iunxyz, FMT = xyz_fmt ) &
                 TRIM( atm( ityp( ia ) ) ), &
                 pos(3*ia-2,i) * bohr_radius_angs, &
                 pos(3*ia-1,i) * bohr_radius_angs, &
                 pos(3*ia-0,i) * bohr_radius_angs
             !
          END DO
          !
       END DO
       !
       CLOSE( UNIT = iunxyz )
       !
       ! ... the *.crd file is written here
       !
       OPEN( UNIT = iuncrd, FILE = crd_file, STATUS = "UNKNOWN", &
             ACTION = "WRITE" )
       ALLOCATE( tau_out(3,nat,num_of_points) )
       !
       DO i = 1, num_of_points
         DO ia = 1,nat
           tau_out(1,ia,i) = pos(3*ia-2,i)
           tau_out(2,ia,i) = pos(3*ia-1,i)
           tau_out(3,ia,i) = pos(3*ia-0,i)
         ENDDO
       ENDDO
       !
       SELECT CASE( tau_format )
          !
          ! ... convert output atomic positions from internally used format
          ! ... (bohr units, for path) to the same format used in input
          !
       CASE( 'alat' )
          strcrd = "ATOMIC_POSITIONS (alat)"
          tau_out(:,:,:) = tau_out(:,:,:) / alat
       CASE( 'bohr' )
          strcrd = "ATOMIC_POSITIONS (bohr)"
       CASE( 'crystal' )
          strcrd = "ATOMIC_POSITIONS (crystal)"
          tau_out(:,:,:) = tau_out(:,:,:) / alat
          DO i = 1, num_of_points
            call cryst_to_cart( nat, tau_out(1,1,i), bg, -1 )
          ENDDO
       CASE( 'angstrom' )
          strcrd = "ATOMIC_POSITIONS (angstrom)"
          tau_out(:,:,:) = tau_out(:,:,:) * bohr_radius_angs
       CASE DEFAULT
          strcrd = "ATOMIC_POSITIONS"
       END SELECT
       DO i = 1, num_of_points
          !
          DO ia = 1, nat
             !
             WRITE( UNIT = iuncrd, FMT = '(1x,a4,3f18.10)' ) &
                 TRIM( atm( ityp( ia ) ) ), &
                 tau_out(1:3,ia,i)
             !
          END DO
          !
       END DO
       !
       DEALLOCATE ( tau_out )
       CLOSE( UNIT = iuncrd )
       !
       ! ... the *.axsf file is written here
       !
       OPEN( UNIT = iunaxsf, FILE = axsf_file, STATUS = "UNKNOWN", &
             ACTION = "WRITE" )
       !
       WRITE( UNIT = iunaxsf, FMT = '(" ANIMSTEPS ",I3)' ) num_of_points
       WRITE( UNIT = iunaxsf, FMT = '(" CRYSTAL ")' )
       WRITE( UNIT = iunaxsf, FMT = '(" PRIMVEC ")' )
       WRITE( UNIT = iunaxsf, FMT = '(3F14.10)' ) &
           at(1,1) * alat * bohr_radius_angs, &
           at(2,1) * alat * bohr_radius_angs, &
           at(3,1) * alat * bohr_radius_angs
       WRITE( UNIT = iunaxsf, FMT = '(3F14.10)' ) &
           at(1,2) * alat * bohr_radius_angs, &
           at(2,2) * alat * bohr_radius_angs, &
           at(3,2) * alat * bohr_radius_angs
       WRITE( UNIT = iunaxsf, FMT = '(3F14.10)' ) &
           at(1,3) * alat * bohr_radius_angs, &
           at(2,3) * alat * bohr_radius_angs, &
           at(3,3) * alat * bohr_radius_angs
       !
       DO i = 1, num_of_points
          !
          WRITE( UNIT = iunaxsf, FMT = '(" PRIMCOORD ",I3)' ) i
          WRITE( UNIT = iunaxsf, FMT = '(I5,"  1")' ) nat
          !
          DO ia = 1, nat
             !
             WRITE( UNIT = iunaxsf, FMT = axsf_fmt ) &
                 TRIM( atm(ityp(ia)) ), &
                 pos(3*ia-2,i) * bohr_radius_angs, &
                 pos(3*ia-1,i) * bohr_radius_angs, &
                 pos(3*ia-0,i) * bohr_radius_angs, &
                 - grad(3*ia-2,i) / bohr_radius_angs, &
                 - grad(3*ia-1,i) / bohr_radius_angs, &
                 - grad(3*ia-0,i) / bohr_radius_angs
             !
          END DO
          !
       END DO
       !
       CLOSE( UNIT = iunaxsf )
       !
     END SUBROUTINE write_dat_files
     !
     !
     !-----------------------------------------------------------------------
     SUBROUTINE read_restart()
       !-----------------------------------------------------------------------
       !
       USE dbo_io_units_module,   ONLY : iunpath
       USE dbo_io_units_module,   ONLY : iunrestart, path_file
       !
       USE dbo_variables,         ONLY : dim1, lexplore, &
                                          pending_point, pos, pes, grad, &
                                          num_of_points, search_step, search_cutoff
       USE dbo_explorer,          ONLY : visit_history, unsampled_ngbrs, &
                                          cur_pos, cur_pos_index
                                          
                                          
       IMPLICIT NONE
       !
       INTEGER            :: i, j, ierr
       INTEGER            :: nim_inp
       CHARACTER(LEN=256) :: input_line
       LOGICAL            :: exists
       LOGICAL, EXTERNAL  :: matches
       !
       REAL(DP)            :: in_search_step, in_search_cutoff
       !
       !
       IF ( meta_ionode ) THEN
          !
          WRITE( UNIT = iunpath, &
                 FMT = '(/,5X,"reading file ''",A,"''",/)') TRIM( path_file )
          !
          INQUIRE( FILE = TRIM( path_file ), EXIST = exists )
          !
          IF ( .NOT. exists ) &
             CALL errore( 'read_restart', 'restart file not found', 1 )
          !
          OPEN( UNIT = iunrestart, FILE = path_file, STATUS = "OLD", &
                ACTION = "READ" )
          !
          READ( UNIT = iunrestart, FMT = '(256A)' ) input_line
          !
          IF ( matches( "RESTART INFORMATION", input_line ) ) THEN
             !
             !READ( UNIT = iunrestart, FMT = * ) istep_path
             !READ( UNIT = iunrestart, FMT = * ) nstep_path
             READ( UNIT = iunrestart, FMT = * ) pending_point
             !
          ELSE
             !
             ! ... mandatory fields
             !
             CALL errore( 'read_restart', 'RESTART INFORMATION missing', 1 )
             !
          END IF
          !
          READ( UNIT = iunrestart, FMT = '(256A)' ) input_line
          !
          IF ( matches( "NUMBER OF POINTS", input_line ) ) THEN
             !
             ! ... optional field
             !
             READ( UNIT = iunrestart, FMT = * ) nim_inp
             !
             IF ( nim_inp > num_of_points ) &
                CALL errore( 'read_restart', &
                             'Number of points in restart file greater than number of points in input file!', 1 )
             !
             READ( UNIT = iunrestart, FMT = '(256A)' ) input_line
             !
          ELSE
             !
             CALL errore( 'read_restart', &
                          'NUMBER OF POINTS missing', 1 )
             !
          END IF
          !
          IF ( .NOT. ( matches( "ENERGIES, POSITIONS AND GRADIENTS", &
                                 input_line ) ) ) THEN
             !
             ! ... mandatory fields
             !
             CALL errore( 'read_restart', &
                          'ENERGIES, POSITIONS AND GRADIENTS missing', 1 )
             !
          END IF
          !
          !
          DO i = 1, pending_point
             !
             READ( UNIT = iunrestart, FMT = * )
             READ( UNIT = iunrestart, FMT = * ) pes(i)
             !
             DO j = 1, dim1, 3
                !
                READ( UNIT = iunrestart, FMT = * ) &
                    pos(j+0,i),                    &
                    pos(j+1,i),                    &
                    pos(j+2,i),                    &
                    grad(j+0,i),               &
                    grad(j+1,i),               &
                    grad(j+2,i)
                !
             END DO
             !
          END DO
          !
          READ( UNIT = iunrestart, FMT = '(256A)', IOSTAT = ierr ) input_line
          !
          IF ( ( ierr == 0 ) .AND. lexplore ) THEN
             !
             IF ( matches( "EXPLORER INFO", input_line ) ) THEN
                READ( UNIT = iunrestart, FMT = * ) in_search_step, in_search_cutoff
                IF ( ABS (in_search_step - search_step) > 1.0d-9 ) &
                    CALL errore( 'read_restart', &
                            'Search step changed in input file!', 1 )
                IF ( in_search_cutoff < search_cutoff ) &
                    CALL errore( 'read_restart', &
                            'WARNING: search_cutoff smaller than in restart file. This can break the search algorithm!', 0 )             
                !
                READ( UNIT = iunrestart, FMT = '(256A)', IOSTAT = ierr ) input_line
             ELSE
               CALL errore( 'read_restart', &
                            'EXPLORED SPACE missing. Are you restarting a fixed calculation?', 1 )                
             END IF             
             !
             IF ( matches( "EXPLORED SPACE", input_line ) ) THEN
                !
                ! ... optional fields
                !
                !
                DO i = 1, pending_point
                   !
                   READ( UNIT = iunrestart, FMT = * )
                   !
                   !
                   READ( UNIT = iunrestart, FMT = * ) &
                       visit_history(1,i),                               &
                       visit_history(2,i),                               &
                       visit_history(3,i)
                   !
                   READ(UNIT = iunrestart, FMT = *) &
                       unsampled_ngbrs(i)
                   !
                   !
                   !
                END DO
                READ( UNIT = iunrestart, FMT = '(256A)', IOSTAT = ierr ) input_line
                IF ( matches( "RESTART POSITION", input_line ) ) THEN
                   !
                   READ(UNIT = iunrestart, FMT = *) cur_pos
                   READ(UNIT = iunrestart, FMT = *) cur_pos_index
                ELSE
                   CALL errore( 'read_restart', &
                               'RESTART POSITION missing.', 1 )                 
                END IF
                !
             ELSE
               CALL errore( 'read_restart', &
                            'EXPLORED SPACE missing. Are you restarting a fixed calculation?', 1 )                
             END IF
             !
          END IF
          !
          CLOSE( iunrestart )
          !
          !
       END IF
       !
       ! ... broadcast to all nodes
       !
       CALL mp_bcast( pending_point, meta_ionode_id, world_comm )
       !
       CALL mp_bcast( pos,      meta_ionode_id, world_comm )
       CALL mp_bcast( pes,      meta_ionode_id, world_comm )
       CALL mp_bcast( grad,     meta_ionode_id, world_comm )
       !
       IF ( lexplore ) THEN
          !
          CALL mp_bcast( visit_history   , meta_ionode_id, world_comm )
          CALL mp_bcast( unsampled_ngbrs , meta_ionode_id, world_comm )
          CALL mp_bcast( cur_pos         , meta_ionode_id, world_comm )
          CALL mp_bcast( cur_pos_index   , meta_ionode_id, world_comm )
          !
       END IF
       !
       RETURN
       !
     END SUBROUTINE read_restart
     !
     !-----------------------------------------------------------------------
     SUBROUTINE write_restart()
       !-----------------------------------------------------------------------
       !
       USE dbo_io_units_module, ONLY : iunrestart, path_file
       USE io_files, ONLY : tmp_dir
       USE dbo_variables,   ONLY : pending_point, lexplore, &
                                    dim1, num_of_points, pos, pes, grad
                                    
       USE path_formats,     ONLY : energy, dbo_explorered_space, &
                                        restart_first
       !
       IMPLICIT NONE
       !
       INTEGER            :: i, j
       !
       CHARACTER(LEN=6), EXTERNAL :: int_to_char
       !
       IF ( meta_ionode ) THEN
          !
          ! ... first the restart file is written in the working directory
          !
          OPEN( UNIT = iunrestart, FILE = path_file, STATUS = "UNKNOWN", &
                ACTION = "WRITE" )
          !
          CALL write_common_fields( iunrestart )
          !
          IF (  lexplore ) THEN
             !
             CALL write_explored_space( iunrestart )
             !
          END IF
          !
          CLOSE( iunrestart )
          !
       END IF
       !
       CONTAINS
         !
         !-------------------------------------------------------------------
         SUBROUTINE write_common_fields( in_unit )
           !-------------------------------------------------------------------
           !
           IMPLICIT NONE
           !
           INTEGER, INTENT(IN) :: in_unit
           !
           !
           WRITE( UNIT = in_unit, FMT = '("RESTART INFORMATION")' )
           !
           WRITE( UNIT = in_unit, FMT = '(I8)' ) pending_point
           !
           WRITE( UNIT = in_unit, FMT = '("NUMBER OF POINTS")' )
           !
           WRITE( UNIT = in_unit, FMT = '(I4)' ) num_of_points
           !
           WRITE( UNIT = in_unit, &
                  FMT = '("ENERGIES, POSITIONS AND GRADIENTS")' )
           !
           DO i = 1, pending_point
              !
              WRITE( UNIT = in_unit, FMT = '("Point: ",I4)' ) i
              !
              !
              WRITE( UNIT = in_unit, FMT = energy ) pes(i)
              !              !
              DO j = 1, dim1, 3
                 !                 !
                 !
                 WRITE( UNIT = in_unit, FMT = restart_first ) &
                     pos(j+0,i),                              &
                     pos(j+1,i),                              &
                     pos(j+2,i),                              &
                     grad(j+0,i),                         &
                     grad(j+1,i),                         &
                     grad(j+2,i)
                 !
                 !
              END DO
              !
           END DO
           !
           RETURN
           !
         END SUBROUTINE write_common_fields
         !
         !-------------------------------------------------------------------
         SUBROUTINE write_explored_space( in_unit )
           !-------------------------------------------------------------------
           USE  dbo_explorer, ONLY: visit_history,   &       ! positions visited
                                     unsampled_ngbrs, &
                                     cur_pos, cur_pos_index
           USE dbo_variables, ONLY: search_step, search_cutoff
           !
           IMPLICIT NONE
           !
           INTEGER, INTENT(IN) :: in_unit
           !
           !
           WRITE( UNIT = in_unit, FMT = '("EXPLORER INFO")' )
           WRITE( UNIT = in_unit, FMT = '(2(2X,F18.12))' ) search_step, search_cutoff
           !
           WRITE( UNIT = in_unit, FMT = '("EXPLORED SPACE")' )
           !
           DO i = 1, pending_point
              !
              WRITE( UNIT = in_unit, FMT = '("Point: ",I4)' ) i
              !
              !
              WRITE( UNIT = in_unit, FMT = dbo_explorered_space ) &
                  visit_history(1,i),                               &
                  visit_history(2,i),                               &
                  visit_history(3,i)
              !
              WRITE( UNIT = in_unit, &
                     FMT = '(I1)' ) unsampled_ngbrs(i)
              !
              !
           END DO
           !
           WRITE( UNIT = in_unit, FMT = '("RESTART POSITION")' )
           !
           WRITE( UNIT = in_unit, FMT = dbo_explorered_space ) cur_pos
           WRITE( UNIT = in_unit, FMT = '(I3)' ) cur_pos_index
           !
           RETURN
           !
         END SUBROUTINE write_explored_space
         !
     END SUBROUTINE write_restart
     !
     !-----------------------------------------------------------------------
     SUBROUTINE write_colors()
       !-----------------------------------------------------------------------
       USE dbo_io_units_module, ONLY : iuncolor, color_file
       USE dbo_explorer,  ONLY : unsampled_ngbrs
       USE dbo_variables, ONLY : dim1, pes, pos, pending_point, search_cutoff,lexplore
       USE path_formats,     ONLY : dbo_colors
       !
       IMPLICIT NONE
       !
       INTEGER            :: i, color
       !
       CHARACTER(LEN=6), EXTERNAL :: int_to_char
       !
       IF ( meta_ionode ) THEN
          !
          ! ... first the restart file is written in the working directory
          !
          OPEN( UNIT = iuncolor, FILE = color_file, STATUS = "UNKNOWN", &
                ACTION = "WRITE" )
          !
          WRITE( UNIT = iuncolor, FMT = '("POINTS COLOR")' )
          WRITE( UNIT = iuncolor, FMT = '("See Ref. ////. 1 Green, 2 Yellow, 3 Red.")' )
          WRITE( UNIT = iuncolor, FMT = '("Format: Point number, color, pos_x, pos_y, pos_z, energy")' )
          !
          IF (  lexplore ) THEN
             !
             DO i = 1, pending_point
                color = 2
                
                IF (unsampled_ngbrs(i) == 0) color = 1
                IF ( pes(i) >= search_cutoff ) color = 3
                
                
                WRITE( UNIT = iuncolor, FMT = dbo_colors ) &
                    i, &
                    color, &
                    pos(dim1-2,i), &
                    pos(dim1-1,i), &
                    pos(dim1,i), &
                    pes(i)
             END DO
             
             !
          END IF
          !
          CLOSE( iuncolor )
          !
       END IF       
       
     END SUBROUTINE write_colors
END MODULE dbo_io_routines
