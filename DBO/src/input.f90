!
! Copyright (C) 2002-2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!----------------------------------------------------------------------------
SUBROUTINE iodbo()
  !-----------------------------------------------------------------------------
  !
  ! ... Copy dbo-specific variables from dbo_input_variables into modules,
  ! ... Variables that have the same name in input file and in the modules
  ! ... are renamed (the logic is the same as in routine "iosys")
  ! ... This is done so that it is possible to use a different input parser
  !
  USE kinds,         ONLY : DP
  USE io_global,     ONLY : stdout
  USE io_files,      ONLY : tmp_dir 
  USE dbo_variables, ONLY : restart
  !
  ! renamed variables
  USE dbo_variables, ONLY : num_of_points_   => num_of_points, &
                             first_pot_       => first_pot, &
                             search_cutoff_   => search_cutoff, &
                             search_step_     => search_step, &
                             saving_interval_ => saving_interval, &
                             use_pot_         => use_pot, &
                             use_wfc_         => use_wfc, &
                             use_struct_      => use_struct, &
                             max_step_        => max_step, &
                             lfixed, lexplore
  
  !
  USE dbo_input_parameters_module, ONLY : restart_mode, num_of_points, &
                               search_method, search_cutoff, first_pot, &
                               search_step, max_step, saving_interval, &
                               use_pot, use_wfc, use_struct
  !
  IMPLICIT NONE
  !
  !
  SELECT CASE(trim( search_method ))
  !   !
  CASE( 'fixed' )
     !
     lfixed  = .true.
     lexplore = .false.
     !
  CASE( 'explore' )
     !
     lexplore  = .true.
     lfixed  = .false.
     !
  CASE DEFAULT
     !
     CALL errore( 'iodbo', 'search_method ' // &
                & trim( search_method ) // ' not implemented', 1 )
     !
  END SELECT
  !!
  SELECT CASE( trim( restart_mode ) )
  CASE( 'from_scratch' )
     !
     restart        = .false.
     !
  CASE( 'restart' )
     !
     restart = .true.
     !
  CASE DEFAULT
     !
     CALL errore( 'ioneb', &
                & 'unknown restart_mode ' // trim( restart_mode ), 1 )
     !
  END SELECT
  !
!
! check da mettere dopo iosys del pw
!
     !IF( io_level < 0) CALL errore ( 'iodbo', &
     !                  'DBO does not work with "disk_io" set to "none"', 1)
     !
     !
     !
  !
  ! ... "path"-optimization variables
  !
  num_of_points_   = num_of_points
  first_pot_       = first_pot
  search_cutoff_   = search_cutoff
  search_step_     = search_step
  max_step_        = max_step
  use_pot_         = use_pot
  use_wfc_         = use_wfc
  use_struct_      = use_struct
  
  !
  IF ( saving_interval < 1 ) THEN
    ! dummy value to avoid division by 0 or strange results
    saving_interval_ = num_of_points + 1 
  ELSE
    saving_interval_ = saving_interval
  END IF  
  !
  !
  CALL verify_dbo_tmpdir( tmp_dir )
  !
  RETURN
  !
END SUBROUTINE iodbo
!
!-----------------------------------------------------------------------
SUBROUTINE verify_dbo_tmpdir( tmp_dir )
  !-----------------------------------------------------------------------
  !
  USE wrappers,         ONLY : f_mkdir
  USE dbo_input_parameters_module, ONLY : restart_mode
  USE io_files,         ONLY : prefix,  delete_if_present
  USE dbo_variables,    ONLY : num_of_points
  USE mp_world,         ONLY : world_comm, mpime, nproc
  USE io_global,        ONLY : meta_ionode
  USE mp,               ONLY : mp_barrier
  !
  IMPLICIT NONE
  !
  CHARACTER(len=*), INTENT(inout) :: tmp_dir
  !
  INTEGER             :: image, proc, nofi
  LOGICAL             :: exst, parallelfs
  CHARACTER (len=256) :: file_path
  CHARACTER(len=6), EXTERNAL :: int_to_char
  !
  !
  file_path = trim( tmp_dir ) // trim( prefix )
  !
  !
  IF ( restart_mode == 'from_scratch' ) THEN
     !
     ! ... let us try to create the scratch directory
     !
     CALL check_tempdir ( tmp_dir, exst, parallelfs )
     !
  ENDIF
  !
  !
  ! ... if starting from scratch all temporary files are removed
  ! ... from tmp_dir ( only by the master node )
  !
  IF ( meta_ionode ) THEN
     !
     ! ... files needed by parallelization among images are removed
     !
     CALL delete_if_present( trim( file_path ) // '.newimage' )
     !
     ! ... file containing the broyden's history
     !
     IF ( restart_mode == 'from_scratch' ) THEN
        !
        CALL delete_if_present( trim( file_path ) // '.broyden' )
        !
     ENDIF
     !
  ENDIF ! end if ionode
  !
  nofi = num_of_points
  !
  DO image = 1, nofi
     !
     file_path = trim( tmp_dir ) // trim( prefix ) //"_" // &
                 trim( int_to_char( image ) ) // '/'
     !
     CALL check_tempdir ( file_path, exst, parallelfs )
     !
     ! ... if starting from scratch all temporary files are removed
     ! ... from tmp_dir ( by all the cpus in sequence )
     !
     IF ( restart_mode == 'from_scratch' ) THEN
        !
        DO proc = 0, nproc - 1
           !
           IF ( proc == mpime ) THEN
              !
              ! ... extrapolation file is removed
              !
              CALL delete_if_present( trim( file_path ) // &
                                    & trim( prefix ) // '.update' )
              !
              ! ... standard output of the self-consistency is removed
              !
              CALL delete_if_present( trim( file_path ) // 'PW.out' )
              !
           ENDIF
           !
           CALL mp_barrier( world_comm )
           !
        ENDDO
        !
     ENDIF ! end restart_mode
     !
  ENDDO ! end do image
  !
  RETURN
  !
END SUBROUTINE verify_dbo_tmpdir
