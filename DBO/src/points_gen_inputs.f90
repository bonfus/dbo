!
! Copyright (C) 2011 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE points_gen_inputs(parse_file_name,engine_prefix,npoint,root,comm)
!
USE mp_global, only : mp_rank

implicit none
!
INTEGER, EXTERNAL :: find_free_unit 
! 
character(len=*), intent(in) :: parse_file_name
character(len=*), intent(in) :: engine_prefix
integer, intent(out) :: npoint
integer, intent(in) :: root
integer, intent(in) :: comm
!
character(len=512) :: dummy
integer :: i, j
integer :: parse_unit, neb_unit
integer, allocatable :: unit_tmp(:)
integer :: unit_tmp_i
character(len=10) :: a_tmp

integer :: myrank


myrank =  mp_rank(comm)

parse_unit = find_free_unit()
open(unit=parse_unit,file=trim(parse_file_name),status="old")


! ---------------------------------------------------
! DBO INPUT PART
! ---------------------------------------------------

i=0
npoint = 0

neb_unit = find_free_unit()

open(unit=neb_unit,file='dbo.dat',status="unknown")
dummy=""
do while (LEN_TRIM(dummy)<1)
read(parse_unit,fmt='(A512)',END=10) dummy
enddo

if(trim(dummy)=="BEGIN") then
  do while (trim(dummy)/="END")
    read(parse_unit,*) dummy
    if(trim(dummy)=="BEGIN_DBO_INPUT") then

        read(parse_unit,'(A512)') dummy

      do while (trim(dummy)/="END_DBO_INPUT")
        if(myrank==root) write(neb_unit,*) trim(dummy)
        read(parse_unit,'(A512)') dummy
      enddo
    endif
    if(trim(dummy)=="POINTSET") then
      read(parse_unit,*) dummy
      do while (trim(dummy)/="END_POSITIONS")
        npoint = npoint + 1
        read(parse_unit,*) dummy
      end do
    endif
  enddo
else
  write(0,*) "key word BEGIN missing"
endif
close(neb_unit)
!------------------------------------------------
!
!
! ------------------------------------------------
! ENGINE INPUT PART
! ------------------------------------------------

allocate(unit_tmp(1:npoint))
unit_tmp(:) = 0

do i=1,npoint
unit_tmp(i) = find_free_unit()
enddo

do i=1,npoint
    if(i>=1.and.i<10) then
    write(a_tmp,'(i1)') i
    elseif(i>=10.and.i<100) then
    write(a_tmp,'(i2)') i
    elseif(i>=100.and.i<1000) then
    write(a_tmp,'(i3)')
    endif
    
    unit_tmp_i = unit_tmp(i)
    
    open(unit=unit_tmp_i,file=trim(engine_prefix)//trim(a_tmp)//".in")
    
    REWIND(parse_unit)
    
    dummy=""
    do while (LEN_TRIM(dummy)<1)
    read(parse_unit,fmt='(A512)',END=10) dummy
    enddo
    
    if(trim(dummy)=="BEGIN") then
    do while (trim(dummy)/="END")
        dummy=""
        do while (LEN_TRIM(dummy)<1)
            read(parse_unit,fmt='(A512)',END=10) dummy
        enddo
    
        if(trim(dummy)=="BEGIN_ENGINE_INPUT") then
        dummy=""
        do while (LEN_TRIM(dummy)<1)
            read(parse_unit,fmt='(A512)',END=10) dummy
        enddo
        
        do while (trim(dummy)/="BEGIN_POSITIONS")
            if(myrank==root) write(unit_tmp_i,'(A)') trim(dummy)
            read(parse_unit,'(A512)') dummy
        enddo
        do while (trim(dummy)/="POINTSET")
            if(myrank==root) write(unit_tmp_i,'(A)') trim(dummy)
            read(parse_unit,'(A512)') dummy
        enddo
        if(trim(dummy)=="POINTSET") then
            j=1
            read(parse_unit,'(A512)') dummy
                do while (trim(dummy)/="END_POSITIONS")
                    if (j==i) then
                        if(myrank==root) write(unit_tmp_i,'(A)') trim(dummy)
                    end if
                    read(parse_unit,'(A512)') dummy
                    j=j+1
                enddo
                !do while (trim(dummy)/="END_POSITIONS")
                !    read(parse_unit,'(A512)') dummy
                !enddo
                read(parse_unit,'(A512)') dummy
                do while (trim(dummy)/="END_ENGINE_INPUT")
                    if(myrank==root) write(unit_tmp_i,'(A)') trim(dummy)
                    read(parse_unit,'(A512)') dummy
                enddo
                endif
        endif
    enddo
    endif
    
    close(unit_tmp_i)
enddo

deallocate(unit_tmp)

close(parse_unit)
!
10 CONTINUE
!
end subroutine points_gen_inputs
