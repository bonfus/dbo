!
! Copyright (C) 2011-2013 Pietro Bonfa'
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
PROGRAM dbo
  !----------------------------------------------------------------------------
  !
  ! ... Double Born-Oppenheimer method for muon science
  !
  USE io_global,         ONLY : meta_ionode_id
  USE environment,       ONLY : environment_start, environment_end
  USE check_stop,        ONLY : check_stop_init
  USE mp,                ONLY : mp_bcast
  USE mp_global,         ONLY : mp_startup
  USE mp_world,          ONLY : world_comm, mpime, root
  USE read_input,        ONLY : read_input_file
  USE command_line_options,  ONLY : input_file_
  !
  USE dbo_variables,    ONLY : conv_potential
  USE dbo_base,         ONLY : initialize_path, build_pot, print_data
  USE dbo_read_namelists_module, ONLY : dbo_read_namelist
  !
  USE dbo_input_parameters_module, ONLY : input_points, num_of_points, &
                                           search_method, &
                                           allocate_dbo_input_ions, &
                                           deallocate_dbo_input_ions
  !
  IMPLICIT NONE
  !
  CHARACTER(len=256) :: engine_prefix, parsing_file_name
  INTEGER :: unit_tmp, i
  INTEGER, EXTERNAL :: find_free_unit, input_points_getarg
  CHARACTER(LEN=6), EXTERNAL :: int_to_char
  !
  !
  CALL mp_startup ( start_images=.true. )
  CALL environment_start ( 'DBO' )
  !
  ! INPUT RELATED
  !
  engine_prefix = "pw_"
  !
  ! ... open input file
  !
  IF ( input_file_ /= ' ') THEN
     WRITE(0,*) ""
     WRITE(0,*) "parsing_file_name: ", trim(input_file_)
     CALL points_gen_inputs ( trim(input_file_), engine_prefix, &
                            input_points, root, world_comm )
  ELSE
     WRITE(0,*) ""
     WRITE(0,*) "NO input file found, assuming nothing to parse."
     WRITE(0,*) "Searching argument -input_images or --input_images"
     IF ( mpime == root )  input_points = input_points_getarg ( )
     CALL mp_bcast(input_points,root, world_comm)
     !
     IF (input_points == 0) CALL errore('dbo', &
        'Neither a file to parse nor input files for each point found',1)
     !
  ENDIF
  !
  unit_tmp = find_free_unit () 
  open(unit=unit_tmp,file="dbo.dat",status="old")
  CALL dbo_read_namelist(unit_tmp)
  close(unit=unit_tmp)
  !
  ! warn if input_points != num_points
  ! this should be beautified by running iodbo somewhere above this point...
  if ( num_of_points /= input_points .and. trim( search_method ) == 'fixed' ) then
    CALL errore('DBO', &
        'Number of points differs from parsed points!',1)
  end if
  
  do i=1,input_points
    !
    IF ( i > 1 ) CALL clean_pw(.true.)
    parsing_file_name = trim(engine_prefix)//trim(int_to_char(i))//".in"
    !
    CALL read_input_file ( 'PW', parsing_file_name )
    CALL iosys()
    !
    IF ( i == 1 ) THEN
      CALL engine_to_path_nat()
      CALL engine_to_path_alat()
      CALL allocate_dbo_input_ions(num_of_points)
    END IF
    CALL engine_to_path_pos(i)
    !
  enddo
  !!
  !!
  CALL iodbo()
  CALL set_engine_output()
  !!
  !! END INPUT RELATED
  !!
  CALL check_stop_init()
  CALL initialize_path()
  CALL deallocate_dbo_input_ions()
  !
  CALL build_pot()
  !
  ! This function is already implemented and will be added in future releases
  !
  !CALL solve_pot
  !
  CALL print_data()
  !
  conv_potential = .true.
  CALL dbo_stop_run( conv_potential )
  !
  STOP
  !
END PROGRAM dbo
