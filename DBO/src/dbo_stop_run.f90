!
! Copyright (C) 2001-2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE dbo_stop_run( lflag )
  !----------------------------------------------------------------------------
  !
  ! ... Close all files and synchronize processes before stopping.
  ! ... Called at the end of the run with flag = .TRUE. (removes 'restart')
  ! ... or during execution with flag = .FALSE. (does not remove 'restart')
  !
  USE io_global,           ONLY : stdout
  USE mp_global,           ONLY : mp_global_end
  USE environment,         ONLY : environment_end
  USE dbo_variables,       ONLY : path_deallocation, lexplore
  USE dbo_explorer,        ONLY : deallocate_explorer
  USE dbo_io_units_module,      ONLY : iunpath
  !
  IMPLICIT NONE
  !
  LOGICAL, INTENT(IN) :: lflag
  !
  !
  CALL close_files(lflag)
  !
  ! as environment_end writes final info on stdout
  ! stdout has to be redirected to iunpath (dbo output)
  !
  stdout=iunpath
  !
  CALL environment_end( 'DBO' )
  !
  CALL clean_pw( .TRUE. )
  !
  CALL path_deallocation()
  !
  IF ( lexplore ) CALL deallocate_explorer()
  !
  CALL mp_global_end()
  !
  IF ( .not. lflag ) THEN
     !
     STOP 1
     !
  END IF
  !
END SUBROUTINE dbo_stop_run
!
!----------------------------------------------------------------------------
