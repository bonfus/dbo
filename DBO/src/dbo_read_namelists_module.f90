!
! Copyright (C) 2002-2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
MODULE dbo_read_namelists_module
  !----------------------------------------------------------------------------
  !
  !  ... this module handles the reading of input namelists
  !  ... written by: Carlo Cavazzoni
  !  --------------------------------------------------
  !
  USE kinds,     ONLY : DP
  USE dbo_input_parameters_module
  !
  IMPLICIT NONE
  !
  SAVE
  !
  PRIVATE
  !
  PUBLIC :: dbo_read_namelist
  !
  ! ... modules needed by read_xml.f90
  !
  !  ----------------------------------------------
  !
  CONTAINS
     !
     !=----------------------------------------------------------------------=!
     !
     !  Variables initialization for Namelist PATH
     !
     !=----------------------------------------------------------------------=!
     !
     !-----------------------------------------------------------------------
     SUBROUTINE dbo_defaults( )
       !-----------------------------------------------------------------------
       !
       USE dbo_input_parameters_module
       !
       IMPLICIT NONE
       !
       !
       ! ... defaults for "path" optimisations variables
       !
         restart_mode    = 'from_scratch'
         search_method   = 'fixed'
         search_cutoff   = 0.0
         search_step     = 0.2
         num_of_points   = 0
         saving_interval = 8
         max_step        = 2
         first_pot       = .FALSE.
         use_pot         = .false.
         use_wfc         = .false.
         use_struct      = .false.
       !
       ! for reading ions namelist we need to set calculation=relax
       !
       RETURN
       !
     END SUBROUTINE
     !
     !=----------------------------------------------------------------------=!
     !
     !  Broadcast variables values for Namelist NEB
     !
     !=----------------------------------------------------------------------=!
     !
     !-----------------------------------------------------------------------
     SUBROUTINE dbo_bcast()
       !-----------------------------------------------------------------------
       !
       USE io_global, ONLY: ionode_id
       USE mp,        ONLY: mp_bcast
       USE mp_world,  ONLY: world_comm
       USE dbo_input_parameters_module
       !
       IMPLICIT NONE
       !
       ! ... "path" variables broadcast
       !
       CALL mp_bcast ( restart_mode,      ionode_id, world_comm )
       CALL mp_bcast ( search_method,     ionode_id, world_comm ) 
       CALL mp_bcast ( search_cutoff,     ionode_id, world_comm ) 
       CALL mp_bcast ( search_step  ,     ionode_id, world_comm ) 
       CALL mp_bcast ( max_step     ,     ionode_id, world_comm ) 
       CALL mp_bcast ( num_of_points,     ionode_id, world_comm )
       CALL mp_bcast ( saving_interval,   ionode_id, world_comm )
       CALL mp_bcast ( first_pot,         ionode_id, world_comm )
       CALL mp_bcast ( use_pot,           ionode_id, world_comm )
       CALL mp_bcast ( use_wfc,           ionode_id, world_comm )
       CALL mp_bcast ( use_struct,        ionode_id, world_comm )
       !
       RETURN
       !
     END SUBROUTINE
     !
     !=----------------------------------------------------------------------=!
     !
     !  Namelist parsing main routine
     !
     !=----------------------------------------------------------------------=!
     !
     !-----------------------------------------------------------------------
     SUBROUTINE dbo_read_namelist(unit)
       !-----------------------------------------------------------------------
       !
       !  this routine reads data from standard input and puts them into
       !  module-scope variables (accessible from other routines by including
       !  this module, or the one that contains them)
       !  ----------------------------------------------
       !
       ! ... declare modules
       !
       USE io_global, ONLY : ionode, ionode_id
       USE mp,        ONLY : mp_bcast
       USE mp_world,  ONLY : world_comm
       !
       IMPLICIT NONE
       !
       ! ... declare variables
       !
       INTEGER, intent(in) :: unit
       !
       !
       ! ... declare other variables
       !
       INTEGER :: ios
       character(len=255) :: message
       !
       ! ... end of declarations
       !
       !  ----------------------------------------------
       !
       !
       ! ... default settings for all namelists
       !
       CALL dbo_defaults( )
       !
       ! ... Here start reading standard input file
       !
       ! ... PATH namelist
       !
       ios = 0
       IF ( ionode ) THEN
          !
          READ( unit, path, iostat = ios , iomsg=message)
          !
       END IF
       CALL mp_bcast( ios, ionode_id, world_comm )
       IF( ios /= 0 ) THEN
          CALL errore( ' dbo_read_namelists ', &
                     & ' reading namelist path: ' // message, ABS(ios) )
       END IF
       !
       !
       CALL dbo_bcast( )
       !
       RETURN
       !
     END SUBROUTINE dbo_read_namelist
     !
END MODULE dbo_read_namelists_module
