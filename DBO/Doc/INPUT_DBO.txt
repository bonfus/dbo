*** FILE AUTOMATICALLY CREATED: DO NOT EDIT, CHANGES WILL BE LOST ***

------------------------------------------------------------------------
INPUT FILE DESCRIPTION

Program: neb.x / NEB / Quantum Espresso
------------------------------------------------------------------------


Input data format: { } = optional, [ ] = it depends, | = or

All quantities whose dimensions are not explicitly specified are in
RYDBERG ATOMIC UNITS

BEWARE: TABS, DOS <CR><LF> CHARACTERS ARE POTENTIAL SOURCES OF TROUBLE

General input file structure:
===============================================================================

dbo.x DOES NOT READ FROM STANDARD INPUT
There are two ways for running a calculation with dbo.x:
1) specifying a file to parse with the ./neb.x -inp or
 neb.x -input command line option.
2) or specifying the number of copies of PWscf input ./neb.x -input\_images.

For case 1) a file containing KEYWORDS has to be written (see below).
These KEYWORDS tells the parser which part of the file regards neb specifics
and which part regards the energy/force engine (at the moment only PW).
After the parsing different files are generated: neb.dat, with
neb specific variables and a set of pw_*.in PWscf input files like
one for each input position. All options for a single SCF calculation apply.

The general structure of the file to be parsed is:

BEGIN
BEGIN_PATH_INPUT
... neb specific namelists and cards
END_PATH_INPUT
BEGIN_ENGINE_INPUT
...pw specific namelists and cards
BEGIN_POSITIONS
BEGIN_POSITIONS
...pw ATOMIC_POSITIONS card
POINTSET
...continuation of ATOMIC_POSITIONS card
END_POSITIONS
... other pw specific cards
END_ENGINE_INPUT
END

For case 2) dbo.dat and all pw_1.in, pw_2.in ... should be already present.

Structure of the input data (file dbo.dat) :
===============================================================================

&PATH
  ...
/



========================================================================
NAMELIST: &PATH

   +--------------------------------------------------------------------
   Variable:       search_method
   
   Type:           CHARACTER
   Default:        'none'
   Description:    a string describing the task to be performed:
                      'fixed',
                      'explore'
   +--------------------------------------------------------------------
   
   +--------------------------------------------------------------------
   Variable:       restart_mode
   
   Type:           CHARACTER
   Default:        'from_scratch'
   Description:    'from_scratch'  : from scratch
                   
                   'restart'       : from previous interrupted run
   +--------------------------------------------------------------------
   
   +--------------------------------------------------------------------
   Variable:       num_of_points
   
   Type:           INTEGER
   Default:        0
   Description:    Maximum number of points used to sample the potential.
   +--------------------------------------------------------------------
   
   +--------------------------------------------------------------------
   Variable:       first_pot
   
   Type:           LOGICAL
   Default:        .FALSE.
   Description:    If set to TRUE the code will load the density from the directory
                   tmpdir/prefix_1/prefix.save.
                   This option is for the systems that have history dependent structural
                   optimization.
                   IMPORTANT NOTE: this overrides use_pot value to .true.
   +--------------------------------------------------------------------
   
   +--------------------------------------------------------------------
   Variable:       search_step
   
   Type:           REAL
   Default:        0.2D0 Bohr
   Description:    Step for potential exploration, in Bohr units.
   +--------------------------------------------------------------------
   
   +--------------------------------------------------------------------
   Variable:       max_step
   
   Type:           INTEGER
   Default:        2
   Description:    The maximum number of "search_step"s that the code will use while
                   exploring the total energies for the various positions of the
                   exploring particle.
                   This is used to limit the distance between the nuclear positions
                   in the 'explore' algorithm.
                   IMPORTANT NOTE: during the exploration process the code will
                   perform self consistent calculations in the +1 and -1 directions
                   along x,y,z with respect to the current position.
                   Afterwards, the code will decide where to move with respect to
                   the current position (and not with respect to the last explore point).
                   This means that a value smaller than 1 will block the exploration.
                   The parameter therefore defines the maximum distance (search_step*max_step)
                   between the exploring particle positions during the exploration.
   +--------------------------------------------------------------------
   
   +--------------------------------------------------------------------
   Variable:       search_cutoff
   
   Type:           REAL
   Default:        0.1D0 Hartree atomic units
   Description:    Maximum energy to be explored ( in Hartree atomic units! ).
   +--------------------------------------------------------------------
   
   +--------------------------------------------------------------------
   Variable:       use_pot
   
   Type:           LOGICAL
   Default:        .TRUE.
   Description:    If. TRUE. the exploration is  performed by reusing the density of
                   the previous path points. Useful speedup the calculation.
   +--------------------------------------------------------------------
   
   +--------------------------------------------------------------------
   Variable:       use_wfc
   
   Type:           LOGICAL
   Default:        .FALSE.
   Description:    If. TRUE. the exploration is performed by reusing the wavefunctions of
                   the previous path points. Can be useful speedup the calculation.
                   Only works with wfcollect=.true.
   +--------------------------------------------------------------------
   
   +--------------------------------------------------------------------
   Variable:       use_struct
   
   Type:           LOGICAL
   Default:        .FALSE.
   Description:    If. TRUE. the exploration is performed by updating the structure factors
                   using the previous path points. TO BE TESTED.
   +--------------------------------------------------------------------
   
===END OF NAMELIST======================================================


