\documentclass[12pt,a4paper]{article}
\def\version{1.0}
\def\qe{{\sc Quantum ESPRESSO}}
\def\DBO{\texttt{DBO}} % to be decided
\usepackage{html}

% BEWARE: don't revert from graphicx for epsfig, because latex2html
% doesn't handle epsfig commands !!!
\usepackage{graphicx}

\textwidth = 17cm
\textheight = 24cm
\topmargin =-1 cm
\oddsidemargin = 0 cm

\def\pwx{\texttt{pw.x\ }}
\def\cpx{\texttt{cp.x\ }}
\def\phx{\texttt{ph.x\ }}
\def\dbox{\texttt{dbo.x\ }}
\def\configure{\texttt{configure}}
\def\PWscf{\texttt{PWscf}}
\def\PHonon{\texttt{PHonon}}
\def\CP{\texttt{CP}}
\def\PostProc{\texttt{PostProc}}
\def\make{\texttt{make}}

\begin{document} 
\author{}
\date{}

\def\qeImage{../../Doc/quantum_espresso.pdf}
\def\democritosImage{../../Doc/democritos.pdf}

\begin{htmlonly}
\def\qeImage{../../Doc/quantum_espresso.png}
\def\democritosImage{../../Doc/democritos.png}
\end{htmlonly}

\title{
  \includegraphics[width=5cm]{\qeImage} \hskip 2cm
  \includegraphics[width=6cm]{\democritosImage}\\
  \vskip 1cm
  % title
  \Huge User's Guide for \DBO\
  \Large (version \version)
}
%\endhtmlonly

\maketitle

\tableofcontents

\section{Introduction}

This guide covers the usage of \DBO, version \version: 
an open-source package for the calculation of zero point energy 
using the double Born-Oppenheimer approximation.

This guide assumes that you know the physics 
that \DBO\ describes and the methods it implements.
It also assumes  that you have already installed,
or know how to install, \qe. If not, please read
the general User's Guide for \qe, found in 
directory \texttt{Doc/} two levels above the 
one containing this guide; or consult the web site:\\
\texttt{http://www.quantum-espresso.org}.


\section{People and terms of use}
This package contains various software that are subject to different license agreements.
The main code is in the \texttt{src/} directory while the tools for the solution of the Shr\"odinger
equation are provided within the \texttt{SHsolve/} directory.
The terms of use for each of the sub-programs were agreed with the original authors of the codes and
 are detailed in the following paragraphs.

\paragraph{DBO} The current implementation of \DBO\ is provided by Pietro Bonf\`a and is
largely inspired by the NEB code written by Carlo Sbraccia and maintained by Layla Martin-Samos,
Paolo Giannozzi, Stefano de Gironcoli.

\DBO\ is free software, released under the 
GNU General Public License. \\ See
\texttt{http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt}, 
or the file License in the distribution).
    
We shall greatly appreciate if scientific work done using this code will 
contain an explicit acknowledgement and the following reference:
\begin{quote}
Efficient and Reliable Strategy for Identifying Muon Sites Based on the Double Adiabatic Approximation \\
P. Bonf\`a, F. Sartori, and R. De Renzi\\
J. Phys. Chem. C, Articles ASAP (As Soon As Publishable) \\
DOI: 10.1021/jp5125876
\end{quote}

\paragraph{schroedsolve} This program was made by Stefan Janecek and Eckhard Krotscheck and published under \texttt{Standard CPC licence (http://cpc.cs.qub.ac.uk/licence/licence.html)}. A slightly modified version of the \texttt{3dsh} 
program which allows to parse and interpolate a potential defined on scattered data points is provided with
permission from the original authors. Any result published with this code must contain the following reference:
\begin{quote}
S. Janecek, E. Krotscheck, A fast and simple program for solving local Schr\"odinger equations in two and three dimensions,\\ Computer Physics Communications, Volume 178, Issue 11, 1 June 2008, \\Pages 835-842, ISSN 0010-4655, \\http://dx.doi.org/10.1016/j.cpc.2008.01.035.
\end{quote}
Redistribution of the code is not possible without written permission of the aforementioned authors.

\paragraph{fschr} This program is a Fortran90 port of the code by Zbigniew Romanowski \\(\texttt{http://wielkiewyzwania.pl/?page\_id=517}) and is provided under GNU GPL. Any work published using this code must contain the following reference:
\begin{quote}
Zbigniew Romanowski 
B-spline solver for one-electron Schr\"odinger equation,\\
Molecular Physics Volume 109, Issue 22, 2011 \\
pages 2679-2691, 
DOI: 10.1080/00268976.2011.631055
\end{quote}
and an explicit mention of the Fortran implementation provided with the DBO package.

\section{Compilation}

\DBO\ is a package tightly bound to \qe.
For instruction on how to download and compile \qe, please refer 
to the general Users' Guide, available in file \texttt{Doc/user\_guide.pdf}
under the main \qe\ directory, or in web site 
\texttt{http://www.quantum-espresso.org}.

Once \qe\ is correctly configured, \DBO\ can be automatically 
downloaded, unpacked and compiled by
just typing \texttt{make dbo}, from the main \qe\ directory.
\texttt{make dbo} will produce 
the following codes in \texttt{DBO/src}:
\begin{itemize}
\item \dbox: reconstruct the potential felt by a atom up to a specified threshold.
\item \texttt{SHsolver/1D/FGH.py}: solves the Schr\"odinger equation in 1D with the Fourier Grid Hamiltonian method (adapted from github.com/aeberspaecher/FGH/).
\item \texttt{SHsolver/schroedsolve/3dsh}: solves the Schr\"odinger equation in 3D with the imaginary time evolution algorithm.
\item \texttt{SHsolver/fschr/fschr}: solves the Schr\"odinger equation with a spline base and finite elements approach.

\end{itemize}

Symlinks to executable programs will be placed in the
\texttt{bin/} subdirectory of the main \qe\  directory.


\subsection{Running examples}
\label{SubSec:Examples}
As a final check that compilation was successful, you may want to run some or
all of the examples (presently only one). To run the examples, you should follow this procedure:
\begin{enumerate}   
\item Edit the \texttt{environment\_variables} file in the main \qe \ directory,
 setting the following variables as needed: 
\begin{quote}
   BIN\_DIR: directory where executables reside\\
   PSEUDO\_DIR: directory where pseudopotential files reside\\
   TMP\_DIR: directory to be used as temporary storage area
\end{quote}
\end{enumerate}
The default values of BIN\_DIR and PSEUDO\_DIR should be fine, 
unless you have installed things in nonstandard places. The TMP\_DIR 
variable must point to a directory where you have read and write permissions, 
with enough available space to host the temporary files produced by the 
example runs, and possibly offering good I/O performance (i.e., don't 
use an NFS-mounted directory). \textbf{N.B.} Use a dedicated directory,
because the example script will automatically delete all data inside TMP\_DIR.
If you have compiled the parallel version of \qe\ (this
is the default if parallel libraries are detected), you will usually
have to specify a driver program (such as \texttt{mpirun} or \texttt{mpiexec}) 
and the number of processors: see Sec.\ref{SubSec:para} for
details. In order to do that, edit again the \texttt{environment\_variables} 
file
and set the PARA\_PREFIX and PARA\_POSTFIX variables as needed. 
Parallel executables will be started with a command line like this: 
\begin{verbatim}
      $PARA_PREFIX dbo.x $PARA_POSTFIX -inp file.in > file.out
\end{verbatim}
For example, the command for IBM's POE looks like this:
\begin{verbatim}
      poe dbo.x -procs 4 -inp file.in > file.out
\end{verbatim}
therefore you will need to set PARA\_PREFIX="poe", PARA\_POSTFIX="-procs 4". 
Furthermore, if your machine does not support interactive use, you
must run the commands specified below through the batch queuing
system installed on that machine. Ask your system administrator for
instructions. 

Go to DBO/examples and execute:
\begin{verbatim}
      ./run_example
\end{verbatim}
This will create a subdirectory \texttt{results/} containing the input and
output files generated by the calculation.

The \texttt{reference/} subdirectory contains
verified output files, that you can check your results against. They
were generated on a Linux PC using the Intel compiler. On different
architectures the precise numbers could be slightly different, in
particular if different FFT dimensions are automatically selected. For
this reason, a plain diff of your results against the reference data
doesn't work, or at least, it requires human inspection of the
results. 

\section{Parallelism}
\label{Sec:para}

The \DBO\ code is interfaced to \PWscf, which is used as computational engine 
for total energies and forces. It can therefore take advantage from the two 
parallelization paradigms currently implemented in \qe, namely
  Message Passing Interface (MPI) and OpenMP threads, and exploit
all \PWscf-specific parallelization options.
For a detailed information about parallelization in \qe, 
please refer to the general documentation.

As \DBO \ makes several independent evaluations of energy and forces at each step of
the path optimization (one for each point in the points set or during the potential exploration)
 it is possible to distribute them among processors using an additional level of 
parallelization (see later).
%corresponding to a point in configuration space (i.e. to
%a different set of atomic positions).

\subsection{Running on parallel machines}
\label{SubSec:para}

Parallel execution is strongly system- and installation-dependent. 
Typically one has to specify:
\begin{enumerate}
\item a launcher program (not always needed), 
such as \texttt{poe}, \texttt{mpirun}, \texttt{mpiexec},
  with the  appropriate options (if any);
\item the number of processors, typically as an option to the launcher
  program, but in some cases to be specified after the name of the
  program to be
  executed; 
\item the program to be executed, with the proper path if needed: for
  instance, \texttt{./dbo.x}, or \texttt{\$HOME/bin/dbo.x}, or
  whatever applies; 
\item other \texttt{PWscf}-specific parallelization options, to be
  read and interpreted by the running code; 
\item the number of image groups used by \DBO (see next subsection).
\end{enumerate}
Items 1) and 2) are machine- and installation-dependent, and may be 
different for interactive and batch execution. Note that large
parallel machines are  often configured so as to disallow interactive
execution: if in doubt, ask your system administrator.
Item 3) also depend on your specific configuration (shell, execution
path, etc). 
Item 4) is optional but may be important: see the following section
for the meaning of the various options.

For illustration, here is how to run \texttt{dbo.x} \ on 16 processors partitioned into
4 image groups (4 processors each), for a path containing at least 4 images with POE:
\begin{verbatim}
   poe dbo.x -procs 16 -ni 4 -i input
\end{verbatim}


\subsection{Parallelization levels}

Data structures are distributed across processors.
Processors are organized in a hierarchy of groups, 
which are identified by different MPI communicators level.
The groups hierarchy is as follow:
\begin{verbatim}
  world - image_group - PWscf hierarchy
\end{verbatim}

\texttt{world}: is the group of all processors (MPI\_COMM\_WORLD).

\texttt{image\_group}: Processors can then be divided into different image groups 
(the name is reminiscent of the parallelization among the images of the NEB path optimization)
each of them taking care of one or more points.

%{\bf Communications}:
Image parallelization is of loosely coupled type, so that processors belonging to
different image groups communicate only once in a while, 
whereas processors within the same image group are tightly coupled and 
communications are more significant (please refer to the user guide of \PWscf).

The default number of image groups is one, corresponding to the option
 \texttt{-ni 1} (or, equivalently,  \texttt{-nimage 1}).

{\bf NOTA BENE:} In \texttt{explore} mode {\bf do not use more than two images!} 
A maximum of 6 points are done at the same time at the beginnig of the simulation
 but during the exploration process it
is possible that only one scf calculation is done between exploration steps.

\newpage

\section{Using \DBO}

DBO calculations with \dbox can be started in two different ways:
\begin{enumerate}
\item by reading a single input file, specified with the command line 
option \texttt{-i} (or \texttt{-in}, or \texttt{-inp} );
\item by specifying the number $N$ of points with the command line option 
\texttt{-input\_images N}, and providing the input data for \DBO\ in
a file named \texttt{neb.dat} and for the \PWscf\ engine in the files
 \texttt{pw\_X.in} ($X=1,...,N$, see also below).
\end{enumerate}

In the first case, the input file contains keywords (described here below) 
that enable the code to distinguish between parts of the input containing 
DBO-specific parameters and parts containing instructions for the 
computational engine (only PW is currently supported).

\noindent\textbf{N.B.:} the \dbox\ code does not read from standard input,
so that input redirection (e.g., \texttt{dbo.x < dbo.in ...}) cannot be used.

The general structure of the file to be parsed should be as follows:
\begin{verbatim}
BEGIN
BEGIN_DBO_INPUT
~... dbo namelist 
END_DBO_INPUT
BEGIN_ENGINE_INPUT
~...pw specific namelists and cards
BEGIN_POSITIONS
~...pw ATOMIC_POSITIONS card
POINTSET
~...pw ATOMIC_POSITIONS card continue
END_POSITIONS
~... other pw specific cards
END_ENGINE_INPUT
END
\end{verbatim}

After the parsing is completed, several files are generated by \DBO, more 
specifically: \texttt{dbo.dat}, with DBO-related input data, 
and a set of input files in the \PWscf\ format, \texttt{pw\_1.in}, \ldots,
\texttt{pw\_N.in}, one for each atomic position (point) found in 
the original input file anfter the POINTSET directive.
For the second case, the \texttt{dbo.dat} file and all \texttt{pw\_X.in} 
should be already present in the directory where the code is started.
A detailed description of all DBO-specific input variables is contained 
in the input description files \texttt{Doc/INPUT\_DBO.*}, while for the
\PWscf\ engine all the options of a \texttt{scf} calculation apply (see
\texttt{PW/Doc/INPUT\_PW.*} and \texttt{example01} in the 
\texttt{NEB/examples} directory). 

A DBO calculation will produce a number of output files containing additional 
information on the potential value and gradients. The following files are created in the
directory were the code is started:
\begin{description}
\item[\texttt{prefix.dat}]
is a file containing the potential energy in eV that can be used to solve the Schr\"odinger equation for the light
particle in the double BO approximation.
%\item[\texttt{prefix.int}]
%contains an interpolation of the path energy profile that pass exactly through each
%image; it is computed using both the image energies and their derivatives
%\item[\texttt{prefix.path}]
%information used by \qe\ 
%to restart a path calculation, its format depends on the input
%details and is undocumented
\item[\texttt{prefix.axsf}]
atomic positions of all points in the XCrySDen animation format:
to visualize it, use \texttt{xcrysden -\--axsf prefix.axsf}
\item[\texttt{prefix.xyz}]
atomic positions of all points in the generic xyz format, used by
many quantum-chemistry softwares
\item[\texttt{prefix.crd}]
points in the input format used by \pwx, suitable for a manual
restart of the calculation
\end{description}
where \texttt{prefix} is the \PWscf\ variable specified in the input.
The more verbose output from the \PWscf\ engine is not printed on the standard
output, but is redirected into a file stored in the image-specific temporary 
directories (e.g. \texttt{outdir/prefix\_1/PW.out} for the first image, etc.).

DBO is provided with a serier of tools that can be used to obtain the DBO total energy 
by solving the Schr\"odinger equation for the particle in the potential already obtained:
\begin{enumerate}
\item 
\texttt{tools/data\_interp.py} is a tool to inderpolate the potential using scipy routines implementing 
nearest, linear and Radial Basis Function interpolations. It produces a .xsf XCrySDen file showing the
potential energy (in eV) and a simple input file for the 3D Schr\"odinger equation
solver provided with this package. BEWARE: the input file should be customized and checked.
\item
\texttt{interpolator} is a fortran library tha uses QS3 and RBF interpolations as provided in http://people.sc.fsu.edu/~jburkardt/ . 
It can be usefull to interface your code with the scattered data provided by DBO.
\item 
\texttt{SHsolver}
is the directory containing various 3D shroedinger equation solvers already interfaced with DBO output.
Many different solutions are present:
\begin{itemize}
\item
\texttt{SHsolver/schroedsolve/3dsch} is based on imaginary time evolution and assumes {\bf periodic boundary conditions}.
It was made by S. Janecek and E. Krotscheck (Institut f\"ur Theoretische Physik, Johannes Kepler Universit\"at Linz, Austria) and is discussed in the reference given above.
The program is well documented and additional input parameters are discussed in \texttt{sch\_solver\_adds.tex}. 
The 2d solver and original example input files are left for reference.
Please be careful when providing potential files. The code's input parameter \texttt{H2M} defines the quantity $\hbar ^{2} / 2 m$. 
If you are working in Hartree atomic units and you are considering a {\bf muon} this quantity is about 0.02418 Ha*Bohr$^{2}$.
\item
\texttt{SHsolver/fschr} is a fortran port of the code by Zbigniew Romanowski. It assumes Diriclet (i.e. zero) boundary conditions.
This code is still experimental and only works with intel compilers with MKL library
(the MKL library is needed to compile the FEAST routine which solves the sparse matrix eigenvalue problem)

\end{itemize}

\end{enumerate}

{\bf NOTA BENE:} If \texttt{fschr} or \texttt{3dsch} programs crash during the interpolation process use:
\begin{verbatim}
    ulimit -s unlimited
\end{verbatim}
in the terminal where running the application.


\section{Performances}

\DBO \ requires roughly the time and memory 
needed for a single SCF calculation, times
\texttt{num\_of\_images}, times the number 
of DBO iterations needed to reach convergence.
We
refer the reader to the PW user\_guide for more information.

\section{Troubleshooting}

Almost all problems in \DBO \ arise from incorrect input data 
and result in
error stops. Error messages should be self-explanatory, but unfortunately
this is not always true. If the code issues a warning messages and continues,
pay attention to it but do not assume that something is necessarily wrong in
your calculation: most warning messages signal harmless problems.

\end{document}
