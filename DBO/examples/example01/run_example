#!/bin/sh

# run from directory where this script is
cd `echo $0 | sed 's/\(.*\)\/.*/\1/'` # extract pathname
EXAMPLE_DIR=`pwd`

# check whether ECHO has the -e option
if test "`echo -e`" = "-e" ; then ECHO=echo ; else ECHO="echo -e" ; fi

$ECHO
$ECHO "$EXAMPLE_DIR : starting"
$ECHO
$ECHO "This example illustrates how to use dbo.x and ph.x+qha.x to calculate "
$ECHO "the electronic levels of H atoms in HI."


# set the needed environment variables
. ../../../environment_variables

# required executables and pseudopotentials
BIN_LIST="pw.x phcg.x dbo.x"
PSEUDO_LIST="I.pz-hgh.UPF H.pz-vbc.UPF"

$ECHO
$ECHO "  executables directory: $BIN_DIR"
$ECHO "  pseudo directory:      $PSEUDO_DIR"
$ECHO "  temporary directory:   $TMP_DIR"
$ECHO
$ECHO "  checking that needed directories and files exist...\c"

# check for directories
for DIR in "$BIN_DIR" "$PSEUDO_DIR" ; do
    if test ! -d $DIR ; then
        $ECHO
        $ECHO "ERROR: $DIR not existent or not a directory"
        $ECHO "Aborting"
        exit 1
    fi
done
for DIR in "$TMP_DIR" "$EXAMPLE_DIR/results" ; do
    if test ! -d $DIR ; then
        mkdir $DIR
    fi
done
cd $EXAMPLE_DIR/results

# check for executables
for FILE in $BIN_LIST ; do
    if test ! -x $BIN_DIR/$FILE ; then
        $ECHO
        $ECHO "ERROR: $BIN_DIR/$FILE not existent or not executable"
        $ECHO "Aborting"
        exit 1
    fi
done

# check for pseudopotentials
for FILE in $PSEUDO_LIST ; do
    if test ! -r $PSEUDO_DIR/$FILE ; then
       $ECHO
       $ECHO "Downloading $FILE to $PSEUDO_DIR...\c"
            $WGET $PSEUDO_DIR/$FILE $NETWORK_PSEUDO/$FILE 2> /dev/null
    fi
    if test $? != 0; then
        $ECHO
        $ECHO "ERROR: $PSEUDO_DIR/$FILE not existent or not readable"
        $ECHO "Aborting"
        exit 1
    fi
done
$ECHO " done"

# how to run executables
PW_COMMAND="$PARA_PREFIX $BIN_DIR/pw.x $PARA_POSTFIX"
PHCG_COMMAND="$PARA_PREFIX $BIN_DIR/phcg.x $PARA_POSTFIX"
DBO_COMMAND="$PARA_PREFIX $BIN_DIR/dbo.x $PARA_POSTFIX"
DYNMAT_COMMAND=" $BIN_DIR/dynmat.x"
$ECHO
$ECHO "  running pw.x as: $PW_COMMAND"
$ECHO "  running phcg.x as: $PHCG_COMMAND"
$ECHO "  running dbo.x as: $DBO_COMMAND"
$ECHO "  running dynmat.x as: $DYNMAT_COMMAND"
$ECHO

# check for gnuplot
GP_COMMAND=`which gnuplot 2>/dev/null`
if [ "$GP_COMMAND" = "" ]; then
        $ECHO
        $ECHO "gnuplot not in PATH"
        $ECHO "Results will not be plotted"
fi

# clean TMP_DIR
$ECHO "  cleaning $TMP_DIR...\c"
rm -rf $TMP_DIR/hi*
rm -rf $TMP_DIR/_ph0/hi*
$ECHO " done"

# self-consistent calculation
cat > hi.scf.in << EOF
 &control
    calculation='scf',
    restart_mode='from_scratch',
    prefix='hi'
    pseudo_dir = '$PSEUDO_DIR/',
    outdir='$TMP_DIR/'
 /
 &system
    ibrav = 1, celldm(1) = 16.0, nat=2, ntyp=2,
    ecutwfc = 50.0
 /
 &electrons
    mixing_beta = 0.7
    conv_thr =  1.0d-8
 /
ATOMIC_SPECIES
 H  1.0         H.pz-vbc.UPF
 I   126.90447  I.pz-hgh.UPF
ATOMIC_POSITIONS (bohr)
 I 8.0000000  8.0 8.0
 H 11.081346  8.0 8.0
K_POINTS gamma
EOF
$ECHO "  running the scf calculation for Hi...\c"
$PW_COMMAND < hi.scf.in > hi.scf.out
check_failure $?
$ECHO " done"

# normal mode calculation 
cat > hi.nm.in << EOF
normal modes for ih
 &inputph
  tr2_ph=1.0d-14,
  prefix='hi',
  amass(1)=1.008,
  amass(2)=126.90447,
  outdir='$TMP_DIR/',
  epsil=.true.,
  trans=.true., asr=.true.
  raman=.false.
  fildyn='hi.dyn'
 /
 0.0 0.0 0.0
EOF
$ECHO "  running the phonon calculation at Gamma for HI...\c"
$PHCG_COMMAND < hi.nm.in > hi.nm.out
$ECHO " done"

# IR cross sections  for SiH4
cat > hi.dyn.in << EOF
 &input fildyn='hi.dyn', asr='zero-dim' /
EOF

$ECHO "  running IR cross section calculation for HI...\c"
$DYNMAT_COMMAND < hi.dyn.in > hi.dyn.out
check_failure $?
$ECHO " done"

# Output 
$ECHO " Linear response result: "
grep 'freq (    6)' dynmat.out

# DBO calculation 
cat > hi.dbo.in << EOF
BEGIN
BEGIN_DBO_INPUT
&PATH
  search_method     = 'fixed'
  restart_mode      = 'from_scratch'
  num_of_points     = 49,
/
END_DBO_INPUT
BEGIN_ENGINE_INPUT
 &control
    prefix='hi'
    pseudo_dir = '$PSEUDO_DIR/',
    outdir='$TMP_DIR/'
 /
 &system
    ibrav = 1, celldm(1) = 16.0, nat=2, ntyp=2,
    ecutwfc = 50.0
 /
 &electrons
    mixing_beta = 0.7
    conv_thr =  1.0d-8
 /
ATOMIC_SPECIES
 H  1.0         H.pz-vbc.UPF
 I   126.90447  I.pz-hgh.UPF

K_POINTS gamma

BEGIN_POSITIONS
ATOMIC_POSITIONS (bohr)
 I 8.0000000  8.0 8.0
POINTSET
 H 10.331346  8.0 8.0
 H 10.362596  8.0 8.0
 H 10.393846  8.0 8.0
 H 10.425096  8.0 8.0
 H 10.456346  8.0 8.0
 H 10.487596  8.0 8.0
 H 10.518846  8.0 8.0
 H 10.550096  8.0 8.0
 H 10.581346  8.0 8.0
 H 10.612596  8.0 8.0
 H 10.643846  8.0 8.0
 H 10.675096  8.0 8.0
 H 10.706346  8.0 8.0
 H 10.737596  8.0 8.0
 H 10.768846  8.0 8.0
 H 10.800096  8.0 8.0
 H 10.831346  8.0 8.0
 H 10.862596  8.0 8.0
 H 10.893846  8.0 8.0
 H 10.925096  8.0 8.0
 H 10.956346  8.0 8.0
 H 10.987596  8.0 8.0
 H 11.018846  8.0 8.0
 H 11.050096  8.0 8.0
 H 11.081346  8.0 8.0
 H 11.112596  8.0 8.0
 H 11.143846  8.0 8.0
 H 11.175096  8.0 8.0
 H 11.206346  8.0 8.0
 H 11.237596  8.0 8.0
 H 11.268846  8.0 8.0
 H 11.300096  8.0 8.0
 H 11.331346  8.0 8.0
 H 11.362596  8.0 8.0
 H 11.393846  8.0 8.0
 H 11.425096  8.0 8.0
 H 11.456346  8.0 8.0
 H 11.487596  8.0 8.0
 H 11.518846  8.0 8.0
 H 11.550096  8.0 8.0
 H 11.581346  8.0 8.0
 H 11.612596  8.0 8.0
 H 11.643846  8.0 8.0
 H 11.675096  8.0 8.0
 H 11.706346  8.0 8.0
 H 11.737596  8.0 8.0
 H 11.768846  8.0 8.0
 H 11.800096  8.0 8.0
 H 11.831346  8.0 8.0
END_POSITIONS
END_ENGINE_INPUT
END
EOF
$ECHO "  running the DBO calculation for HI...\c"
$DBO_COMMAND -inp hi.dbo.in > hi.dbo.out
$ECHO " done"

grep 'H' hi.crd > pos
pr -m -t -s pos hi.dat |awk '{print $2,$5}' > data

cat > analyze.gplot << EOF
f(x) = a*(x**2) + c
fit[-0.2:0.2] f(x) 'data' u (\$1-11.081346):2 via a,c
EOF

$GP_COMMAND analyze.gplot > /dev/null

a=`grep 'a               =' fit.log | tail -1|cut -c 18-26`

freq=`echo "sqrt(2*$a)*981.8706" | bc`

$ECHO 'DBO results (harmonic approximation): \c'
$ECHO $freq

$ECHO " done"

$ECHO
$ECHO "$EXAMPLE_DIR: done"
